package com.elrsoft.note.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.FragmentNavigationManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.controller.MainActivityController;
import com.elrsoft.note.db.service.UserService;
import com.elrsoft.note.fragment.CloseAppFragment;
import com.elrsoft.note.fragment.LoginFragment;
import com.elrsoft.note.fragment.MainFragment;


public class MainActivity extends ActionBarActivity {

    ActionBar actionBar;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleAnalyticsController.setTracker(this, "main_activity");

        actionBar = ActionBarManagerController.managerActionBarCustom(this, R.layout.custom_action_bar, false);

        // визначає чи програма вимагає авторизації при запуску та віркриває відповідний фрагмент
        MainActivityController.openFragmentAfterStart(this, savedInstanceState);
//        Наразі опція Background буде недоступною, але код НЕ ВИДАЛЯТИ
//        MainActivityController.showBackgroundAfterStart(this);

    }

    public void get() {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // слухає item із меню, з яким відбулась взаємодія
        new MainActivityController(this).onMenuItemSelected(item, this);

        return true;
    }

    @Override
    public void onBackPressed() {
        // повертає на попередній фрагмент

        MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_main_fragment));
        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_login_fragment));
        CloseAppFragment closeAppFragment = (CloseAppFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.close_fragment));

        if (closeAppFragment == null) {
            if (mainFragment != null && mainFragment.isVisible() || loginFragment != null && loginFragment.isVisible() && loginFragment.getB()) {
                FragmentNavigationManagerController.moveToFragment(MainActivity.this, new CloseAppFragment(), MainActivity.this.getString(R.string.close_fragment));
            } else {
                MainActivityController.backPressed(this);
            }
        }
        //   super.onBackPressed();


    }


    public void paintClicked(View view) {
        MainActivityController.paintClicked(view, MainActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // обробляє результат, отриманий із певного актівіті
        new MainActivityController(MainActivity.this).onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(this);
    }

    @Override
    protected void onStop() {
        GoogleAnalyticsController.stopAnalytics(this);
        super.onStop();
    }

}