package com.elrsoft.note.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageView;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ImageController;

import java.util.Random;

/**
 * Created by HANZ on 18.05.2015.
 */
public class StartActivity extends ActionBarActivity {

    final String path = "app/hello/";
    Handler handler;
    final String[] resImage = {"glad.png", "hello.png", "hi.png", "nice.png", "welcom.png"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_fragment);
        Random random = new Random();
        getSupportActionBar().hide();
        ((ImageView) findViewById(R.id.imageApp)).setImageBitmap(ImageController.getBitmapFromAsset(this, path + resImage[random.nextInt(4)]));
        handler = new Handler();
        getSupportActionBar().hide();
        handler.postDelayed(runnable, 1200);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            StartActivity.this.startActivity(new Intent(StartActivity.this, MainActivity.class));
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        handler.removeCallbacks(runnable);
        super.onBackPressed();
    }
}
