package com.elrsoft.note.db.service;

import android.content.Context;
import android.util.Log;

import com.elrsoft.note.db.dao.PostDao;
import com.elrsoft.note.db.dao.PostFileDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Yana on 23.03.2015.
 */
public class PostService extends ServiceOpenDB implements BaseService<Post> {

    Context context;

    public PostService(Context context) {
        this.context = context;
    }

    // відкриває БД, викликає метод, що зберігає запис про об'єкт post в БД, після чого закриває БД
    @Override
    public long save(Post post) {
        try {
            if (!isOpenDB()) {
                open(context);
            }

            return new PostDao(getSqLiteDatabase()).save(post);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що видаляє запис про об'єкт post в БД, після чого закриває БД
    @Override
    public long delete(Post post){
    try {
        if (!isOpenDB()) {
            open(context);
        }
        return new PostDao(getSqLiteDatabase()).delete(post);
    } finally {
        if (isOpenDB()) {
            close();
        }
    }
    }

    // відкриває БД, викликає метод, що оновлює запис про об'єкт post в БД, після чого закриває БД
    @Override
    public int update(Post post) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostDao(getSqLiteDatabase()).update(post);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає список усіх записів таблиці post в БД, після чого закриває БД
    @Override
    public List<Post> getAll() {

        try {
            if (!isOpenDB()) {
                Log.d("MyLog", "open");
                open(context);
            }
            return new PostDao(getSqLiteDatabase()).get();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає запис запитаного об'єкта post, після чого закриває БД
    @Override
    public Post getById(Post post) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostDao(getSqLiteDatabase()).getBy(post.getId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public List<Post> getByName(String name){
        try {
            if(!isOpenDB()) {
                open(context);
            }
            return new PostDao(getSqLiteDatabase()).getByName(name);
        } finally {
            if(isOpenDB()) {
                close();
            }
        }
    }

    public Map<Post, List<File>> getListPostWithTagAndFiles(){
        try {
            if (!isOpenDB()) {
                open(context);
            }
            Map<Post, List<File>> map = new TreeMap<>();
            List<Post> posts = new PostDao(getSqLiteDatabase()).get();
            for(Post post : posts) {
                List<File> files = new PostFileDao(getSqLiteDatabase()).getBy(post);
                map.put(post, files);
            }
            return map;

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public int getCountPost(){
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostDao(getSqLiteDatabase()).getCountPost();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

}
