
package com.elrsoft.note.db.dao;

import com.elrsoft.note.model.Group;
import com.elrsoft.note.model.Post;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.model.PostGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vall on 03.03.15.
 */
public class PostGroupDao {

    SQLiteDatabase sqLiteDatabase;

    public PostGroupDao (SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    // створюємо PostGroup для відповідних post і group, викликаємо save, що збереже цей екземпляр
    public long create(Post post, Group group) {
        PostGroup instance = new PostGroup(post, group);
        return save(instance);
    }

    // отримуємо список груп, що відповідають запитаному посту
    public List<Group> getBy(Post post){

        List<Group> groupsList = new ArrayList<>();
        Group group = new Group();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.PostGroup.TABLE_NAME + " where " + DBResource.PostGroup.POST_ID + "= ?", new String[]{String.valueOf(post.getId())});


        if (cursor != null && cursor.moveToFirst()) {
            do {
                long groupId = cursor.getLong(cursor.getColumnIndex(DBResource.PostGroup.GROUP_ID));
                group.setGroupId(groupId);
                groupsList.add(group);

            } while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }

        return groupsList;
    }

// отримуємо список постів, що відповідають запитаній групі
    public List<Post> getBy(Group group){

        Post post = new Post();
        List<Post> postsList = new ArrayList();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.PostGroup.TABLE_NAME + " where " + DBResource.PostGroup.GROUP_ID + "= ?", new String[]{String.valueOf(group.getGroupId())});
        if (cursor != null && cursor.moveToFirst()) {
            do {
                long postId = cursor.getLong(cursor.getColumnIndex(DBResource.PostGroup.POST_ID));
                post.setId(postId);
                postsList.add(post);

            } while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }

        return postsList;
    }

// отримуємо запис з таблиці PostGroup бази даних, що відповідає запитатим параметрам
    public PostGroup get(long postId, long groupId){

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.PostGroup.TABLE_NAME + " where " + DBResource.PostGroup.POST_ID + "= ? and " + DBResource.PostGroup.GROUP_ID + "= ? " ,
                new String[]{String.valueOf(postId), String.valueOf(groupId)} );
        if (cursor != null) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }
    }

    // зберігаємо запис про відповідний об'єкт PostGroup в таблиці бази даних
    public long save(PostGroup postGroup){

        return sqLiteDatabase.insert(DBResource.PostGroup.TABLE_NAME, null, fullContentValue(postGroup));
    }

    // оновлюємо запис відповідного postGroup в базі даних
    public int update(PostGroup postGroup){

        long postId = postGroup.getPost().getId();
        long groupId = postGroup.getGroup().getGroupId();
        return sqLiteDatabase.update(DBResource.PostGroup.TABLE_NAME, fullContentValue(postGroup), DBResource.PostGroup.POST_ID + " = ? and " +
                        DBResource.PostGroup.GROUP_ID + "= ?",
                new String[]{String.valueOf(postId), String.valueOf(groupId)});
    }

    public int remove(long postId, long groupId){

        return sqLiteDatabase.delete(DBResource.PostGroup.TABLE_NAME, DBResource.PostGroup.POST_ID + " = ? and " + DBResource.PostGroup.GROUP_ID + "= ? ",
                new String[]{String.valueOf(postId), String.valueOf(groupId)});

    }

    // видаляємо з таблиці бази даних, усі записи, що містять дані, про запитаний пост
    public int removeAll(Post post){

        return sqLiteDatabase.delete(DBResource.PostGroup.TABLE_NAME, DBResource.PostGroup.POST_ID + " = ? ",
                new String[]{String.valueOf(post.getId())});
    }

    // видаляємо з таблиці бази даних усі записи, що містять дані про запитану групу
    public int removeAll(Group group){

        return sqLiteDatabase.delete(DBResource.PostGroup.TABLE_NAME, DBResource.PostGroup.GROUP_ID + " = ? ",
                new String[]{String.valueOf(group.getGroupId())});
    }


    // повертає List, що складається з Id всіх Groups, що належать посту з запитаним Id
    public List getListOfGroupsId(long post_id){
        List groupsIdList = new ArrayList();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.PostGroup.TABLE_NAME + " where " + DBResource.PostGroup.ID +
                " = ? ", new String[]{String.valueOf(post_id)});

        if(cursor != null && cursor.moveToFirst()){
            do {

                long groupId = cursor.getLong(cursor.getColumnIndex(DBResource.PostGroup.GROUP_ID));
                groupsIdList.add(groupId);

            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return groupsIdList;
    }


    // повертає список груп, що відповідають запитаному списку ід
    public List<Group> getGroupsById(List<Integer> groupsIdList){
        List<Group> groupsList = new ArrayList();

        for(int temp: groupsIdList){
            Cursor cursor = sqLiteDatabase.rawQuery("Select * from " + DBResource.Group.TABLE_NAME + " where " + DBResource.Group.ID +
                    " = ?", new String[]{String.valueOf(temp)});

            if(cursor != null && cursor.moveToFirst()){
                do {
                    long groupId = cursor.getLong(cursor.getColumnIndex(DBResource.Group.ID));
                    groupsList.add(new Group(groupId));

                } while (cursor.moveToNext());
            }
            if (cursor != null) {
                cursor.close();
            }
        }
        return groupsList;
    }


    // навігація по таблиці PostGroup в базі даних
    public List<PostGroup> parseCursor(Cursor cursor) {

        List<PostGroup> postGroupList = new ArrayList<>();
        Post post = new Post();
        Group group = new Group();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                long postId = cursor.getLong(cursor.getColumnIndex(DBResource.PostGroup.POST_ID));
                long groupId = cursor.getLong(cursor.getColumnIndex(DBResource.PostGroup.GROUP_ID));

                post.setId(postId);
                group.setGroupId(groupId);

                postGroupList.add(new PostGroup(post, group));

            } while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }
        return postGroupList;

    }

    // створюємо об'єкт ContentValues що містить дані про звичайний об'єкт PostGroup

    private ContentValues fullContentValue(PostGroup postgroup) {
        ContentValues cv = new ContentValues();
        cv.put(DBResource.PostGroup.POST_ID, postgroup.getPost().getId());
        cv.put(DBResource.PostGroup.GROUP_ID, postgroup.getGroup().getGroupId());

        return cv;
    }

}
