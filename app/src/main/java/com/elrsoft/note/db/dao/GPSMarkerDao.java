package com.elrsoft.note.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.core.Dao;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.GPSMarker;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.model.PostFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vika on 20.03.15.
 */
public class GPSMarkerDao implements Dao<GPSMarker> {
    SQLiteDatabase sqLiteDatabase;

    public GPSMarkerDao(SQLiteDatabase sqLiteDatabase) {

        this.sqLiteDatabase = sqLiteDatabase;
    }

    //метод save, який зберігає відповідний об'єкт GPSMarker в таблиці бази даних
    @Override
    public long save(GPSMarker gpsMarker) {
        return sqLiteDatabase.insert(DBResource.GPSMarker.TABLE_NAME, null, fullContentValues(gpsMarker));


    }

    // метод delete видаляє запис про відповідний gpsMarker із таблиці бази даних
    @Override
    public long delete(GPSMarker gpsMarker) {


        return sqLiteDatabase.delete(DBResource.GPSMarker.TABLE_NAME, DBResource.GPSMarker.ID
                + " = ? ", new String[]{String.valueOf(gpsMarker.getId())});


    }
    //метод update оновлює запис з таблиці GPSMarker
    @Override
    public int update(GPSMarker gpsMarker) {

        return sqLiteDatabase.update(DBResource.GPSMarker.TABLE_NAME, fullContentValues(gpsMarker), DBResource.GPSMarker.ID + " = ?", new String[]{String.valueOf(gpsMarker.getId())});


    }

    // отримуємо список усіх записів із таблиці GPSMarker в базі даних
    @Override
    public List<GPSMarker> get() {


        Cursor cursor = sqLiteDatabase.rawQuery("select * from " +
                DBResource.GPSMarker.TABLE_NAME, null);
        List<GPSMarker> gpsMarkerList = parseCursor(cursor);
        return gpsMarkerList;


    }
    // отримуємо запис із таблиці GPSMarker, id якого відповідає аргументу, що передаєм в метод
    @Override
    public GPSMarker getBy(long id) {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from  " +
                        DBResource.GPSMarker.TABLE_NAME + " where " + DBResource.GPSMarker.ID + " = ?",
                new String[]{String.valueOf(id)});

        return parseCursor(cursor).get(0);


    }
    //метод parseCursor приймає готовий курсор, создаєм новий ліст, витягуєм id поста і файла, додаємо в свій ліст, і повертаємо ліст даних

    @Override
    public List<GPSMarker> parseCursor(Cursor cursor) {

        List<GPSMarker> gpsMarkerList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DBResource.GPSMarker.ID));
                double latitude = cursor.getDouble(cursor.getColumnIndex(DBResource.GPSMarker.LATITUDE));
                double longitude = cursor.getDouble(cursor.getColumnIndex(DBResource.GPSMarker.LONGITUDE));

                gpsMarkerList.add(new GPSMarker(id, latitude, longitude));
            } while (cursor.moveToNext());


        }
        if (cursor != null) {
            cursor.close();
        }
        return gpsMarkerList;


    }
    //створюємо ContentValues, який містить дані про об'єкт PostFile
    private ContentValues fullContentValues(GPSMarker gpsMarker) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBResource.GPSMarker.LATITUDE, String.valueOf(gpsMarker.getLatitude()));
        contentValues.put(DBResource.GPSMarker.LONGITUDE, String.valueOf(gpsMarker.getLongitude()));
        return contentValues;
    }
}


