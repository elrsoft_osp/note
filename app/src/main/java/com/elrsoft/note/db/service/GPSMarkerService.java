package com.elrsoft.note.db.service;

import android.content.Context;

import com.elrsoft.note.db.dao.GPSMarkerDao;
import com.elrsoft.note.db.dao.UserDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.GPSMarker;

import java.util.List;

/**
 * Created by vika on 23.03.15.
 */
public class GPSMarkerService extends ServiceOpenDB implements BaseService<GPSMarker> {
    Context context;

    public GPSMarkerService(Context context) {
        this.context = context;
        

    }

    //відкриваємо БД, викликаємо метод save, що зберігає запис про об'єкт gpsMarker в БД, після чого закриваємо БД
    @Override
    public long save(GPSMarker gpsMarker) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GPSMarkerDao(getSqLiteDatabase()).save(gpsMarker);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    //відкриваємо БД, метод delete видаляє запис про відповідний gpsMarker із таблиці бази даних, після чого закриває БД
    @Override
    public long delete(GPSMarker gpsMarker) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GPSMarkerDao(getSqLiteDatabase()).delete(gpsMarker);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
    //відкриваємо БД, метод update оновлює запис з таблиці GPSMarker, після чого закриває БД
    @Override
    public int update(GPSMarker gpsMarker) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GPSMarkerDao(getSqLiteDatabase()).update(gpsMarker);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
    // відкриваємо БД, викликає метод, що повертає список усіх записів таблиці GPSMarker в БД, після чого закриваємо БД

    @Override
    public List<GPSMarker> getAll() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GPSMarkerDao(getSqLiteDatabase()).get();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
    // відкриваємо БД, викликає метод, що повертає запис запитаного об'єкта gpsMarker, після чого закриваємо БД
    @Override
    public GPSMarker getById(GPSMarker gpsMarker) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GPSMarkerDao(getSqLiteDatabase()).getBy(gpsMarker.getId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
}
