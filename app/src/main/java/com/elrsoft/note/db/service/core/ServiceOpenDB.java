package com.elrsoft.note.db.service.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.note.db.DBHelper;

/**
 * Created by <ELRsoft> HANZ on 20.03.2015.
 */
public abstract class ServiceOpenDB {

    private DBHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;

    public SQLiteDatabase getSqLiteDatabase() {
        return sqLiteDatabase;
    }

    protected boolean isOpenDB() {
        return sqLiteDatabase != null && dbHelper != null && sqLiteDatabase.isOpen();
    }

    protected void open(Context context) {
        if (sqLiteDatabase == null || !sqLiteDatabase.isOpen()) {
            dbHelper = new DBHelper(context);
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
    }

    protected void close() {
        if (dbHelper != null) {
            dbHelper.close();

        }
    }

}
