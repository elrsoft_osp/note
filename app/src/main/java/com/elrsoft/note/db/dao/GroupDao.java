package com.elrsoft.note.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.core.Dao;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 23.03.2015.
 */
public class GroupDao implements Dao<Group> {

    SQLiteDatabase sqLiteDatabase;

    public GroupDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    // вносимо запис group в відповідну таблицю бази даних
    @Override
    public long save(Group group) {

        return sqLiteDatabase.insert(DBResource.Group.TABLE_NAME, null, fullFileContentValue(group));

    }

    // видаляємо запис про відповідний group із таблиці бази даних
    @Override
    public long delete(Group group) {

        return sqLiteDatabase.delete(DBResource.Group.TABLE_NAME, DBResource.Group.GROUP_ID + " = ?",
                new String[]{String.valueOf(group.getGroupId())});

    }

    // оновлюємо запис відповідної group-u в базі даних
    @Override
    public int update(Group group) {

        return sqLiteDatabase.update(DBResource.Group.TABLE_NAME, fullFileContentValue(group), DBResource.Group.GROUP_ID + " = ?",
                new String[]{String.valueOf(group.getGroupId())});

    }

    // отримуємо список усіх записів із таблиці group в базі даних
    @Override
    public List<Group> get() {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Group.TABLE_NAME, null);
        List<Group> groupList = parseCursor(cursor);
        return groupList;

    }

    // отримуємо запис із таблиці group, id якого відповідає аргументу, що передаєм в метод
    @Override
    public Group getBy(long id) {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Group.TABLE_NAME + " where " + DBResource.Group.GROUP_ID+ "= ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }

    }

    // навігація по таблиці group в базі даних
    @Override
    public List<Group> parseCursor(Cursor cursor) {

        List<Group> fileList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {

                long groupId = cursor.getLong(cursor.getColumnIndex(DBResource.Group.GROUP_ID));
                fileList.add(new Group(groupId));

            } while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }
        return fileList;

    }

    // створюємо об'єкт ContentValues що містить дані про звичайний об'єкт group
    private ContentValues fullFileContentValue(Group group) {
        ContentValues cv = new ContentValues();
        cv.put(DBResource.Group.GROUP_ID, group.getGroupId());

        return cv;
    }
}
