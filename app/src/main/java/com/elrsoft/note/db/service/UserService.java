package com.elrsoft.note.db.service;

import android.content.Context;

import com.elrsoft.note.db.dao.UserDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.User;

import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 20.03.2015.
 */
public class UserService extends ServiceOpenDB implements BaseService<User> {

    Context context;

    public UserService(Context context) {
        this.context = context;
    }

    //відкриваємо БД, викликаємо метод save, що зберігає запис про об'єкт user в БД, після чого закриваємо БД
    @Override
    public long save(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).save(user);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    //відкриваємо БД, метод delete видаляє запис про відповідний user із таблиці  User бази даних, після чого закриває БД
    @Override
    public long delete(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).delete(user);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
    //відкриваємо БД, метод update оновлює запис з таблиці User, після чого закриває БД
    @Override
    public int update(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).update(user);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
    // відкриваємо БД, викликає метод, що повертає список усіх записів таблиці User в БД, після чого закриваємо БД
    @Override
    public List<User> getAll() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).get();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриваємо БД, викликає метод, що повертає запис запитаного об'єкта user, після чого закриваємо БД
    @Override
    public User getById(User user) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).getBy(user.getId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
  //відкриваємо БД, викликаємо метод, що повертає об'єкт user, після чого закриваємо БД
    public User getUser() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).getUser();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }


    // повертає булеве значення відповідно до того чи користувач є авторизований у програмі
    public boolean isLogin(User user){

        try {
            if (!isOpenDB()) {
                open(context);
            }
            int isLogin = new UserDao(getSqLiteDatabase()).isLogin(user);

            if(isLogin == 1){
                return true;
            }else{
                return false;
            }
        } finally {
            if (isOpenDB()) {
                close();
            }
        }

    }

    // повертає булеве значення відповідно до того чи користувач знаходиться наразі у додатку
    public boolean isOnline(){

        try {
            if (!isOpenDB()) {
                open(context);
            }
            int isOnline = new UserDao(getSqLiteDatabase()).isOnline();

            if(isOnline == 1){
                return true;
            }else{
                return false;
            }
        } finally {
            if (isOpenDB()) {
                close();
            }
        }

    }

    // оновлює дані користувача, в залежності від того вирішив він активувати
    // чи скасувати функцію логіна при запуску програми
    public int updateLogin( boolean isLogin){
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).updateLogin(isLogin);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }

    }

    // оновлює дані користувача, в залежності від того чи він авторизувався або вийшов із додатку
    public int updateOnline( boolean isOnline){
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new UserDao(getSqLiteDatabase()).updateOnline(isOnline);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }

    }

    //зберігає новий пароль для користувача
    public int updatePassword(String password){
        try{
            if(!isOpenDB()){
                open(context);
            }
            return  new UserDao(getSqLiteDatabase()).updatePassword(password);
        }finally {
            if(isOpenDB()){
                close();
            }
        }
    }
}
