package com.elrsoft.note.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.core.Dao;
import com.elrsoft.note.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nazar on 19.03.2015.
 */
public class UserDao implements Dao<User> {

    SQLiteDatabase sqLiteDatabase;

    public UserDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    // вносимо запис User в відповідну таблицю бази даних
    @Override
    public long save(User user) {
        return sqLiteDatabase.insert(DBResource.User.TABLE_NAME, null, fullUserContentValue(user));
    }

    // видаляємо запис про відповідний user із таблиці бази даних
    @Override
    public long delete(User user) {
        return sqLiteDatabase.delete(DBResource.User.TABLE_NAME, DBResource.User.ID + "= ?", new String[]{String.valueOf(user.getId())});
    }

    // оновлюємо запис відповідного user-а в базі даних
    @Override
    public int update(User user) {
        return sqLiteDatabase.update(DBResource.User.TABLE_NAME, fullUserContentValue(user), DBResource.User.ID + "= ?", new String[]{String.valueOf(user.getId())});
    }

    // отримуємо список усіх записів із таблиці User в базі даних
    @Override
    public List<User> get() {
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.User.TABLE_NAME, null);
        List<User> userList = parseCursor(cursor);
        return userList;
    }

    // отримуємо запис із таблиці User, id якого відповідає аргументу, що передаєм в метод
    @Override
    public User getBy(long id) {
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.User.TABLE_NAME + " where " + DBResource.User.ID + "= ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }
    }

    //отримуємо ліст юзерів з таблиці User
    public User getUser() {
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.User.TABLE_NAME + " where " + DBResource.User.ID + "= ?", new String[]{String.valueOf(1)});
        if (cursor != null) {
            List<User> users = parseCursor(cursor);
            if (users.size() != 0) {
                return users.get(0);
            }


        }
        return null;

    }

    // повертає int-ову 1 якщо користувач виконав вхід у програму
    public int isLogin(User user) {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.User.TABLE_NAME + " where " + DBResource.User.ID + "= ?", new String[]{String.valueOf(1)});

        cursor.moveToFirst();
        if (cursor != null) {
            int isLogin = cursor.getInt(cursor.getColumnIndex(DBResource.User.IS_LOGIN));

            if (isLogin == 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return Integer.parseInt(null);
        }
    }

    // повертає int-ову 1 якщо користувач виконав логін у програму при її запуску
    public int isOnline() {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.User.TABLE_NAME + " where " + DBResource.User.ID + "= ?", new String[]{String.valueOf(1)});

        cursor.moveToFirst();
        if (cursor != null) {
            int isOnline = cursor.getInt(cursor.getColumnIndex(DBResource.User.IS_ONLINE));

            if (isOnline == 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return Integer.parseInt(null);
        }
    }

    // оновлює дані користувача, в залежності від того вирішив він активувати
    // чи скасувати функцію логіна при запуску програми
    public int updateLogin(boolean isLogin) {
        ContentValues contentValues = new ContentValues();

        if (isLogin) {
            contentValues.put(DBResource.User.IS_LOGIN, 1);
        } else {
            contentValues.put(DBResource.User.IS_LOGIN, 0);
        }
        return sqLiteDatabase.update(DBResource.User.TABLE_NAME, contentValues, DBResource.User.ID + "= ?", new String[]{String.valueOf(1)});
    }

    // оновлює дані користувача, в залежності від того запустив він додаток чи вийшов із нього
    public int updateOnline(boolean isOnline) {
        ContentValues contentValues = new ContentValues();

        if (isOnline) {
            contentValues.put(DBResource.User.IS_ONLINE, 1);
        } else {
            contentValues.put(DBResource.User.IS_ONLINE, 0);
        }
        return sqLiteDatabase.update(DBResource.User.TABLE_NAME, contentValues, DBResource.User.ID + "= ?", new String[]{String.valueOf(1)});
    }

    //зберігає відновлений пароль
    public int updatePassword(String password) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBResource.User.PASSWORD, password);

        return sqLiteDatabase.update(DBResource.User.TABLE_NAME, contentValues, DBResource.User.ID + "= ?", new String[]{String.valueOf(1)});
    }

    // навігація по таблиці User
    @Override
    public List<User> parseCursor(Cursor cursor) {
        List<User> userList = new ArrayList<>();


        if (cursor != null && cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DBResource.User.ID));
                String firstName = cursor.getString(cursor.getColumnIndex(DBResource.User.FIRST_NAME));
                String lastName = cursor.getString(cursor.getColumnIndex(DBResource.User.LAST_NAME));
                int sex = cursor.getInt(cursor.getColumnIndex(DBResource.User.SEX));
                String email = cursor.getString(cursor.getColumnIndex(DBResource.User.EMAIL));
                String password = cursor.getString(cursor.getColumnIndex(DBResource.User.PASSWORD));
                String country = cursor.getString(cursor.getColumnIndex(DBResource.User.COUNTRY));
                String  birthday =cursor.getString(cursor.getColumnIndex(DBResource.User.BIRTHDAY));
                    Log.d("birthday", birthday + " ");



                userList.add(new User(id, firstName, lastName, sex, email, password, birthday, country));

            } while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }
        return userList;
    }

    //створюємо ContentValues, який містить дані про об'єкт User
    private ContentValues fullUserContentValue(User user) {
        ContentValues cv = new ContentValues();

        cv.put(DBResource.User.FIRST_NAME, user.getFirstName());
        cv.put(DBResource.User.LAST_NAME, user.getLastName());
        cv.put(DBResource.User.SEX, user.getSex());
        cv.put(DBResource.User.EMAIL, user.getEmail());
        cv.put(DBResource.User.PASSWORD, user.getPassword());
        cv.put(DBResource.User.BIRTHDAY, String.valueOf(user.getBirthday()));
        cv.put(DBResource.User.COUNTRY, user.getCountry());

        return cv;
    }
}
