package com.elrsoft.note.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.core.Dao;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.model.PostFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vall on 03.03.15.
 */
public class PostFileDao {
    SQLiteDatabase sqLiteDatabase;


    public PostFileDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;

    }

    //отримуємо список файлів, які відповідають посту із запитаним id, повертаємо новий List файлів
    public List<Long> getBy(long postId) {
        List<Long> fileList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " +
                        DBResource.PostFile.TABLE_NAME + " where " +
                        DBResource.PostFile.ID_POST + " = ? ",
                new String[]{String.valueOf(postId)});
        if (cursor != null && cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DBResource.PostFile.ID_FILE));
                fileList.add(id);
            }
            while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }
        return fileList;
    }

// отримуємо  список постів, які відповідають даному файлу, повертаємо новий List постів
    public List<Post> getBy(File file) {
        List<Post> postList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from" +
                        DBResource.PostFile.TABLE_NAME + "where" +
                        DBResource.PostFile.ID_FILE + " = ?",
                new String[]{String.valueOf(file.getId())}
        );
        if (cursor != null && cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DBResource.PostFile.ID_POST));
                postList.add(new Post(id));

            }
            while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }
        return postList;
    }

    public List<File> getBy(Post post) {
        List<File> fileList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from" +
                        DBResource.PostFile.TABLE_NAME + "where" +
                        DBResource.PostFile.ID_POST + " = ?",
                new String[]{String.valueOf(post.getId())}
        );
        if (cursor != null && cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DBResource.PostFile.ID_POST));
                fileList.add(new File(id));

            }
            while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }
        return fileList;
    }

    //отримуємо з таблиці PostFile всі дані по id і повертаємо відпарсений курсор
    public PostFile get(long postId, long fileId) {
        Cursor cursor = sqLiteDatabase.rawQuery("select * from" +
                DBResource.PostFile.TABLE_NAME + "where " +
                DBResource.PostFile.ID_POST + " = ? and " +
                DBResource.PostFile.ID_FILE + " = ?", new String[]{String.valueOf(postId),
                String.valueOf(fileId)});

        return parseCursor(cursor).get(0);


    }

//метод parseCursor приймає готовий курсор, создаєм новий ліст, витягуєм id поста і файла, додаємо в свій ліст, і повертаємо ліст даних
    private List<PostFile> parseCursor(Cursor cursor) {
        List<PostFile> postFileList = new ArrayList<>();
        Post post = new Post();
        File file = new File();

        if (cursor != null && cursor.moveToFirst()) {
            do {

                long postId = cursor.getLong(cursor.getColumnIndex(DBResource.PostFile.ID_POST));
                long fileId = cursor.getLong(cursor.getColumnIndex(DBResource.PostFile.ID_FILE));
                post.setId(postId);
                file.setId(fileId);
                postFileList.add(new PostFile(post, file));

            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();

        }
        return postFileList;

    }

    //метод save, який зберігає відповідний об'єкт PostFile в таблиці бази даних
    public long save(long postId, long fileId) {
        return sqLiteDatabase.insert(DBResource.PostFile.TABLE_NAME, null, fullContentValues(postId, fileId));
    }

    //метод update оновлює запис з таблиці PostFile
    public int update(long postId, long fileId) {
        return sqLiteDatabase.update(DBResource.PostFile.TABLE_NAME, fullContentValues(postId, fileId),
                DBResource.PostFile.ID_POST + " = ? and " +
                        DBResource.PostFile.ID_FILE + " = ? ",
                new String[]{String.valueOf(postId),
                        String.valueOf(fileId)});

    }


    //метод  remove видаляє запис з даної таблиці PostFile по id
    public int remove(long post, long file) {
        return sqLiteDatabase.delete(DBResource.PostFile.TABLE_NAME,
                DBResource.PostFile.ID_POST + " = ? and " +
                        DBResource.PostFile.ID_FILE + " = ? ",
                new String[]{String.valueOf(post), String.valueOf(file)});

    }

    //метод removeAll видаляє усі записи, які містять дані про запитаний пост
    public int removeAll(Post post) {
        return sqLiteDatabase.delete(DBResource.PostFile.TABLE_NAME,
                DBResource.PostFile.ID_POST + " = ?",
                new String[]{String.valueOf(post.getId())});
    }

    //метод removeAll видаляє усі записи, які містять дані про запитаний файл
    public int removeAll(File file) {
        return sqLiteDatabase.delete(DBResource.PostFile.TABLE_NAME,
                DBResource.PostFile.ID_FILE + " = ?",
                new String[]{String.valueOf(file.getId())});
    }


    //створюємо ContentValues, який містить дані про об'єкт PostFile
    private ContentValues fullContentValues(long postId, long fileId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBResource.PostFile.ID_POST, postId);
        contentValues.put(DBResource.PostFile.ID_FILE, fileId);
        return contentValues;
    }
}