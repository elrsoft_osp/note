package com.elrsoft.note.db.service;

import android.content.Context;
import android.database.Cursor;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.FileDao;
import com.elrsoft.note.db.dao.GroupDao;
import com.elrsoft.note.db.dao.PostDao;
import com.elrsoft.note.db.dao.PostGroupDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.Group;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.model.PostGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 23.03.2015.
 */
public class PostGroupService extends ServiceOpenDB implements BaseService<PostGroup> {

    Context context;

    public PostGroupService(Context context) {
        this.context = context;
    }

    // відкриває БД, викликає метод, що зберігає запис про об'єкт PostGroup в БД, після чого закриває БД
    @Override
    public long save(PostGroup postGroup) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostGroupDao(getSqLiteDatabase()).save(postGroup);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що видаляє запис про об'єкт PostGroup в БД, після чого закриває БД
    @Override
    public long delete(PostGroup postGroup){

    try
    {
        if (!isOpenDB()) {
            open(context);
        }
        new PostDao(getSqLiteDatabase()).delete(postGroup.getPost());
        new GroupDao(getSqLiteDatabase()).delete(postGroup.getGroup());
        new PostGroupDao(getSqLiteDatabase()).remove(postGroup.getPost().getId(), postGroup.getGroup().getGroupId());

        return postGroup.getPost().getId();
    }

    finally

    {
        if (isOpenDB()) {
            close();
        }
    }

}

    // відкриває БД, викликає метод, що оновлює запис про об'єкт  PostGroup в БД, після чого закриває БД
    @Override
    public int update(PostGroup postGroup) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostGroupDao(getSqLiteDatabase()).update(postGroup);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }


    @Override
    public List<PostGroup> getAll() {
        return null;
    }


    // відкриває БД, викликає метод, що повертає список усіх записів таблиці PostGroup в БД, після чого закриває БД
    public PostGroup getAll(PostGroup postGroup) {

        try {
            if (!isOpenDB()) {
                open(context);
            }


            return new PostGroupDao(getSqLiteDatabase()).get(postGroup.getPost().getId(), postGroup.getGroup().getGroupId());

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає запис запитаного об'єкта  PostGroup, після чого закриває БД
    @Override
    public PostGroup getById(PostGroup postGroup) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostGroupDao(getSqLiteDatabase()).get(postGroup.getPost().getId(), postGroup.getGroup().getGroupId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає список усіх постів для запитаної групи, після чого закриває БД
    public List<Post> getBy(Group group) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostGroupDao(getSqLiteDatabase()).getBy(group);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає список усіх груп для вказаного поста, після чого закриває БД
    public List<Group> getBy(Post post) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostGroupDao(getSqLiteDatabase()).getBy(post);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }


    // повертає список груп, що відповідають запитаному списку ід
    public List<Group> getGroupsById(List<Integer> groupsIdList) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostGroupDao(getSqLiteDatabase()).getGroupsById(groupsIdList);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // повертає List, що складається з Id всіх Groups, що належать посту з запитаним Id
    public List getListOfGroupsId(long post_id){

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostGroupDao(getSqLiteDatabase()).getListOfGroupsId(post_id);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
}
