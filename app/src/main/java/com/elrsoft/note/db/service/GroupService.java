package com.elrsoft.note.db.service;

import android.content.Context;

import com.elrsoft.note.db.dao.FileDao;
import com.elrsoft.note.db.dao.GroupDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.Group;

import java.util.List;

/**
 * Created by Yana on 23.03.2015.
 */
public class GroupService extends ServiceOpenDB implements BaseService<Group> {

    Context context;

    public GroupService(Context context){
        this.context = context;
    }

    // відкриває БД, викликає метод, що зберігає запис про об'єкт group в БД, після чого закриває БД
    @Override
    public long save(Group group) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GroupDao(getSqLiteDatabase()).save(group);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що видаляє запис про об'єкт group в БД, після чого закриває БД
    @Override
    public long delete(Group group) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GroupDao(getSqLiteDatabase()).delete(group);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що оновлює запис про об'єкт group в БД, після чого закриває БД
    @Override
    public int update(Group group) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GroupDao(getSqLiteDatabase()).update(group);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає список усіх записів таблиці group в БД, після чого закриває БД
    @Override
    public List<Group> getAll() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GroupDao(getSqLiteDatabase()).get();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає запис запитаного об'єкта group, після чого закриває БД
    @Override
    public Group getById(Group group) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new GroupDao(getSqLiteDatabase()).getBy(group.getGroupId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
}
