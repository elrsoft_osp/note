package com.elrsoft.note.db.service.core;

import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 20.03.2015.
 */
public interface BaseService<T> {

    long save(T tt);

    long delete(T t);

    int update(T t);

    List<T> getAll();

    T getById(T t);
}
