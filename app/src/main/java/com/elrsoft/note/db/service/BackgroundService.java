package com.elrsoft.note.db.service;

import android.content.Context;

import com.elrsoft.note.db.dao.BackgroundDao;
import com.elrsoft.note.db.dao.PostDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.Background;
import com.elrsoft.note.model.Post;

import java.util.List;

/**
 * Created by Yana on 23.03.2015.
 */
public class BackgroundService extends ServiceOpenDB implements BaseService<Background> {

    Context context;

    public BackgroundService (Context context) {
        this.context = context;
    }

    // відкриває БД, викликає метод, що зберігає запис про об'єкт background в БД, після чого закриває БД
    @Override
    public long save(Background background) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new BackgroundDao(getSqLiteDatabase()).save(background);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що видаляє запис про об'єкт background в БД, після чого закриває БД
    @Override
    public long delete(Background background) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new BackgroundDao(getSqLiteDatabase()).delete(background);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що оновлює запис про об'єкт background в БД, після чого закриває БД
    @Override
    public int update(Background background) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new BackgroundDao(getSqLiteDatabase()).update(background);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає список усіх записів таблиці background в БД, після чого закриває БД
    @Override
    public List<Background> getAll() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new BackgroundDao(getSqLiteDatabase()).get();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає запис запитаного об'єкта background, після чого закриває БД
    @Override
    public Background getById(Background background) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new BackgroundDao(getSqLiteDatabase()).getBy(background.getId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public Background getBackground(){
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new BackgroundDao(getSqLiteDatabase()).getByDefault();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public void setBackground(Background background){
        try {
            if (!isOpenDB()) {
                open(context);
            }

            BackgroundDao backgroundDao = new BackgroundDao(getSqLiteDatabase());
            Background b = backgroundDao.getByDefault();
            if(b != null) {
                b.setIsDefault(0);
                backgroundDao.update(b);
            }
            background.setIsDefault(1);
            if(backgroundDao.getBy(background.getId()) == null){
                backgroundDao.save(background);
            } else {
                backgroundDao.update(background);
            }
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }
}
