package com.elrsoft.note.db;

/**
 * Created by Markevych on 28.02.2015.
 */
public class DBResource {

    public static final String DB_NAME_NOTE = "note";
    public static final int DB_VERSION_NOTE = 1;

    // таблиця для об'єкта Background
    public class Background {
        public static final String TABLE_NAME = "background";
        public static final String ID = "id";
        public static final String PATH = "path";
        public static final String COLOR = "color";
        public static final String IS_DEFAULT = "is_default";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " (" + ID + " integer primary key autoincrement, " + PATH + " varchar(255), " +
                COLOR + " varchar(255), " + IS_DEFAULT + " integer);";
    }

    // таблиця для об'єкта Post
    public class Post {

        public static final String TABLE_NAME = "post";
        public static final String ID = "id";
        public static final String TITLE = "title";
        public static final String POST_TEXT = "post_text";
        public static final String RATING = "rating";
        public static final String ID_MARKER = "id_marker";
        public static final String IMAGE = "image";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " ( " + ID + " integer primary key autoincrement, " +  TITLE +
                " varchar(255), " + POST_TEXT + " varchar(8384), " + RATING +
                " float, " + ID_MARKER + " integer, " + IMAGE + " varchar(128));";
    }



    // таблиця для об'єкта File
    public class File {

        public static final String TABLE_NAME = "file";
        public static final String ID = "id";
        public static final String FILE = "file";
        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " ( " + ID + " integer primary key autoincrement, " + FILE +
                " varchar(255));";

    }

    // таблиця для об'єкта PostFile
    public class PostFile {
        public static final String TABLE_NAME = "post_file";
        public static final String ID = "id";
        public static final String ID_POST = "id_post";
        public static final String ID_FILE = "id_file";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " ( " + ID + " integer primary key autoincrement, " +
                ID_POST + " integer, " + ID_FILE + " integer);";
    }


    // таблиця для об'єкта GPSMarker
    public class GPSMarker {
        public static final String TABLE_NAME = "gps_marker";
        public static final String ID = "id";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " ( " + ID + " integer primary key autoincrement, " +
                LATITUDE + " double,  " + LONGITUDE + " double);";

    }

    // таблиця для об'єкта Group
    public class Group {
        public static final String TABLE_NAME = "grp_table";
        public static final String ID = "id";
        public static final String GROUP_ID = "group_id";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " ( " + ID + " integer primary key autoincrement, " + GROUP_ID + " long);";
    }

    // таблиця для об'єкта  PostGroup
    public class PostGroup {
        public static final String TABLE_NAME = "post_group";
        public static final String ID = "id";
        public static final String GROUP_ID = "group_id";
        public static final String POST_ID = "post_id";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " ( " + ID + " integer primary key autoincrement, " + GROUP_ID + " long, " + POST_ID + " long);";

    }

    // таблиця для об'єкта User
    public class User{

        public static final String TABLE_NAME = "user";
        public static final String ID= "id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String SEX = "sex";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String BIRTHDAY = "birthday";
        public static final String COUNTRY = "country";
        public static final String IS_LOGIN = "is_login";
        public static final String IS_ONLINE = "is_online";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME +
                " ( " + ID + " integer primary key autoincrement, " + FIRST_NAME + " varchar(64), " + LAST_NAME +
                " varchar(64), " + SEX + " integer, " + EMAIL + " varchar(108), " + PASSWORD +
                " varchar(64), " + BIRTHDAY + " varchar(12), " + COUNTRY + " varchar(64), " + IS_LOGIN + " integer, " + IS_ONLINE + " integer); ";

    }
}