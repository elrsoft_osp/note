package com.elrsoft.note.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.core.Dao;
import com.elrsoft.note.model.Background;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 19.03.2015.
 */
public class BackgroundDao implements Dao<Background> {

    SQLiteDatabase sqLiteDatabase;

    public BackgroundDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }


    // вносимо запис background в відповідну таблицю бази даних
    @Override
    public long save(Background background) {
        return sqLiteDatabase.insert(DBResource.Background.TABLE_NAME, null, fullBackgroundContentValue(background));

    }

    // видаляємо запис про відповідний background із таблиці бази даних
    @Override
    public long delete(Background background) {

        return sqLiteDatabase.delete(DBResource.Background.TABLE_NAME, DBResource.Background.ID + " = ?",
                new String[]{String.valueOf(background.getId())});
    }

    // оновлюємо запис відповідного background-а в базі даних
    @Override
    public int update(Background background) {
        return sqLiteDatabase.update(DBResource.Background.TABLE_NAME, fullBackgroundContentValue(background), DBResource.Background.ID + " = ?",
                new String[]{String.valueOf(background.getId())});
    }

    // отримуємо список усіх записів із таблиці background в базі даних
    @Override
    public List<Background> get() {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Background.TABLE_NAME, null);
        List<Background> backgroundList = parseCursor(cursor);
        return backgroundList;

    }

    // отримуємо запис із таблиці background, id якого відповідає аргументу, що передаєм в метод
    @Override
    public Background getBy(long id) {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Background.TABLE_NAME + " where " + DBResource.Background.ID + "= ?", new String[]{String.valueOf(id)});
        if (!parseCursor(cursor).isEmpty()) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }

    }

    // навігація по таблиці Background в базі даних
    @Override
    public List<Background> parseCursor(Cursor cursor) {

        List<Background> backgroundList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(DBResource.Background.ID));
                String path = cursor.getString(cursor.getColumnIndex(DBResource.Background.PATH));
                String color = cursor.getString(cursor.getColumnIndex(DBResource.Background.COLOR));
                int isDefaunt = cursor.getInt(cursor.getColumnIndex(DBResource.Background.IS_DEFAULT));

                backgroundList.add(new Background(id, path, color, isDefaunt));

            } while (cursor.moveToNext());

        }

        return backgroundList;

    }

    public Background getByDefault(){
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Background.TABLE_NAME + " where " + DBResource.Background.IS_DEFAULT + "= ?", new String[]{String.valueOf(1)});
        List<Background> list = parseCursor(cursor);
        if (!list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }

    }

    // створюємо об'єкт ContentValues що містить дані про звичайний об'єкт background
    private ContentValues fullBackgroundContentValue(Background background) {
        ContentValues cv = new ContentValues();
        cv.put(DBResource.Background.PATH, background.getPath());
        cv.put(DBResource.Background.COLOR, background.getColor());
        cv.put(DBResource.Background.IS_DEFAULT, background.getIsDefault());

        return cv;
    }

}
