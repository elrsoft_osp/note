package com.elrsoft.note.db.service;

import android.content.Context;

import com.elrsoft.note.db.dao.FileDao;
import com.elrsoft.note.db.dao.PostDao;
import com.elrsoft.note.db.dao.PostFileDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.model.PostFile;

import java.util.List;

/**
 * Created by vika on 23.03.15.
 */
public class PostFileService extends ServiceOpenDB {
    Context context;

    public PostFileService(Context context) {
        this.context = context;
    }


    //відкриваємо БД, викликаємо метод save, що зберігає запис про об'єкт postFile в БД, після чого закриваємо БД
    public long save(long postId, long fileId) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostFileDao(getSqLiteDatabase()).save(postId, fileId);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }


    //відкриваємо БД, метод delete видаляє запис про відповідний postFile із таблиці бази даних, після чого закриває БД
    public long delete(long postId, long fileId) {
        try {
            if (!isOpenDB()) {
                open(context);
            }

            return new PostFileDao(getSqLiteDatabase()).remove(postId, fileId);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public long delete(Post post) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostFileDao(getSqLiteDatabase()).removeAll(post);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }


    //відкриваємо БД, метод update оновлює запис з таблиці PostFile, після чого закриває БД
    public int update(long postId, long fileId) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostFileDao(getSqLiteDatabase()).update(postId, fileId);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }


    // відкриваємо БД, викликає метод, що повертає список усіх записів таблиці PostFile в БД, після чого закриваємо БД
    public PostFile getAll(PostFile postFile) {

        try {
            if (!isOpenDB()) {
                open(context);
            }

            return new PostFileDao(getSqLiteDatabase()).get(postFile.getPost().getId(),
                    postFile.getFile().getId());

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриваємо БД, викликає метод, що повертає запис запитаного об'єкта postFile, після чого закриваємо БД
    public PostFile getById(PostFile postFile) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostFileDao(getSqLiteDatabase()).get(postFile.getPost().getId(),
                    postFile.getFile().getId());
        } finally {
            if (isOpenDB()) {
                close();
            }

        }
    }

    // відкриває БД, викликає метод, що повертає список усіх постів для запитаного файла, після чого закриває БД
    public List<Post> getBy(File file) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostFileDao(getSqLiteDatabase()).getBy(file);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає список усіх файлів для вказаного поста, після чого закриває БД
    public List<Long> getBy(long postId) {

        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new PostFileDao(getSqLiteDatabase()).getBy(postId);

        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

}

