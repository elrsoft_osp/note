package com.elrsoft.note.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.elrsoft.note.db.service.BackgroundService;
import com.elrsoft.note.model.Background;

/**
 * Created by Markevych on 28.02.2015.
 */
public class DBHelper extends SQLiteOpenHelper
{
    Context context;

    public DBHelper(Context context) {
        super(context, DBResource.DB_NAME_NOTE, null, DBResource.DB_VERSION_NOTE);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DBResource.Post.CREATE_TABLE);
        db.execSQL(DBResource.File.CREATE_TABLE);
        db.execSQL(DBResource.PostFile.CREATE_TABLE);
        db.execSQL(DBResource.GPSMarker.CREATE_TABLE);
        db.execSQL(DBResource.Group.CREATE_TABLE);
        db.execSQL(DBResource.PostGroup.CREATE_TABLE);
        db.execSQL(DBResource.User.CREATE_TABLE);
        db.execSQL(DBResource.Background.CREATE_TABLE);

        db.insert(DBResource.Background.TABLE_NAME, null, fullBackgroundContentValue(new Background(0, "fon/for_girl.jpg", null, 1)));
        db.insert(DBResource.Background.TABLE_NAME, null, fullBackgroundContentValue(new Background(0, "fon/gray_roses.jpg", null, 0)));
        db.insert(DBResource.Background.TABLE_NAME, null, fullBackgroundContentValue(new Background(0, "fon/normal.jpg", null, 0)));
        db.insert(DBResource.Background.TABLE_NAME, null, fullBackgroundContentValue(new Background(0, "fon/paris.jpg", null, 0)));
        db.insert(DBResource.Background.TABLE_NAME, null, fullBackgroundContentValue(new Background(0, "fon/wack.jpg", null, 0)));
        db.insert(DBResource.Background.TABLE_NAME, null, fullBackgroundContentValue(new Background(0, "fon/wall.jpg", null, 0)));

    }

    private ContentValues fullBackgroundContentValue(Background background) {
        ContentValues cv = new ContentValues();
        cv.put(DBResource.Background.PATH, background.getPath());
        cv.put(DBResource.Background.COLOR, background.getColor());
        cv.put(DBResource.Background.IS_DEFAULT, background.getIsDefault());

        return cv;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}