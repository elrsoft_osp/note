package com.elrsoft.note.db.dao.core;

import android.database.Cursor;

import java.util.List;

/**
 * Created by Yana on 19.03.2015.
 */
public interface Dao<T> {

    long save(T t);

    long delete(T t);

    int update(T t);

    List<T> get();

    T getBy(long id);

    List<T> parseCursor(Cursor cursor);
}