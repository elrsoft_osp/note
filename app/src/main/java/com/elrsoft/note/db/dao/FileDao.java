package com.elrsoft.note.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.core.Dao;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 20.03.2015.
 */
public class FileDao implements Dao<File> {

    SQLiteDatabase sqLiteDatabase;

    public FileDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    // вносимо запис file в відповідну таблицю бази даних
    @Override
    public long save(File file) {

        return sqLiteDatabase.insert(DBResource.File.TABLE_NAME, null, fullFileContentValue(file));

    }

    // видаляємо запис про відповідний file із таблиці бази даних
    @Override
    public long delete(File file) {

        return sqLiteDatabase.delete(DBResource.File.TABLE_NAME, DBResource.File.ID + " = ?",
                new String[]{String.valueOf(file.getId())});

    }

    public long delete(long fileId) {

        return sqLiteDatabase.delete(DBResource.File.TABLE_NAME, DBResource.File.ID + " = ?",
                new String[]{String.valueOf(fileId)});

    }

    // оновлюємо запис відповідного file-а в базі даних
    @Override
    public int update(File file) {

        return sqLiteDatabase.update(DBResource.File.TABLE_NAME, fullFileContentValue(file), DBResource.File.ID + " = ?",
                new String[]{String.valueOf(file.getId())});

    }

    // отримуємо список усіх записів із таблиці file в базі даних
    @Override
    public List<File> get() {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.File.TABLE_NAME, null);
        List<File> fileList = parseCursor(cursor);
        return fileList;

    }

    // отримуємо запис із таблиці file, id якого відповідає аргументу, що передаєм в метод
    @Override
    public File getBy(long id) {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.File.TABLE_NAME + " where " + DBResource.File.ID + "= ?", new String[]{String.valueOf(id)});
        if (cursor.moveToFirst()) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }

    }

    // навігація по таблиці file в базі даних
    @Override
    public List<File> parseCursor(Cursor cursor) {

        List<File> fileList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {

                String file = cursor.getString(cursor.getColumnIndex(DBResource.File.FILE));
                fileList.add(new File(file));

            } while (cursor.moveToNext());

        }
        if (cursor != null) {
            cursor.close();
        }
        return fileList;

    }

    // створюємо об'єкт ContentValues що містить дані про звичайний об'єкт file
    private ContentValues fullFileContentValue(File file) {
        ContentValues cv = new ContentValues();
        cv.put(DBResource.File.FILE, file.getPath());

        return cv;
    }
}
