package com.elrsoft.note.db.service;

import android.content.Context;

import com.elrsoft.note.db.dao.FileDao;
import com.elrsoft.note.db.dao.PostDao;
import com.elrsoft.note.db.service.core.BaseService;
import com.elrsoft.note.db.service.core.ServiceOpenDB;
import com.elrsoft.note.model.File;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 23.03.2015.
 */
public class FileService extends ServiceOpenDB implements BaseService<File> {

    Context context;

    public FileService(Context context){
        this.context = context;
    }

    // відкриває БД, викликає метод, що зберігає запис про об'єкт file в БД, після чого закриває БД
    @Override
    public long save(File file) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new FileDao(getSqLiteDatabase()).save(file);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що видаляє запис про об'єкт file в БД, після чого закриває БД
    @Override
    public long delete(File file) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new FileDao(getSqLiteDatabase()).delete(file);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public long delete(long fileId) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new FileDao(getSqLiteDatabase()).delete(fileId);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що оновлює запис про об'єкт file в БД, після чого закриває БД
    @Override
    public int update(File file) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new FileDao(getSqLiteDatabase()).update(file);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає список усіх записів таблиці file в БД, після чого закриває БД
    @Override
    public List<File> getAll() {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new FileDao(getSqLiteDatabase()).get();
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    // відкриває БД, викликає метод, що повертає запис запитаного об'єкта file, після чого закриває БД
    @Override
    public File getById(File file) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new FileDao(getSqLiteDatabase()).getBy(file.getId());
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public File getById(long fileId) {
        try {
            if (!isOpenDB()) {
                open(context);
            }
            return new FileDao(getSqLiteDatabase()).getBy(fileId);
        } finally {
            if (isOpenDB()) {
                close();
            }
        }
    }

    public List<File> getListFileToListId(List<Long> listIdFile){
        List<File> list = new ArrayList<>();
        try {
            if (!isOpenDB()) {
                open(context);
            }
            for(long i : listIdFile) {
                list.add(new FileService(context).getById(i));
            }

            return list;

        } finally {
            if (isOpenDB()) {
                close();
            }
    }
    }
}
