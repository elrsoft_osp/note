package com.elrsoft.note.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.dao.core.Dao;
import com.elrsoft.note.model.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 19.03.2015.
 */
public class PostDao implements Dao<Post> {

    SQLiteDatabase sqLiteDatabase;


    public PostDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }


    // вносимо запис post в відповідну таблицю бази даних
    @Override
    public long save(Post post) {
        Log.d("MyLog", "contentValuev = "+ fullPostContentValue(post));
        return sqLiteDatabase.insert(DBResource.Post.TABLE_NAME, null, fullPostContentValue(post));
    }

    // видаляємо запис про відповідний post із таблиці бази даних
    @Override
    public long delete(Post post) {

        return sqLiteDatabase.delete(DBResource.Post.TABLE_NAME, DBResource.Post.ID + " = ?",
                new String[]{String.valueOf(post.getId())});

    }

    // оновлюємо запис відповідного post-a в базі даних
    @Override
    public int update(Post post) {
        return sqLiteDatabase.update(DBResource.Post.TABLE_NAME, fullPostContentValue(post), DBResource.Post.ID + " = ?",
                new String[]{String.valueOf(post.getId())});

    }

    // отримуємо список усіх записів із таблиці post в базі даних
    @Override
    public List<Post> get() {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Post.TABLE_NAME, null);
        List<Post> authList = parseCursor(cursor);

        return authList;

    }

    // отримуємо запис із таблиці post, id якого відповідає аргументу, що передаєм в метод
    @Override
    public Post getBy(long id) {

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Post.TABLE_NAME + " where " + DBResource.Post.ID + "= ?", new String[]{String.valueOf(id)});
        if (cursor.moveToFirst()) {
            return parseCursor(cursor).get(0);
        } else {
            return null;
        }

    }

    public int getCountPost(){
        return get().size();
    }

    // навігація по таблиці post в базі даних
    @Override
    public List<Post> parseCursor(Cursor cursor) {

        List<Post> postsList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DBResource.Post.ID));
                String title = cursor.getString(cursor.getColumnIndex(DBResource.Post.TITLE));
                String postText = cursor.getString(cursor.getColumnIndex(DBResource.Post.POST_TEXT));
                float rating = cursor.getFloat(cursor.getColumnIndex(DBResource.Post.RATING));
                long markerId = cursor.getLong(cursor.getColumnIndex(DBResource.Post.ID_MARKER));
                String image = cursor.getString(cursor.getColumnIndex(DBResource.Post.IMAGE));
                Log.d("MyLog", id + " " + title + " " + postText + " " + rating + " " + markerId + " " + image);
                postsList.add(new Post(id, title, postText, rating, markerId, image));

            } while (cursor.moveToNext());

        }

        return postsList;

    }

    public List<Post> getByName(String name) {
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DBResource.Post.TABLE_NAME + " where " + DBResource.Post.TITLE + " LIKE ? ", new String[]{"%" + name + "%"});
        if (cursor.moveToFirst()) {
            return parseCursor(cursor);
        } else {
            return null;
        }
    }

    // створюємо об'єкт ContentValues що містить дані про звичайний об'єкт post
    private ContentValues fullPostContentValue(Post post) {
        ContentValues cv = new ContentValues();
        cv.put(DBResource.Post.TITLE, post.getTitle());
        cv.put(DBResource.Post.POST_TEXT, post.getPostText());
        cv.put(DBResource.Post.RATING, post.getRating());
        cv.put(DBResource.Post.ID_MARKER, post.getMarkerId());
        cv.put(DBResource.Post.IMAGE, String.valueOf(post.getTitleImage()));
        return cv;
    }

}
