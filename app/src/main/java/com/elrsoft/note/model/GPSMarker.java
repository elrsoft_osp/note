package com.elrsoft.note.model;

/**
 * Created by vika on 27.02.15.
 */
public class GPSMarker {
    long id;
    double latitude;
    double longitude;

    public GPSMarker() {
    }

    public GPSMarker(long id) {
        this.id = id;
    }

    public GPSMarker(long id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
