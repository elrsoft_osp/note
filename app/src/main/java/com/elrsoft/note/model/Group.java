package com.elrsoft.note.model;

/**
 * Created by Yana on 20.03.2015.
 */
public class Group {

    long groupId;

    public  Group(){}

    public Group(long groupId){
        this.groupId = groupId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }




}
