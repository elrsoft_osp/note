package com.elrsoft.note.model;


import java.util.List;

/**
 * Created by vall on 03.03.15.
 */
public class PostGroup {
    private Post post;

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

    private List<Group> groupList;
    private Group group;


public PostGroup(Post post, List<Group> groupList){
    this.post = post;
    this.groupList = groupList;
}

    public PostGroup(Post post, Group group) {
        this.post = post;
        this.group = group;



    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Group getGroup() {
        return group;
    }


    public void setGroup(Group group) {
        this.group = group;
    }
}
