package com.elrsoft.note.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yana on 25.02.2015.
 */
public class Post implements Parcelable {
    private long id;
    private String title, postText;
    private String titleImage;
    private long markerId;
    private float rating;

    public Post(float rating) {
        this.rating = rating;
    }

    public Post(long id) {
        this.id = id;
    }

    public Post(String title, String postText) {
        this.title = title;
        this.postText = postText;
    }


    public Post(long id, String title, String postText, float rating, long markerId, String titleImage) {
        this.id = id;
        this.title = title;
        this.postText = postText;
        this.rating = rating;
        this.markerId = markerId;
        this.titleImage = titleImage;
    }


    public Post(String title, int rating, String postText) {
        this.title = title;
        this.rating = rating;
        this.postText = postText;
    }


    public Post() {

    }

    public String getTitle() {
        return title;
    }

    public long getMarkerId() {
        return markerId;
    }

    public void setMarkerId(long markerId) {
        this.markerId = markerId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImage) {
        this.titleImage = titleImage;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(postText);
        dest.writeString(titleImage);
        dest.writeLong(markerId);
        dest.writeFloat(rating);

    }

    private Post(Parcel parcel) {
        id = parcel.readLong();
        title = parcel.readString();
        postText = parcel.readString();
        titleImage = parcel.readString();
        markerId = parcel.readLong();
        rating = parcel.readFloat();
    }
}
