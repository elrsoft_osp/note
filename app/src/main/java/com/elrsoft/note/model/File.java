package com.elrsoft.note.model;

/**
 * Created by Yana on 25.02.2015.
 */
public class File {

    private String path;
    private long id;


    public File() {
    }

    public File(String path, long id) {

        this.path = path;
        this.id = id;
    }

    public File(long id) {
        this.id = id;

    }

    public File( String path) {
        this.path = path;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
