package com.elrsoft.note.model;

/**
 * Created by Markevych on 28.02.2015.
 */
public class Background {
    private int id;
    private String path;
    private String color;
    private int isDefault;


    public Background(){}

    public Background(int id, String path, String color, int isDefault){
        this.id=id;
        this.path=path;
        this.color=color;
        this.isDefault=isDefault;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}