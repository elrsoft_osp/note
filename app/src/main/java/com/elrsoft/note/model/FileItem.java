package com.elrsoft.note.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Yana on 02.03.2015.
 */
public class FileItem implements Comparable<FileItem>, Parcelable{
    private String fileName;
    private String fileData;
    private String fileDate;
    private String filePath;
    private String fileImage;

    public FileItem(String fileName,String fileData, String fileDate, String filePath, String fileImage)
    {
        this.fileName = fileName;
        this.fileData = fileData;
        this.fileDate = fileDate;
        this.filePath = filePath;
        this.fileImage = fileImage;
    }

    public  FileItem(){}

    public String getName()
    {
        return fileName;
    }
    public String getData()
    {
        return fileData;
    }
    public String getDate()
    {
        return fileDate;
    }
    public String getPath()
    {
        return filePath;
    }
    public String getImage() {
        return fileImage;
    }
    public int compareTo(FileItem o) {
        if(this.fileName != null)
            return this.fileName.toLowerCase().compareTo(o.getName().toLowerCase());
        else
            throw new IllegalArgumentException();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FileItem> CREATOR = new Creator<FileItem>() {
        @Override
        public FileItem createFromParcel(Parcel source) {
            return new FileItem(source);
        }

        @Override
        public FileItem[] newArray(int size) {
            return new FileItem[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fileName);
        dest.writeString(fileData);
        dest.writeString(fileDate);
        dest.writeString(filePath);
        dest.writeString(fileImage);

    }

    private FileItem(Parcel parcel) {
        fileName = parcel.readString();
        fileData = parcel.readString();
        fileDate = parcel.readString();
        filePath = parcel.readString();
        fileImage = parcel.readString();
    }

}