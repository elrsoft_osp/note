package com.elrsoft.note.model;

import com.elrsoft.note.db.dao.PostFileDao;

/**
 * Created by vall on 03.03.15.
 */
public class PostFile {

    private Post post;
    private File file;
    private long id;


    public PostFile() {

    }

    public PostFile(Post post, File file) {
        this.post = post;
        this.file = file;
    }

    public PostFile(long id, Post post, File file) {
        this.id = id;
        this.post = post;
        this.file = file;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }


}
