package com.elrsoft.note.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ImageController;
import com.elrsoft.note.controller.PhotoSizeController;
import com.elrsoft.note.controller.ViewImageMangerController;
import com.elrsoft.note.model.Background;

import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 02.03.2015.
 */
public class ImageGridAdapter extends BaseAdapter {
    List<Background> list;
    LayoutInflater inflater;
    Activity activity;

    private class ViewHolder {
        ImageView image;
    }

    public ImageGridAdapter(List<Background> list, Activity activity) {
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        //тут відбувається магія, ці строчки не чіпаки
        ViewHolder holder = new ViewHolder();
        holder.image = ViewImageMangerController.createImageViewForGrigView(activity);

        int px = activity.getResources().getDimensionPixelSize(R.dimen.image_size2);
        if (position < 6 || position == list.size() - 1) {
            holder.image.setImageBitmap(ImageController.getBitmapFromAsset(activity, list.get(position).getPath()));
        } else {
            holder.image.setImageBitmap(new PhotoSizeController().decodeSampledBitmapFromResource(list.get(position).getPath(), px, px));

        }

        return holder.image;
    }
}