package com.elrsoft.note.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.model.Draw;
import com.elrsoft.note.view.MvBoliTextView;

import java.util.List;
import java.util.Map;

/**
 * Created by vika on 01.03.15.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {
    Map<String, List<Draw>> map;
    Context context;

    public ExpandableListAdapter(Context context, Map<String, List<Draw>> map) {
        this.context = context;
        this.map = map;

    }


    @Override
    public int getGroupCount() {
        return map.keySet().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return map.get(map.keySet().toArray()[groupPosition]).size();
    }

    @Override
    public String[] getGroup(int groupPosition) {
        return (String[]) (map.keySet().toArray());
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return map.get(map.keySet().toArray()[groupPosition]).get(childPosition).getName();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.group_item, null);
        }
        TextView
                textView = (TextView) convertView.findViewById(R.id.txtGroupItem);

        String name = (String) map.keySet().toArray()[groupPosition];

        if (name != null) {

            textView.setText(name);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.d("Domain", "id " + groupPosition);
        ViewHolder holder = null;
        if (map.keySet().toArray()[groupPosition].equals("Change color")) {
            convertView = layoutInflater.inflate(R.layout.group_item_color_child, null);
            if (holder == null) {
                holder = new ViewHolder();
                holder.textView = (MvBoliTextView) convertView.findViewById(R.id.txtGroupItemChild);
                holder.color = (ImageView) convertView.findViewById(R.id.image_icon_color);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
        } else if (map.keySet().toArray()[groupPosition].equals("Change style")) {
            convertView = layoutInflater.inflate(R.layout.group_item_child, null);
            if (holder == null) {
                holder = new ViewHolder();
                holder.textView = (MvBoliTextView) convertView.findViewById(R.id.txtGroupItemChild);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
        } else {
            convertView = layoutInflater.inflate(R.layout.group_item_child, null);
            if (holder == null) {
                holder = new ViewHolder();
                holder.textView = (MvBoliTextView) convertView.findViewById(R.id.txtGroupItemChild);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
        }

        String name = (String) getChild(groupPosition, childPosition);
        if (name.equals("Black "))
            holder.color.setImageResource(R.drawable.black);
        if (name.equals("Red "))
            holder.color.setImageResource(R.drawable.red);
        if (name.equals("Blue "))
            holder.color.setImageResource(R.drawable.blue);

        if (name != null) {
            holder.textView.setText(name);


        }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private class ViewHolder {
        MvBoliTextView textView;
        ImageView color;
    }
}
