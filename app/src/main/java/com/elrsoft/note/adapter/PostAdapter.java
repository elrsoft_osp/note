package com.elrsoft.note.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ImageController;
import com.elrsoft.note.controller.PhotoSizeController;
import com.elrsoft.note.controller.SpannableController;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.db.service.PostFileService;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.dialog.ShareDialogFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.view.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 25.02.2015.
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    List<Post> posts;
    LayoutInflater lInflater;
    OnItemClickListener onItemClickListener;
    MainFragment mainFragment;
    RatingBar ratingBar;
    FragmentActivity activity;

    public PostAdapter(List<Post> posts, MainFragment mainFragment, FragmentActivity activity) {
        this.posts = posts;
        this.mainFragment = mainFragment;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = lInflater.from(parent.getContext()).inflate(R.layout.item_post_adapter, null, false);
        final ViewHolder viewHolder = new ViewHolder(view);




        ((TextView) view.findViewById(R.id.share)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Hello, from tutorialspoint");
                mainFragment.getActivity().startActivity(Intent.createChooser(shareIntent, "Share your thoughts"));
            }
        });

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = posts.get(position);
        holder.title.setText(post.getTitle());
        //якщо картинка з assets
        Bitmap bitmap = ImageController.getBitmapFromAsset(mainFragment.getActivity(), post.getTitleImage());
        if(bitmap == null){
            holder.titleImage.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(post.getTitleImage(), mainFragment.getResources().getDimensionPixelSize(R.dimen.image_size), mainFragment.getResources().getDimensionPixelSize(R.dimen.image_size)));
        }else {
            holder.titleImage.setImageBitmap(bitmap);
        }

        if(post.getPostText() != null && post.getPostText().length() >= 255) {
            holder.postText.setText(post.getPostText().substring(0, 255));
        } else {
            holder.postText.setText(post.getPostText());
        }
        holder.postText.setText(SpannableController.validSmile(holder.postText.getText().toString()));
        //декодування смайлів
        holder.postText.setText(SpannableController.getSmiledText(mainFragment.getActivity(), holder.postText.getText()));

        holder.ratingBar.setRating(post.getRating());




    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Post getItemById(int position){
        return posts.get(position);
    }

    @Override
    public int getItemCount() {
        try {
            return posts.size();
        } catch (NullPointerException e){
            return 0;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, postText;
        public SelectableRoundedImageView titleImage;
        final View view1;
        public RatingBar ratingBar;



        public ViewHolder(View view) {
            super(view);
            this.view1 = view;



            ((CardView) view.findViewById(R.id.recycler_view)).setPreventCornerOverlap(false);
            title = (TextView) view.findViewById(R.id.title);
            titleImage = (SelectableRoundedImageView) view.findViewById(R.id.titleImage);
            postText = (TextView) view.findViewById(R.id.postText);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            view.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Post post = posts.get(getAdapterPosition());
                    List<Long> filesId = new PostFileService(activity.getApplicationContext()).getBy(post.getId());
                    List<File> filesList = new FileService(activity.getApplicationContext()).getListFileToListId(filesId);
                    ArrayList<Uri> uriList = new ArrayList<>();

                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, post.getTitle());
                    shareIntent.putExtra(Intent.EXTRA_TEXT, post.getPostText());

//                    for(File temp: filesList){
//                        if(!temp.getPath().contains(".jpg") || temp.getPath().contains(".png")){
//                            uriList.add(Uri.fromFile(new java.io.File(temp.getPath())));
//                        }
//                    }
//
//                    shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);
                    mainFragment.getActivity().startActivity(Intent.createChooser(shareIntent, "Share your thoughts"));
                }
            });

            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                    try{
                        Post post = posts.get(getAdapterPosition());
                        post.setRating(rating);
                        new PostService(view1.getContext()).update(post);

                    } catch (IndexOutOfBoundsException e){

                    }
                }
            });

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(v, getPosition());
            }

        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

    }

    public void setOnItemClickListener(final OnItemClickListener itemClickListener) {
        onItemClickListener = itemClickListener;

    }
}
