package com.elrsoft.note.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.elrsoft.note.R;
import com.elrsoft.note.resource.Resource;

/**
 * Created by <ELRsoft> HANZ on 01.04.2015.
 */
public class SmileAdapter extends BaseAdapter {
    LayoutInflater inflater;
    Activity activity;

    private class ViewHolder {
        ImageView image;
    }

    public SmileAdapter(Activity activity) {
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Resource.Smile.emoticons.size();
    }

    @Override
    public Object getItem(int position) {
        return Resource.Smile.emoticons.get(Resource.Smile.emoticons.keySet().toArray()[position]);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_image_grid_adapter, null, false);
            holder.image = (ImageView) view.findViewById(R.id.imageGridAdapter);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (holder != null) {
            holder.image.setImageResource((int) getItem(position));
        }
        return view;
    }
}
