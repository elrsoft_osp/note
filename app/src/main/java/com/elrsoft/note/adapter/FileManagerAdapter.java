package com.elrsoft.note.adapter;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.model.FileItem;

import java.util.List;

/**
 * Created by Yana on 02.03.2015.
 */


public class FileManagerAdapter extends BaseAdapter {

    Context context;
    List<FileItem> fileItems;

    public FileManagerAdapter(Context context,  List<FileItem> fileItems){

        this.context = context;
        this.fileItems = fileItems;
    }
    @Override
    public int getCount() {
        return fileItems.size();
    }

    @Override
    public FileItem getItem(int position) {
        return fileItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        View view = convertView;
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.file_manager_item, null);
            holder = new ViewHolder();
            holder.dirName = (TextView) view.findViewById(R.id.file_name);
            holder.dirContext = (TextView) view.findViewById(R.id.file_context);
            holder.dirDate = (TextView) view.findViewById(R.id.file_date);
            holder.dirImage = (ImageView) view.findViewById(R.id.file_icon);
            view.setTag(holder);

        }else{
            holder = (ViewHolder)view.getTag();
        }

        final FileItem fileItem = fileItems.get(position);
        if (fileItem != null) {
            String uri = "drawable/" + fileItem.getImage();
            int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
            Log.d("image", "   "+fileItem.getImage());
            Drawable image = context.getResources().getDrawable(imageResource);
            holder.dirImage.setImageDrawable(image);

            if(holder.dirName!=null)
                holder.dirName.setText(fileItem.getName());
            if(holder.dirContext!=null)
                holder.dirContext.setText(fileItem.getData());
            if(holder.dirDate!=null)
                holder.dirDate.setText(fileItem.getDate());
        }

        return view;
    }

    private  class ViewHolder{
        TextView dirName, dirContext, dirDate;
        ImageView dirImage;
    }

}