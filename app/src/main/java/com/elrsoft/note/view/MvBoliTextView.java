package com.elrsoft.note.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by <ELRsoft> HANZ on 26.02.2015.
 */
public class MvBoliTextView extends TextView {
    Context c;

    public MvBoliTextView(Context context) {
        super(context);
        c = context;
        init();
    }

    public MvBoliTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        c = context;
        init();
    }

    public MvBoliTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        c = context;
        init();
    }

    @Override
    public boolean isInEditMode() {
        init();
        return super.isInEditMode();

    }

    public void init() {
        Typeface face = Typeface.createFromAsset(c.getAssets(),
                "fonts/InformalRoman.ttf");
        this.setTypeface(face);
    }

}
