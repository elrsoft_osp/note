package com.elrsoft.note.core;

import android.graphics.Bitmap;
import android.support.v4.app.DialogFragment;

/**
 * Created by <ELRsoft> HANZ on 11.03.2015.
 */
public abstract class DialogFragmentListener extends DialogFragment {

   public OnClickDialogFragment onClickDialogFragment;


    public void setOnClickDialogFragment(OnClickDialogFragment onClickDialogFragment) {
        this.onClickDialogFragment = onClickDialogFragment;
    }

    public interface OnClickDialogFragment {
        public void onClick(String parametr);
    }

}
