package com.elrsoft.note.core;

import android.graphics.Bitmap;
import android.support.v4.app.DialogFragment;

/**
 * Created by <ELRsoft> HANZ on 11.03.2015.
 */
public abstract class ListenerImageNavigator extends DialogFragment {

   public OnClickListenerImageNavigator onClickListenerImageNavigator;


    public void setOnClickListenerImageNavigator(OnClickListenerImageNavigator onClickListenerImageNavigator) {
        this.onClickListenerImageNavigator = onClickListenerImageNavigator;
    }

    public interface OnClickListenerImageNavigator {
        public void onClick(String path);
    }

}
