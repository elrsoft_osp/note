package com.elrsoft.note.controller;

import android.view.animation.Animation;
import android.widget.ImageButton;

import com.software.shell.fab.ActionButton;

/**
 * Created by <ELRsoft> HANZ on 22.03.2015.
 */
public class AnimButtonCreatePostController {

    private ActionButton button;
    private boolean finish = true;

    public AnimButtonCreatePostController(ActionButton button) {
        this.button = button;
    }

    public void startAnim(Animation animation, final int visibl) {
        if (visibl != button.getVisibility() && finish) {
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    finish = false;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    button.setVisibility(visibl);
                    finish = true;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            button.startAnimation(animation);
        }

    }
}
