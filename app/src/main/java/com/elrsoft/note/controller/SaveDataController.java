package com.elrsoft.note.controller;

import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.elrsoft.note.R;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.db.service.GPSMarkerService;
import com.elrsoft.note.db.service.PostFileService;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.GPSMarker;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.resource.Resource;

import java.util.Map;

/**
 * Created by Yana on 01.04.2015.
 */
public class SaveDataController {

    // зберігає/редагує пост
    public void savePost(FragmentActivity activity, Post post, Map<String, String> map) {

        GPSMarkerService markerService = new GPSMarkerService(activity);
        GPSMarker marker = GPSController.getMarker();
        FileService fileService = new FileService(activity);
        File file;
        String title = TextValidationController.validPostData(map.get("title"));
        String postText = map.get("postText");
        String photoPath = map.get("photoPath");
        String videoPath = map.get("videoPath");
        String imagePath = map.get("imagePath");
        String audioPath = map.get("audioPath");

        // Виконується при створенні нового поста
        if (CreatePostController.getPost() == null) {


                Resource.PostData.pathToTitleImage = null;
                //якщо немаэ картинки додаємо свою
                if (photoPath == null) {

                    photoPath = TextFindController.getRandomImage(postText);
                    Log.d("...", "Hello" + photoPath);
                }

                post = new Post(0, title, postText, 0, 0, photoPath);

                if (marker != null) {
                    Log.d("MyLog", "getLatitude = " + marker.getLatitude() + "; getLongitude = " + marker.getLongitude());
                    post.setMarkerId(markerService.save(marker));
                }
                post = new Post(new PostService(activity).save(post));

                if (videoPath != null && !videoPath.equals("")) {
                    file = new File(fileService.save(new File(videoPath)));
                    new PostFileService(activity).save(post.getId(), file.getId());
                    Resource.PostData.pathToVideo = null;
                }
                if (imagePath != null && !imagePath.equals("")) {
                    Log.d("on save data", "image path = " + imagePath);
                    file = new File(fileService.save(new File(imagePath)));
                    new PostFileService(activity).save(post.getId(), file.getId());
                    Resource.PostData.pathToImage = null;
                }
                if (audioPath != null && !audioPath.equals("")) {
                    Log.d("on save data", "audio path = " + audioPath);
                    file = new File(fileService.save(new File(audioPath)));
                    new PostFileService(activity).save(post.getId(), file.getId());
                    Resource.PostData.pathToAudio = null;
                }
                CreatePostController.setPost(null);
                ViewPostController.setPost(null);
                SaveDataController.setPostDataToNull();
                FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));


        }
        //Виконується при редагуванні існуючого поста
        else {

            new UpdatePostController(map, post, marker, activity);
            SaveDataController.setPostDataToNull();
        }
    }

    public static void setPostDataToNull() {
        CreatePostController.setPost(null);
        Resource.PostData.pathToTitleImage = null;
        Resource.PostData.pathToImage = null;
        Resource.PostData.pathToAudio = null;
        Resource.PostData.pathToVideo = null;
    }

}
