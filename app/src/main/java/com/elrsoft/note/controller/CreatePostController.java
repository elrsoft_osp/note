package com.elrsoft.note.controller;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.db.service.PostFileService;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.dialog.AddFileManagerDialogFragment;
import com.elrsoft.note.dialog.ChooseFromDialogFragment;
import com.elrsoft.note.dialog.RecordAudioDialogFragment;
import com.elrsoft.note.dialog.UpdateFileDialogFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.fragment.SmileFragment;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.resource.Resource;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yana on 16.04.2015.
 */
public class CreatePostController implements View.OnClickListener, View.OnLongClickListener {

    static FragmentActivity activity;
    static Post post;
    static EditText editTitle, editPostText;
    static ImageView editTitleImage, videoViewImage, imageViewIcon;
    static TextView txtViewMusic;
    static MediaPlayer mediaPlayer;
    static boolean isPlaying = false;
    UpdateFileDialogFragment updateFileDialogFragment;
    GPSController gpsController;

    public CreatePostController() {
    }

    /**
     * Створює та ініціалізує основний вигляд фрагменту
     *
     * @param view               - view, що належить фрагменту
     * @param activity           - activity, якому належить фрагмент
     * @param savedInstanceState - Bundle, що містить дані, збережені при повороті екрану
     */
    public CreatePostController(View view, FragmentActivity activity, Bundle savedInstanceState) {
        this.activity = activity;

        // ініціалізує необхідні об'єкти
        init(view);
        //відновлює дані після повороту екрану
        onRestoreInstanceState(savedInstanceState);
        // перевіряє створювати новий пост чи редагувати запитаний
        checkPostForEdit(post);

        gpsController = new GPSController((LocationManager) activity.getSystemService(activity.LOCATION_SERVICE), activity);

    }

    /**
     * Ініціалізує необхідні об'єкти
     *
     * @param view - - view, що належить фрагменту
     */
    private void init(View view) {

        editTitle = (EditText) view.findViewById(R.id.editTitle);
        editPostText = (EditText) view.findViewById(R.id.editPostText);
        editTitleImage = (ImageView) view.findViewById(R.id.editTitleImage);
        videoViewImage = (ImageView) view.findViewById(R.id.videoViewImageOnPost);
        imageViewIcon = (ImageView) view.findViewById(R.id.imageView_icon);
        editTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(editTitle.getText().toString().contains("\n")) {
                    editTitle.setText(editTitle.getText().toString().replaceAll("\n", ""));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        setImage();

        txtViewMusic = (TextView) view.findViewById(R.id.musicIcon);

        editTitleImage.setOnClickListener(this);
        videoViewImage.setOnClickListener(this);
        txtViewMusic.setOnClickListener(this);
        view.findViewById(R.id.smile).setOnClickListener(this);

        videoViewImage.setOnLongClickListener(this);
        imageViewIcon.setOnLongClickListener(this);
        txtViewMusic.setOnLongClickListener(this);
    }

    public void setImage(){
        try {
            imageViewIcon.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(Resource.PostData.pathToImage.toString(), activity.getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
            imageViewIcon.setVisibility(View.VISIBLE);
        }catch(NullPointerException e){}
    }

    /**
     * Якщо на фрагмент передається пост для редагування - витягує з нього дані і
     * відображає на фрагменті
     *
     * @param post - пост, що потрібно редагувати
     */
    public void checkPostForEdit(Post post) {
        if (post != null) {
            Resource.PostData.pathToTitleImage = post.getTitleImage();
            editTitle.setText(post.getTitle());
            editPostText.setText(SpannableController.getSmiledText(activity, post.getPostText()));
            editTitleImage.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(Resource.PostData.pathToTitleImage, activity.getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
            if(post.getTitleImage().contains("image_default_title")){
                editTitleImage.setImageBitmap(ImageController.getBitmapFromAsset(activity, post.getTitleImage()));
            }else {
                editTitleImage.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(post.getTitleImage(), activity.getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
            }

            List<Long> filesId = new PostFileService(activity.getApplicationContext()).getBy(post.getId());
            List<File> filesList = new FileService(activity.getApplicationContext()).getListFileToListId(filesId);

            for (File temp : filesList) {
                if(temp != null) {
                    String path = temp.getPath();

                    if (path.contains(".jpg") || path.contains(".JPG")) {

                        Resource.PostData.pathToImage = path;
                        imageViewIcon.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(path, activity.getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
                        imageViewIcon.setVisibility(View.VISIBLE);

                    } else if (path.contains(".mp4") || path.contains(".3gp")) {

                        Resource.PostData.pathToVideo = path;
                        videoViewImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND));
                        videoViewImage.setVisibility(View.VISIBLE);
                    }
                    if (path.contains(".mp3")) {

                        Resource.PostData.pathToAudio = path;
                        txtViewMusic.setText(path.substring(path.lastIndexOf("/") + 1));
                        txtViewMusic.setVisibility(View.VISIBLE);


                    }
                }
            }
        }
    }

    /**
     * Відновлює дані на фрагменті після повороту екрану
     *
     * @param savedInstanceState - Bundle, що містить дані, збережені до повороту екрану
     */
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        if (savedInstanceState != null) {

            editTitle.setText(savedInstanceState.getString("editTitle"));
            editPostText.setText(savedInstanceState.getString("editPostText"));

            if (savedInstanceState.getString("imageBitmap") != null) {
                Bitmap imageBitmap = BitmapFactory.decodeFile(savedInstanceState.getString("imageBitmap"));
                editTitleImage.setImageBitmap(imageBitmap);
            }
            if (savedInstanceState.getString("videoViewImage") != null) {
                videoViewImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(savedInstanceState.getString("videoViewImage"), MediaStore.Images.Thumbnails.MINI_KIND));
                videoViewImage.setVisibility(View.VISIBLE);
                Log.d("video lose", "on restore: " + savedInstanceState.getString("videoViewImage"));
            }
            if (savedInstanceState.getString("imagePath") != null) {
                imageViewIcon.setImageBitmap(BitmapFactory.decodeFile(savedInstanceState.getString("imagePath")));
            }

            if (savedInstanceState.getString("pathToAudio") != null) {
                txtViewMusic.setText(savedInstanceState.getString("pathToAudio"));
            }
        }
    }


    /**
     * Викликає діалог, для вибору файлу та прикріплює його до поста
     *
     * @param fragmentManager - fragmentManager, що відповідає фрагменту
     */

    public static void addFile(FragmentManager fragmentManager) {
        AddFileManagerDialogFragment addFileManagerDialogFragment = new AddFileManagerDialogFragment();
        addFileManagerDialogFragment.setListener(new DialogFragmentListener.OnClickDialogFragment() {
            @Override
            public void onClick(String parameter) {


                if (parameter.contains(".jpg") || parameter.contains(".JPG")) {
                    Resource.PostData.pathToImage = parameter;

                    imageViewIcon.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(parameter, activity.getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
                    imageViewIcon.setVisibility(View.VISIBLE);


                } else if (parameter.contains(".mp4") || parameter.contains(".3gp")) {
                    Resource.PostData.pathToVideo = parameter;
                    videoViewImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(parameter, MediaStore.Images.Thumbnails.MINI_KIND));
                    videoViewImage.setVisibility(View.VISIBLE);


                }
                if (parameter.contains(".mp3")) {
                    Resource.PostData.pathToAudio = parameter;
                    Log.d("music view", "parametr " + parameter + " view " + txtViewMusic);
                    txtViewMusic.setText(parameter.substring(parameter.lastIndexOf("/") + 1));
                    txtViewMusic.setVisibility(View.VISIBLE);


                }

            }
        });

        addFileManagerDialogFragment.show(fragmentManager, " Add File Manager");

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            // при кліку на TitleImage, обираємо зображення для поста
            case R.id.editTitleImage:

                new ChooseFromDialogFragment().show(activity.getSupportFragmentManager(), "Set Image Dialog Fragment");

                break;

            // при кліку на відеофайл, прикріплений до поста, відкривається актівіті перегляду медіафайлів
            case R.id.videoViewImageOnPost:

                MediaFileManagerController.getVideoViewIntent(Resource.PostData.pathToVideo, activity);
                break;

            case R.id.musicIcon:

                if (Resource.PostData.pathToAudio != null) {
                    if (!isPlaying) {
                        mediaPlayer = new MediaPlayer();
                        isPlaying = true;
                        txtViewMusic.setTextColor(activity.getResources().getColor(R.color.fab_material_green_500));

                        try {
                            mediaPlayer.setDataSource(Resource.PostData.pathToAudio);
                            mediaPlayer.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        mediaPlayer.start();
                    } else{
                          stopPlaying();
                    }
                }
                break;


            case R.id.smile:

                openSmile();

                break;
        }
    }

    public void openSmile(){
        //перевірка чи стоїть вже фрагмент
        if (!FragmentNavigationManagerController.isSmileFragmentVisible(activity)) {
            SmileFragment smileFragment = new SmileFragment();
            //вішання слухача для смайла
            smileFragment.setOnClickSmile(new SmileFragment.OnClickSmile() {
                @Override
                public void clickSmile(int position) {
                    //додання смайла
                    SpannableController.setSmile(editPostText, activity, position);
                }
            });
            FragmentNavigationManagerController.addSmileFragment(activity, smileFragment, activity.getString(R.string.tag_smile_fragment));
        } else {
            //видалення фрагмента, якщо він є
            FragmentNavigationManagerController.removeSmileFragment(activity);
        }
    }

    public static void setPost(Post p) {
        post = p;
    }

    public static Post getPost() {
        return post;
    }

    private static void stopPlaying() {
        if (mediaPlayer != null) {

            mediaPlayer.pause();
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
           txtViewMusic.setTextColor(activity.getResources().getColor(R.color.fab_material_black));
            isPlaying = false;

        }

    }

    /**
     * Отримує всі дані з поста
     *
     * @return map, який містить всі дані, що відповідають створеному посту
     */
    public static Map<String, String> getPostData() {
        Map<String, String> map = new HashMap<>();
        map.put("title", editTitle.getText().toString());
        map.put("postText", editPostText.getText().toString());
        map.put("photoPath", Resource.PostData.pathToTitleImage);
        map.put("videoPath", Resource.PostData.pathToVideo);
        map.put("imagePath", Resource.PostData.pathToImage);
        map.put("audioPath", Resource.PostData.pathToAudio);
        return map;
    }


    public static ImageView getTitleImageView() {
        return editTitleImage;
    }

    /**
     * Зберігає дані перед поворотом екрану
     *
     * @param outState - Bundle, в який зберігаються дані
     */
    public void onSaveInstanceState(Bundle outState) {

        outState.putString("editTitle", editTitle.getText().toString());
        outState.putString("editPostText", editPostText.getText().toString());

        if (Resource.PostData.pathToTitleImage != null) {
            outState.putString("imageBitmap", Resource.PostData.pathToTitleImage);
        }
        if (Resource.PostData.pathToVideo != null) {
            outState.putString("videoViewImage", Resource.PostData.pathToVideo);
            Log.d("video lose", "on save: " + Resource.PostData.pathToVideo);

        }
        if (Resource.PostData.pathToImage != null) {
            outState.putString("imagePath", Resource.PostData.pathToImage);
        }
        if (Resource.PostData.pathToAudio != null) {
            outState.putString("pathToAudio", Resource.PostData.pathToAudio);
        }
    }


    public static void recordAudio() {
        RecordAudioDialogFragment recordAudioDialogFragment = new RecordAudioDialogFragment();
        recordAudioDialogFragment.setListener(new DialogFragmentListener.OnClickDialogFragment() {
            @Override
            public void onClick(String parametr) {
                if(parametr != null) {
                    Log.d("recordAudio", parametr);
                    txtViewMusic.setText(parametr);
                    txtViewMusic.setVisibility(View.VISIBLE);
                }
            }
        });
        recordAudioDialogFragment.show(activity.getSupportFragmentManager(), "Record Audio");

    }

    @Override
    public boolean onLongClick(View v) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("post", post);
        switch (v.getId()) {


            case R.id.imageView_icon:
                updateFileDialogFragment = new UpdateFileDialogFragment();
                bundle.putString("path", Resource.PostData.pathToImage);



                break;

            case R.id.videoViewImageOnPost:
                updateFileDialogFragment = new UpdateFileDialogFragment();
                bundle.putString("path", Resource.PostData.pathToVideo);
                break;

            case R.id.musicIcon:
                updateFileDialogFragment = new UpdateFileDialogFragment();
                bundle.putString("path", Resource.PostData.pathToAudio);
                break;
        }
        updateFileDialogFragment.setArguments(bundle);
        updateFileDialogFragment.show(activity.getSupportFragmentManager(), "Update File Dialog Fragment");

        return true;
    }

    public static void onDestroy() {

        if (isPlaying) {
               stopPlaying();
        }


    }

    public static EditText getEditTitle() {
        return editTitle;
    }


    public  void disableGPS() {
        gpsController.getLocationManager().removeUpdates(gpsController);
    }
    public static ImageView getImageViewIcon() {
        return imageViewIcon;
    }

    public static ImageView getVideoViewImage() {
        return videoViewImage;
    }

    public static TextView getTxtViewMusic() {
        return txtViewMusic;
    }
}
