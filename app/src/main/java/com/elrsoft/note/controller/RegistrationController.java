package com.elrsoft.note.controller;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.activity.MainActivity;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.core.ListenerKeyboard;
import com.elrsoft.note.db.service.UserService;
import com.elrsoft.note.dialog.SetBirthdayDialogFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yana on 08.04.2015.
 */
public class RegistrationController implements View.OnClickListener {

    EditText editTextFirstName, editTextLastName, editTextCountry, editTextPassword, editTextEmail;
    TextView birthdayView;
    RadioButton male, female;
    SimpleDateFormat formatter;
    UserService userService;
    CheckBox showPassword;
    int inputType;

    FragmentActivity activity;

    public RegistrationController() {

    }

    /**
     * Створює весь вигляд RegistrationFragment-у:
     *
     * @param view               - view, що відповідає RegistrationFragment-у;
     * @param activity           - activity, якому належить RegistrationFragment;
     * @param savedInstanceState - Bundle, що містить дані, збережені при повороті екрану;
     */
    public RegistrationController(View view, FragmentActivity activity, Bundle savedInstanceState) {
        this.activity = activity;
        formatter = new SimpleDateFormat("dd.MM.yyyy");

        // ініціалізує всі віджети, що належать RegistrationFragment-у
        initializeWidgets(view);
        // в текстових полях виводить інформацію зареєстрованого користувача, якщо такий є
        getUserDataIfExist();
        // відновлює дані RegistrationFragment-а після повороту екрану
        onRestoreInstanceState(savedInstanceState);
        // перевіряє валідність введеної інформації
        checkInputData();


    }

    /**
     * Ініціалізує віджети, що належать RegistrationFragment-у:
     *
     * @param view - - view, що відповідає RegistrationFragment-у;
     */
    public void initializeWidgets(View view) {

        userService = new UserService(activity);

        editTextEmail = (EditText) view.findViewById(R.id.editTextEmail);
        editTextFirstName = (EditText) view.findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText) view.findViewById(R.id.editTextLastName);
        editTextCountry = (EditText) view.findViewById(R.id.editTextCountry);
        editTextPassword = (EditText) view.findViewById(R.id.editTextPassword);
        inputType = editTextPassword.getInputType();
        birthdayView = (TextView) view.findViewById(R.id.textViewBirthday);
        male = (RadioButton) view.findViewById(R.id.male);
        female = (RadioButton) view.findViewById(R.id.female);
        showPassword = (CheckBox)view.findViewById(R.id.showRegistrationPassword);

        birthdayView.setOnClickListener(this);
        showPassword.setOnClickListener(this);

        birthdayView.setText(formatter.format(new Date()));

    }

    /**
     * Перевіряє інформацію, яка вводиться, на валідність
     */
    public void checkInputData() {
        Map<String, EditText> inputFields = new HashMap();
        inputFields.put("email", editTextEmail);
        inputFields.put("firstName", editTextFirstName);
        inputFields.put("lastName", editTextLastName);
        inputFields.put("country", editTextCountry);
        inputFields.put("password", editTextPassword);

        new TextValidationController().checkValidation(inputFields);
    }

    /**
     * Відображає на RegistrationFragment-і інформації користувача,
     * якщо він уже зареєстрований
     */
    public void getUserDataIfExist() {
        if (userService.getAll().size() != 0) {
            User user = userService.getUser();

            editTextEmail.setText(user.getEmail());
            editTextFirstName.setText(user.getFirstName());
            editTextLastName.setText(user.getLastName());
            editTextCountry.setText(user.getCountry());
            try {
                editTextPassword.setText(null);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d("BIRTH", "getUserDataIfExist");
            Log.d("BIRTH", "birth " + user.getBirthday());
            if (user.getBirthday() != null) {
                birthdayView.setText(user.getBirthday());
            }

            if (user.getSex() == 1) {
                male.setChecked(true);
            } else if (user.getSex() == 2) {
                female.setChecked(true);
            }
        }
    }



    /**
     * Зберігає дані перед обертанням екрану
     *
     * @param outState - Bundle, що міститиме збережені дані;
     */
    public void saveInstanceState(Bundle outState) {
        outState.putString("email", editTextEmail.getText().toString());
        outState.putString("firstName", editTextFirstName.getText().toString());
        outState.putString("lastName", editTextLastName.getText().toString());
        outState.putString("country", editTextCountry.getText().toString());
        outState.putString("birthday", birthdayView.getText().toString());
        outState.putString("password", editTextPassword.getText().toString());
        if (male.isChecked()) {
            outState.putInt("sex", 1);
        } else if (female.isChecked()) {
            outState.putInt("sex", 2);
        }


    }

    /**
     * Відновлює дані після повороту екрану:
     *
     * @param savedInstanceState - Bundle, що містить збережені дані;
     */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            editTextEmail.setText(savedInstanceState.getString("email"));
            editTextFirstName.setText(savedInstanceState.getString("firstName"));
            editTextLastName.setText(savedInstanceState.getString("lastName"));
            editTextCountry.setText(savedInstanceState.getString("country"));
            editTextPassword.setText(savedInstanceState.getString("password"));
            birthdayView.setText(savedInstanceState.getString("birthday"));
            if (savedInstanceState.getInt("sex") == 1) {
                male.setChecked(true);
            } else if (savedInstanceState.getInt("sex") == 2) {
                female.setChecked(true);
            }
        }
    }

    /**
     * Заносить введену інформацію про користувача в базу даних
     */
    public void getUserDataForRegistration() {
        String firstName = TextValidationController.validName(editTextFirstName),
        lastName = TextValidationController.validName(editTextLastName),
        country = TextValidationController.validName(editTextCountry),
        email = TextValidationController.validEmail(editTextEmail),
        password = TextValidationController.validPassword(editTextPassword);
        String birthday = birthdayView.getText().toString();
Log.d("BIRTH", "string birth while saving " + birthday);
        int sex = 0;
        if (male.isChecked()) {
            sex = 1;
        } else if (female.isChecked()) {
            sex = 2;
        }


        // перевіряє дані та зберігає користувача
        checkAndSaveUser(firstName, lastName, email, country, password, sex, birthday);


    }

    /**
     * Перевіряє чи заповнені усі реєстраційні поля та зберігає користувача в базі даних:
     *
     * @param firstName - ім'я корисутвача;
     * @param lastName  - прізвище користувача;
     * @param email     - email адрес корисутувача;
     * @param country   - країна користувача;
     * @param password  - пароль для захисту додатку;
     * @param sex       - стать;
     * @param birthday  - дата народження;
     */
    public void checkAndSaveUser( String firstName, String lastName, String email, String country, String password, int sex, String birthday) {
        if (firstName == null || lastName == null || email == null ||
                country == null || birthday == null || password == null
                || sex == 0) {

            showRegistrationMassage(false);
        } else {

            User user = null;
            try {
                user = new User(1L, firstName, lastName, sex, email, new PasswordController().encrypt(password), birthday, country);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("BIRTH", " saving " + birthday);
            // якщо в базі даних немає жодного зареєстрованого користувача, зберігаємо там його дані
            if ((userService.getAll()).size() == 0) {
                userService.save(user);
            }
            // якщо користувач раніше зареєстрований, при повторній реєстрації його дані оновлюються
            else {
                userService.update(user);
            }

            showRegistrationMassage(true);
            FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));
        }
    }

    /**
     * Виводить повідомлення/попередження при реєстрації
     *
     * @param status
     */
    public void showRegistrationMassage(boolean status) {
        Toast massage;
        // всі поля заповнені добре, реєстрація пройшла вдало
        if (status) {
            massage = Toast.makeText(activity.getApplicationContext(), "Thank you for registration!"
                    , Toast.LENGTH_SHORT);
            massage.show();
        }
        // виявлено пусті або неправильно заповнені поля
        else {
            massage = Toast.makeText(activity.getApplicationContext(), "Please, fill all the fields."
                    , Toast.LENGTH_LONG);
            massage.show();
        }
    }


    /**
     * Слухач для віджетів., що належать RegistrationFragment-у:
     *
     * @param v - віджет, з яким відбулась взаємодія;
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            // відкриває діалог вибору дати
            case R.id.textViewBirthday:
                SetBirthdayDialogFragment setBirthdayDialogfragment = new SetBirthdayDialogFragment();
                setBirthdayDialogfragment.setListener(new DialogFragmentListener.OnClickDialogFragment() {
                    @Override
                    public void onClick(String path) {
                        birthdayView.setText(path);
                    }
                });
                setBirthdayDialogfragment.show(activity.getSupportFragmentManager(), activity.getString(R.string.tag_set_birthday_dialog_fragment));
                break;

            // зберігає інформацію про користувача в базу даних

            case R.id.showRegistrationPassword:

                if(showPassword.isChecked()){
                    editTextPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else {
                    editTextPassword.setInputType(inputType);
                }
                break;



        }

    }
}