package com.elrsoft.note.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

import com.elrsoft.note.R;
import com.elrsoft.note.activity.MainActivity;
import com.elrsoft.note.db.service.GPSMarkerService;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.model.GPSMarker;
import com.elrsoft.note.model.Post;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 17.03.2015.
 */
public class DrawPhotoForMapController {

    Bitmap bitmap;
    List<Post> posts;
    GoogleMap googleMap;
    Context context;

    public DrawPhotoForMapController(GoogleMap googleMap, Context context){
        this.context = context;
        this.posts = new PostService(context).getAll();
        this.googleMap = googleMap;
    }

    // Визначає скільки постів розіщені один від одного на space
    public void setMarkerForImage(int space){

        List<Post> posts1 = new ArrayList<>(posts);

        for (int x = 0; x < posts1.size(); x++){
            Post post = posts1.get(x);

            if(post == null || post.getMarkerId() == 0){

                continue;

            }

            final GPSMarker marker = new GPSMarkerService(context).getById(new GPSMarker(post.getMarkerId()));

            bitmap = PhotoSizeController.decodeSampledBitmapFromResource(post.getTitleImage(), context.getResources().getDimensionPixelSize(R.dimen.image_size), context.getResources().getDimensionPixelSize(R.dimen.image_size));

            if (marker != null && bitmap != null) {

                // Індекс першого поста
                int index = -1;
                // Кількість постів
                int count = 0;

                for(int i = 0; i < posts1.size(); i++) {

                    Post p = posts1.get(i);
                    Log.d("MyLog", "map 1 = " + marker.getLatitude() + " , , , , " + marker.getLongitude());
                    // Провіряє чи є поблизу інші пости
                    if(p != null && p.getMarkerId() != 0 && marker.getLatitude() - new GPSMarkerService(context).getById(new GPSMarker(p.getMarkerId())).getLatitude() >= -1 * space && marker.getLatitude() - new GPSMarkerService(context).getById(new GPSMarker(p.getMarkerId())).getLatitude() <= space && marker.getLongitude() - new GPSMarkerService(context).getById(new GPSMarker(p.getMarkerId())).getLongitude() >= -1 * space  && marker.getLongitude() - new GPSMarkerService(context).getById(new GPSMarker(p.getMarkerId())).getLongitude() <= space){
                        index = posts1.indexOf(p);
                        posts1.remove(i);
                        posts1.add(i, null);
                        ++count;
                    }
                }

                // Якщо index >= 0 значить такий пост існує в posts1 і повертає MarkerOptions для поставлення мітки
                if(index >= 0){

                    // Викликає метод для промальовування картинки
                    drowImageForMap(count);
                    count = 0;
                    index = -1;
                    // Ставить мітку
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(marker.getLatitude(), marker.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                            .title(post.getTitle()));
                }


            }

        }


    }

    // Робить всі дії з картинкою
    public void drowImageForMap(int count){

        int size1 = bitmap.getHeight();
        int size2 = bitmap.getWidth();

        // Визначає в якому положеню находиться картинка, у вертикальному чи горизонтальному і обрізає її по центральному квадраті
        if(size1 > size2){
            bitmap = Bitmap.createBitmap(bitmap, 0, (size1 - size2) / 2, size2, size2, null, true);
        } else {
            bitmap = Bitmap.createBitmap(bitmap, (size2-size1)/2, 0, size1, size1, null, true);
        }

        bitmap = Bitmap.createScaledBitmap(bitmap, 120, 120, true);

        // Малює білу рамку навколо картинки
        Canvas canvas = new Canvas(bitmap);
        Paint p = new Paint();
        p.setStrokeWidth(15);
        p.setColor(Color.WHITE);
        canvas.drawLine(0, 0, 120, 0, p);
        canvas.drawLine(0,0,0,120,p);
        canvas.drawLine(120,0,120,120,p);
        canvas.drawLine(0,120,120,120,p);

        //Провіряє чи кількість постів є більшою за 1, якщо так то малює овал із кількістю постів
        if(count > 1) {

            p.setARGB(255, 0, 160, 255);
            RectF rectf = new RectF(75,0,120,30);
            canvas.drawRoundRect(rectf, 20, 20, p);
            p.setColor(Color.WHITE);
            p.setTextSize(23);

            // Провіряє кількість цифр у count і робить відповідний віступ
            if(count >= 10) {

                canvas.drawText(count + "", 85, 22, p);

            } else {

                canvas.drawText(count + "", 91, 22, p);

            }

        }
    }
}
