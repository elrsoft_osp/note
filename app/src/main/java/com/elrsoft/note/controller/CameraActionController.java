package com.elrsoft.note.controller;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yana on 06.05.2015.
 */
public class CameraActionController {

    static final String CAMERA_DOES_NOT_SUPPORTED = "Whoops - your device doesn't support capturing images!",
            CROPPING_DOES_NOT_SUPPORTED = "Whoops - your device doesn't support the crop action!";
    static File outputCameraFile, outputCropFile;

    /**
     * Активує фотокамеру
     * @param activity - актівіті, яким викликається камера
     * @param REQUEST_CODE - код запиту камери
     */
    public static void launchCamera(FragmentActivity activity, final int REQUEST_CODE) {
        GoogleAnalyticsController.setTracker(activity, "Launch photo camera");
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getOutputCameraFile()));
            activity.startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException activityNotFound) {
            Toast.makeText(activity, CAMERA_DOES_NOT_SUPPORTED, Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Дозволяє вибрати ресурс, який зможе обрізати зображнення, отримане з камери
     * @param activity - актівіті, яким викликається crop_intent
     * @param REQUEST_CODE - код запиту даної дії
     */
    public static void launchCropActivity(FragmentActivity activity, final int REQUEST_CODE) {
        Uri photoUri = Uri.fromFile(MediaFileManagerController.getLastModifiedFile(new File("/mnt/sdcard/ext_sd/DCIM/100MEDIA/")));
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            cropIntent.setDataAndType(photoUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            cropIntent.putExtra("return-data", false);
            Uri resUri = Uri.fromFile(getOutputCropFile());
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, resUri);
            activity.startActivityForResult(cropIntent, REQUEST_CODE);

        } catch (ActivityNotFoundException activityNotFound) {
            activityNotFound.printStackTrace();
            Toast.makeText(activity, CROPPING_DOES_NOT_SUPPORTED, Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Файл, в який записуватиметься фото
     * @return - файл, в який записуватиметься знімок, зроблений камерою
     */
    private static File getOutputCameraFile() {

        outputCameraFile = new File("/mnt/sdcard/ext_sd/DCIM/100MEDIA/" + "IMG_" + new SimpleDateFormat("HHmmss").format(new Date()) + ".jpg");
        return outputCameraFile;
    }

    /**
     * Файл, обрізаного зображення
     * @return файл, в який буде записано обрізане зображення
     */
    private static File getOutputCropFile(){
        outputCropFile = new File("/mnt/sdcard/ext_sd/DCIM/100MEDIA/" + "IMG_" + new SimpleDateFormat("HHmmss").format(new Date()) + ".jpg");
        return outputCropFile;
    }
}
