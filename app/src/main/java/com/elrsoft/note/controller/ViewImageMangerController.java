package com.elrsoft.note.controller;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by <ELRsoft> HANZ on 04.04.2015.
 */
public class ViewImageMangerController {

    /**
     * Метод для створення картинки, під різні екрани
     *
     * @param activity актівіті, на якому показуємо
     * @return ImageView - створений обект без картинки
     */
    public static ImageView createImageViewForGrigView(Activity activity) {

        int columIndex = 3;

        DisplayMetrics displayMetrics = activity.getResources()
                .getDisplayMetrics();

        int screenWidthInPix = displayMetrics.widthPixels;
        int screenheightInPix = displayMetrics.heightPixels;
        if (screenWidthInPix > screenheightInPix) {
            columIndex = 4;
        }
        ImageView imageView = new ImageView(activity);
        imageView.setLayoutParams(new GridView.LayoutParams((screenWidthInPix / columIndex) - 10, (screenWidthInPix / columIndex) - 10));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }

}
