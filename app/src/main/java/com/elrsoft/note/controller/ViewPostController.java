package com.elrsoft.note.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.db.service.PostFileService;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.fragment.CreatePostFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.resource.Resource;
import com.google.android.gms.maps.SupportMapFragment;
import com.software.shell.fab.ActionButton;

import java.io.IOException;
import java.util.List;

/**
 * Created by Yana on 08.04.2015.
 */
public class ViewPostController implements View.OnClickListener {

    TextView postTitle, postText;
    ImageView postTitleImage;
    ActionButton editPostButton;
    FragmentManager fragmentManager;
    FragmentActivity activity;
    SupportMapFragment supportMapFragment;
    static ImageView videoViewImage, imageViewIcon;
    TextView txtViewMusic, share;
    boolean isPlaying = false;



    static Post post;

    MediaPlayer mediaPlayer;


    public ViewPostController() {
    }

    /**
     * Створює весь вигляд для ViewPostFragment:
     *
     * @param view              - об'єкт view для ViewPostFragment-а;
     * @param activity          - актівіті, якому належить ViewPostFragment;
     * @param resources         - об'єкт ресурсів ViewPostFragment-а;
     * @param fragmentManager   - об'єкт fragmentManager-а для ViewPostFragment-а;
     * @param saveInstanceState - Bundle із даними, що збереглися при повороті екрану;
     */
    public ViewPostController(View view, FragmentActivity activity, Resources resources, FragmentManager fragmentManager, Bundle saveInstanceState) {

        this.fragmentManager = fragmentManager;
        this.activity = activity;


        initWidgets(view);
        onRestoreInstanceState(saveInstanceState);
        setPostDataOnView(activity, resources, post);
    }


    /**
     * Ініціалізація всіх віджетів, що належать ViewPostFragment:
     *
     * @param view - об'єкт view для ViewPostFragment-а;
     */
    public void initWidgets(View view) {

        supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);

        postTitle = (TextView) view.findViewById(R.id.textTitle);
        postText = (TextView) view.findViewById(R.id.textPostText);
        postTitleImage = (ImageView) view.findViewById(R.id.textTitleImage);
        editPostButton = (ActionButton) view.findViewById(R.id.buttonEditPost);
        share = (TextView)view.findViewById(R.id.share);

        videoViewImage = (ImageView) view.findViewById(R.id.videoViewImageOnPost);
        imageViewIcon = (ImageView) view.findViewById(R.id.imageView_icon);
        txtViewMusic = (TextView) view.findViewById(R.id.musicIcon);

        videoViewImage.setOnClickListener(this);
        editPostButton.setOnClickListener(this);
        txtViewMusic.setOnClickListener(this);
        share.setOnClickListener(this);

    }

    /**
     * Відображає всі дані поста на ViewPostFragment:
     *
     * @param activity  - актівіті, якому належить ViewPostFragment;
     * @param resources - об'єкт ресурсів ViewPostFragment-а;
     * @param post      - пост, що відображається на ViewPostFragment-і;
     */
    public void setPostDataOnView(FragmentActivity activity, Resources resources, Post post) {
        try {
            postTitle.setText(post.getTitle());
            //декодування смайлів

            Spannable spannable = SpannableController.getSmiledText(activity, post.getPostText());
            postText.setText(spannable);


            Resource.PostData.pathToTitleImage = post.getTitleImage();
            if (post.getTitleImage().contains("image_default_title")) {
                postTitleImage.setImageBitmap(ImageController.getBitmapFromAsset(activity, post.getTitleImage()));
            } else {
                postTitleImage.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(post.getTitleImage(), resources.getDimensionPixelSize(R.dimen.image_size), resources.getDimensionPixelSize(R.dimen.image_size)));
            }

            List<Long> filesId = new PostFileService(activity.getApplicationContext()).getBy(post.getId());

            List<File> filesList = new FileService(activity.getApplicationContext()).getListFileToListId(filesId);
            Log.d("lists", "IN VIEW fileId = " + filesId.size() + " fileList = " + filesList.size());

            for (File temp : filesList) {
                if (temp != null) {
                    String path = temp.getPath();

                    Log.d("data on view", "path = " + path);
                    if (path.contains(".jpg") || path.contains(".JPG")) {
                        Log.d("data on view", "photo path " + path);

                        Resource.PostData.pathToImage = path;
                        imageViewIcon.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(path, resources.getDimensionPixelSize(R.dimen.image_size), resources.getDimensionPixelSize(R.dimen.image_size)));
                        imageViewIcon.setVisibility(View.VISIBLE);

                    } else if (path.contains(".mp4") || path.contains(".3gp")) {
                        Log.d("data on view", "video path " + path);
                        Resource.PostData.pathToVideo = path;
                        videoViewImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND));
                        videoViewImage.setVisibility(View.VISIBLE);
                    }
                    if (path.contains(".mp3")) {


                        Resource.PostData.pathToAudio = path;
                        txtViewMusic.setText(path.substring(path.lastIndexOf("/") + 1));
                        txtViewMusic.setVisibility(View.VISIBLE);


                    }
                }
            }
        } catch (NullPointerException e){
            FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));
        }
    }


    /**
     * Відтворює дані після повороту екрану
     *
     * @param savedInstanceState - Bundle, що містить всі збережені при повороті дані
     */
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            postTitle.setText(savedInstanceState.getString("title"));
            postText.setText(savedInstanceState.getString("postText"));
            if (savedInstanceState.getString("titleImage") != null) {
                postTitleImage.setImageBitmap(BitmapFactory.decodeFile(savedInstanceState.getString("titleImage")));
            }
            if (savedInstanceState.getString("image") != null) {
                imageViewIcon.setImageBitmap(BitmapFactory.decodeFile(savedInstanceState.getString("image")));
            }
            if (savedInstanceState.getString("video") != null) {
                videoViewImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(savedInstanceState.getString("video"), MediaStore.Images.Thumbnails.MINI_KIND));
            }
            if (savedInstanceState.getString("audio") != null) {
                txtViewMusic.setText(savedInstanceState.getString("audio"));
            }
        }
    }


    /**
     * Зберігає дані при повороті екрану
     *
     * @param outState - Bundle, в який зберігаються дані при повороті екрану
     */
    public void saveInstanceState(Bundle outState) {
        outState.putString("title", postTitle.getText().toString());
        outState.putString("postText", postText.getText().toString());
        outState.putString("titleImage", Resource.PostData.pathToTitleImage);
        outState.putString("image", Resource.PostData.pathToImage);
        outState.putString("video", Resource.PostData.pathToVideo);
        outState.putString("audio", Resource.PostData.pathToAudio);
    }

    /**
     * Слухачі для віджетів ViewPostFragment-а
     *
     * @param view - віджет, з яким взаємодіє користувач
     */
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.buttonEditPost:
                CreatePostFragment createPostFragment = new CreatePostFragment();
                new CreatePostController().setPost(new PostService(activity).getById(post));
                FragmentNavigationManagerController.moveToFragment(activity, createPostFragment, activity.getString(R.string.tag_create_post_fragment));

                break;

            case R.id.videoViewImageOnPost:

                MediaFileManagerController.getVideoViewIntent(Resource.PostData.pathToVideo, activity);
                break;

            case R.id.musicIcon:

                if (Resource.PostData.pathToAudio != null) {
                    if (!isPlaying) {
                        mediaPlayer = new MediaPlayer();
                        isPlaying = true;
                        txtViewMusic.setTextColor(activity.getResources().getColor(R.color.fab_material_green_500));

                        try {
                            mediaPlayer.setDataSource(Resource.PostData.pathToAudio);
                            mediaPlayer.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        mediaPlayer.start();
                    } else if (isPlaying) {
                        if (mediaPlayer != null) {
                            isPlaying = false;
                            txtViewMusic.setTextColor(activity.getResources().getColor(R.color.fab_material_black));
                            mediaPlayer.pause();
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                    }
                }
                break;

            case R.id.share:

                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, post.getTitle());
                shareIntent.putExtra(Intent.EXTRA_TEXT, post.getPostText());
                activity.startActivity(Intent.createChooser(shareIntent, "Share your thoughts"));
                break;
        }
    }

    public static Post getPost() {
        return post;
    }

    public static void setPost(Post post) {
        ViewPostController.post = post;
    }
}
