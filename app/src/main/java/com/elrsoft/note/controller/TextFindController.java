package com.elrsoft.note.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by <ELRsoft> HANZ on 06.04.2015.
 */
public class TextFindController {

    private static final String DEFAULT_IMAGE = "image_default_title/default.jpg";


    //ресурс картинок з Pattern по пошуку в тексті
    private static final Map<Pattern, String> emoticons = new HashMap<>();

    static {
        addPattern(emoticons, "кохання|love|кохаю|люблю", "image_default_title/love.jpg");
        addPattern(emoticons, "андроід|android", "image_default_title/android.jpg");
        addPattern(emoticons, "тест|test", "image_default_title/test.jpg");
        addPattern(emoticons, "музика|мелодія|звук|music|музику|симфонія|оркестр|melody|sound|symphony", "image_default_title/music.jpg");
        addPattern(emoticons, "людина|особа|людині|person|people|human|humane|humanity", "image_default_title/person.jpg");
        addPattern(emoticons, "кава|чай|гарячий напиток|капучіно|tea|cofe|гарячий шоколад", "image_default_title/tea.jpg");
        addPattern(emoticons, "осінь|осінню|autumn|осінній|осіннє", "image_default_title/autumn.jpg");
        addPattern(emoticons, "небо|небеса|sky|хмари|тучи|cloud", "image_default_title/sky.jpg");
        addPattern(emoticons, "зорі|зірочка|зірки|stars|зоряне|зоряна|starry|starlit|star", "image_default_title/stars.jpg");
        addPattern(emoticons, "вогонь|ватра|вогнем|вогник|fire|вогняний|запальний|згоріти|спалахнути|fiery|camp-fire|light|bonfire|combustion|burn", "image_default_title/fire.jpg");
        addPattern(emoticons, "океан|океаном|ocean|океані", "image_default_title/ocean.jpg");
        addPattern(emoticons, "космос|галактика|spase|galaxy", "image_default_title/spase.jpg");
        addPattern(emoticons, "будинок|дім|хатина|home|халупа|сарай|домашній|затишок|затишний|по домашньому|домовик|sheltered spot|calm|lull|quiet|comfortable|cosy", "image_default_title/home.jpg");
        addPattern(emoticons, "сніг|метель|сніжинки|snow|мороз|сніговий|зима|зимовий|буран|metel|flakes|frost|winter", "image_default_title/snow.jpg");
        addPattern(emoticons, "жадність|жадібність|жадіна|covetousness|скнара|жлоб|барига|скрудж|greed|miser|redneck|huckster|Scrooge", "image_default_title/covetousness.jpg");
        addPattern(emoticons, "радість|веселощі|щасливий|joy|happy|щастя|радісний|fun|ouch", "image_default_title/joy.jpg");
        addPattern(emoticons, "труп|втомлений|відрубатися|не жити|corpse|повіситись|вбити|розчленування|corpse|tired|hang|kill|dismemberment", "image_default_title/corpse.jpg");
        addPattern(emoticons, "квіти|букет|квіточки|троянди|flowers|rose|ромашки|айстри|чорнобривці|тюльпани|лілії|орхідеї|гвоздика|рози", "image_default_title/flowers.jpg");
        addPattern(emoticons, "комп'ютер|техніка|планшет|computer|технічний", "image_default_title/computer.jpg");
        addPattern(emoticons, "гітара|струни|на гітарі|guitar|лабати|медіатор", "image_default_title/guitar.jpg");
        addPattern(emoticons, "ненавижу|не можу терпіти|непереношу|hate", "image_default_title/hate.jpg");
        addPattern(emoticons, "обручка|кольцо|каблучка|wedding ring", "image_default_title/wedding_ring.jpg");
        addPattern(emoticons, "кокаїн|маріхуана|наркотики|передозування|cocain|амфітамін", "image_default_title/cocain.jpg");
        addPattern(emoticons, "сон|спати|кома|знепритомніти|перебувати в трансі|dream|sleep", "image_default_title/dream.jpg");
        addPattern(emoticons, "очі|око|погляд|eyes|макіяж|мейкап", "image_default_title/eyes.jpg");
        addPattern(emoticons, "волосся|на волоссі|hair|патла|хвостик|коса", "image_default_title/hair.jpg");
        addPattern(emoticons, "манікюр|нігті|manicure", "image_default_title/manicure.jpg");
        addPattern(emoticons, "поцілунок|поцілуватися|цілуватися|kiss|поцілував", "image_default_title/kiss.jpg");
        addPattern(emoticons, "день народження|днюха|birthday", "image_default_title/birthday.jpg");
        addPattern(emoticons, "смерть|піти з життя|покинути світ|death|помер|світло в кінці тунеля", "image_default_title/death.jpg");
        addPattern(emoticons, "кров|кровотеча|blood", "image_default_title/blood.jpg");
        addPattern(emoticons, "серце|heart|сердечний|сердечне|щиросердний", "image_default_title/heart.jpg");
        addPattern(emoticons, "посмішка|посміхатися|сміятися|ржати|smile|Хдд|хахахаха", "image_default_title/smile.jpg");
        addPattern(emoticons, "гроші|багатство|грошима|money|копійки|здача|оплата|зарплата", "image_default_title/money.jpg");
        addPattern(emoticons, "золото|золотий|позолочений|дорогоцінний|gold|золотий|золоте", "image_default_title/gold.jpg");
        addPattern(emoticons, "сонце|cонечко|сонцем|тепло|освітлення|sun|ра", "image_default_title/sun.jpg");
        addPattern(emoticons, "уроки|домашнє завдання|пари|заняття|lessons", "image_default_title/lessons.jpg");
        addPattern(emoticons, "біль|болить|болем|pain", "image_default_title/pain.jpg");
        addPattern(emoticons, "шоколадка|солодке|шоколадний|chocolate|конфети|цукерки|солодкоїжка", "image_default_title/chocolate.jpg");
        addPattern(emoticons, "навушники|навушниками|earphones|вуха", "image_default_title/earphones.jpg");
        addPattern(emoticons, "дощ|краплі|дощем|rain|злива|гроза", "image_default_title/rain.jpg");
        addPattern(emoticons, "книга|твір|роман|історія|книжка|book|журнал|тетрадь|тетрадка", "image_default_title/book.jpg");
        addPattern(emoticons, "м'ягка іграшка|ведмедик|ігрушка|toy|ведмедика", "image_default_title/toy.jpg");
        addPattern(emoticons, "море|в морі|мрії|sea", "image_default_title/sea.jpg");
        addPattern(emoticons, "місто|цивілізація|city|місцевий", "image_default_title/city.jpg");
        addPattern(emoticons, "телефон|смартфон|айфон|phone|сенсорка|мобілка|мобільний", "image_default_title/phone.jpg");
        addPattern(emoticons, "мусор|непотрібні речі|сміття|garbage", "image_default_title/garbage.jpg");
        addPattern(emoticons, "помада|блєск для губ|pomade", "image_default_title/pomade.jpg");
        addPattern(emoticons, "бетмен|batman|супергерой|Вася", "image_default_title/batman.jpg");
        addPattern(emoticons, "сюрприз|несподіванка|suprise", "image_default_title/surprise.jpg");
        addPattern(emoticons, "подарунок|подарок|подаруночок|gift|подарували|подарував|подарувала", "image_default_title/gift.jpg");
        addPattern(emoticons, "танці|ритм|рухи|dancing|танцям|танцях", "image_default_title/dancing.jpg");
        addPattern(emoticons, "мистецтво|живопис|кольори|art", "image_default_title/art.jpg");
        addPattern(emoticons, "клубніка|клубнічка|зваблення|strawberries", "image_default_title/strawberries.jpg");
        addPattern(emoticons, "дитина|малюк|діти|child|baby|дитячі|шмакодявка|малявка", "image_default_title/child.jpg");
        addPattern(emoticons, "шаріки|кульки|balls|full metal balls", "image_default_title/balls.jpg");
        addPattern(emoticons, "гори|краєвид|гора|mountains|пейзаш", "image_default_title/mountains.jpg");
        addPattern(emoticons, "захід|романтика|measure|закінчення", "image_default_title/measure.jpg");
        addPattern(emoticons, "годинник|час|clock|time|deadline", "image_default_title/clock.jpg");
        addPattern(emoticons, "рок|rock", "image_default_title/rock.jpg");
        addPattern(emoticons, "метал|metal", "image_default_title/metal.jpg");
        addPattern(emoticons, "кіно|фільм|серіал|cinema|кінотеатр", "image_default_title/cinema.jpg");
        addPattern(emoticons, "сесія|навчання|запара|session|студент", "image_default_title/session.jpg");
        addPattern(emoticons, "ідіот|тряпка|тормоз|дибіл|moron", "image_default_title/moron.jpg");
        addPattern(emoticons, "\\.\\.\\.|думка|ідея|незакінчений", "image_default_title/dot.png");
        addPattern(emoticons, "процесор|магія|processor", "image_default_title/processor.jpg");
        addPattern(emoticons, "морозиво|смачненьке|ice_cream", "image_default_title/ice_cream.jpg");
        addPattern(emoticons, "дзвінок|дзвіночок|call|дзвінкий", "image_default_title/call.jpg");
        addPattern(emoticons, "робота|на роботі|праця|work|будні|робочий", "image_default_title/work.jpg");
        addPattern(emoticons, "зрада|зрадив|обман|розчарування|betrayal", "image_default_title/betrayal.jpg");
        addPattern(emoticons, "дурак|придурок|fool", "image_default_title/fool.jpg");
        addPattern(emoticons, "лампочка|просвітило|bulb|ідея|спишка", "image_default_title/bulb.jpg");
        addPattern(emoticons, "мозок|розум|мізки|brain|голова|розумний", "image_default_title/brain.jpg");
        addPattern(emoticons, "туш|підводка|indian ink", "image_default_title/indian_ink.jpg");
        addPattern(emoticons, "вітер|вітерець|подуло|wind|продуло|занесло", "image_default_title/wind.jpg");
        addPattern(emoticons, "хлопець|чоловік|коханий|man|boy|boyfriend|хлоп|холоп", "image_default_title/man.jpg");
        addPattern(emoticons, "лак|краска|фарба|varnish", "image_default_title/varnish.jpg");
        addPattern(emoticons, "єдинорог|казки|unicorn|сутність|существо", "image_default_title/unicorn.jpg");
        addPattern(emoticons, "паста|paste|макарони|кетчуп", "image_default_title/paste.jpg");
        addPattern(emoticons, "орбіта|стратосфера|orbite", "image_default_title/orbit.jpg");
        addPattern(emoticons, "думки|думаю|роздумувати|views|в печалі|facepalme|сором", "image_default_title/views.jpg");
        addPattern(emoticons, "тортик|торт|печево|cake|тортище", "image_default_title/cake.jpg");
        addPattern(emoticons, "свято|щасливий день|празник|holiday", "image_default_title/holiday.jpg");
        addPattern(emoticons, "піраміда|єгипет|піраміда хеопса|pyramid|верблюд|араби", "image_default_title/pyramid.jpg");
        addPattern(emoticons, "подорож|дорога|відпочинок|прогулянка|екскурсія|trip", "image_default_title/trip.jpg");
        addPattern(emoticons, "шопінг|покупки|магазини|нові речі|shoping", "image_default_title/shoping.jpg");
        addPattern(emoticons, "прекраси|біжутерія|decorations|жінка", "image_default_title/decorations.jpg");
        addPattern(emoticons, "відвертість|відкритість|openness|секрет", "image_default_title/openness.jpg");
        addPattern(emoticons, "їжа|їсти|food|жерти|амброзія", "image_default_title/food.jpg");
        addPattern(emoticons, "дим|кальян|туман|smoke|люлька", "image_default_title/smoke.jpg");
        addPattern(emoticons, "шашлики|барбекю|м'ясо|barbecue", "image_default_title/barbecue.jpg");
        addPattern(emoticons, "відрізати|відрубати|відірвати|cut off", "image_default_title/cut_off.jpg");
        addPattern(emoticons, "дівчина|леді|місс|подруга|подрузі|сестрі|сестра", "image_default_title/girl.jpg");
        addPattern(emoticons, "повія|шлюха|проститутка|вульгарна дівчина|стерво", "image_default_title/prostitute.jpg");
        addPattern(emoticons, "доля|судьба|знедолений|життя|вічність|майбутнє", "image_default_title/fate.jpg");
        addPattern(emoticons, "весілля|наречені|весільний|заручений|свадьба|весільна", "image_default_title/wedding.jpg");
        addPattern(emoticons, "коп'ютерні ігри", "image_default_title/computer_games.jpg");
        addPattern(emoticons, "машина|лімузин|автомобіль|шивроле|БМВ|автомобільний", "image_default_title/car.jpg");
        addPattern(emoticons, "інтернет|інтернеті|інтернетний|сайт|сайті|мережа|мережі", "image_default_title/internet.jpg");
        addPattern(emoticons, "реп|хіп-хоп|брейк", "image_default_title/rap.jpg");
        addPattern(emoticons, "вино|коньяк|горілка|мартіні|шампанське|коктель", "image_default_title/alcoholic_beverages.jpg");
        addPattern(emoticons, "окуляри|очки", "image_default_title/glasses.jpg");
        addPattern(emoticons, "вечірка|клуб|клубняк|тусовка|дискотека|дискотеці", "image_default_title/club.jpg");
        addPattern(emoticons, "ніч|вечір|нічний|вечірний|темно|темний", "image_default_title/night.jpg");
        addPattern(emoticons, "день|полудень|ранок|світанок|ранковий", "image_default_title/day.jpg");
        addPattern(emoticons, "страх|боятися|боязливий|страхування|страшний|страшилки|боязлива", "image_default_title/fear.jpg");
        addPattern(emoticons, "крила|літати|полетіти|полетіла|летіл|летів|літав|окрилений", "image_default_title/wings.jpg");
        addPattern(emoticons, "сумка|сумочка|клач|гаманець", "image_default_title/bag.jpg");
    }

    /**
     * Додає запис до Map<Pattern, Integer>
     *
     * @param map      resource Pattern and id image Map
     * @param smile    resource regex
     * @param resource image in drawable
     */
    private static void addPattern(Map<Pattern, String> map, String smile,
                                   String resource) {
        map.put(Pattern.compile(smile), resource);
    }

    /**
     * Шукає визначені слова в тексті
     *
     * @param text post to parse for searge image
     * @return id image is drawable
     */
    public static String getRandomImage(String text) {

        int count = 0;
        int max = 0;
        String values = "";
        for (Map.Entry<Pattern, String> entry : emoticons.entrySet()) {
            Matcher matcher = entry.getKey().matcher(text);
            while (matcher.find()) {
                count++;
            }
            if (max < count) {
                max = count;
                values = entry.getValue();
            }
            count = 0;
        }

        return values.equals("") ? DEFAULT_IMAGE : values;
    }

}
