package com.elrsoft.note.controller;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yana on 07.05.2015.
 */
public class VideoCameraController {

    private static String fileNameFormat = new SimpleDateFormat("HHmmss").format(new Date());


    /**
     * Місце збереження відеофайла
     * @return файл директорії, де буде зберігатись відео
     */
    public static File getVideoStroageDir() {     // задає місце збереження відео файлів

        File mediaStorageDir = new File("/mnt/sdcard/ext_sd/DCIM/100MEDIA/");

        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        return mediaStorageDir;
    }

    /**
     * Файл, в який записуватиметься відео
     * @return файл, що міститиме записане відео
     */
    public static File getOutputVideoFile() {
        return new File(getVideoStroageDir().getAbsolutePath() + File.separator +
                "VID_" + fileNameFormat + ".mp4");

    }


    /**
     * Активує відеокамеру
     * @param activity - activity, з якого запускатиметься камера
     * @param REQUEST_CODE - код запиту для activity камери
     */
    public static void launchVideoCamera(FragmentActivity activity, final int REQUEST_CODE) {
        GoogleAnalyticsController.setTracker(activity, "Launch video camera");
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getOutputVideoFile()));
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        activity.startActivityForResult(intent, REQUEST_CODE);
    }


}
