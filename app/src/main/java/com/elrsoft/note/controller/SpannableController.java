package com.elrsoft.note.controller;

import android.app.Activity;
import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.widget.EditText;

import com.elrsoft.note.R;
import com.elrsoft.note.resource.Resource;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by <ELRsoft> HANZ on 01.04.2015.
 */
public class SpannableController {

    //створення фабрики
    private static final Spannable.Factory spannableFactory = Spannable.Factory
            .getInstance();

    //ресурс для смайлів
    private static final Map<Pattern, Integer> emoticons = new HashMap<Pattern, Integer>();

    static {
        addPattern(emoticons, ":)", R.drawable.boy_with_flours);
        addPattern(emoticons, ":-)", R.drawable.boy_with_flours_run);
        addPattern(emoticons, ":-(", R.drawable.cry);
        addPattern(emoticons, "_-o", R.drawable.good_morning);
        addPattern(emoticons, "(*", R.drawable.i_was_bitten_by_mosquito);
        addPattern(emoticons, "^o)", R.drawable.many_students_living);
        addPattern(emoticons, "=`)", R.drawable.against_interference);
        addPattern(emoticons, ")**(", R.drawable.appointment);
        addPattern(emoticons, "OO[", R.drawable.bad_girl);
        addPattern(emoticons, "=)", R.drawable.happy);
        addPattern(emoticons, ")O_o", R.drawable.happy_girl_with_phone);
        addPattern(emoticons, "(..", R.drawable.hopelessness);
        addPattern(emoticons, ")|*", R.drawable.boy_with_flours_run);
        addPattern(emoticons, ")[-]", R.drawable.hoping);
        addPattern(emoticons, ":()", R.drawable.hungry);
        addPattern(emoticons, "(((", R.drawable.i_am_sorry);
        addPattern(emoticons, ":3", R.drawable.in_love);
        addPattern(emoticons, ":8", R.drawable.in_work);
        addPattern(emoticons, "^^", R.drawable.love_unlimited);
        addPattern(emoticons, ":D", R.drawable.merriment);
        addPattern(emoticons, ":-(", R.drawable.miss);
        addPattern(emoticons, ":<", R.drawable.offended);
        addPattern(emoticons, ":>|", R.drawable.pirate);
        addPattern(emoticons, ":/", R.drawable.poor_student_life);
        addPattern(emoticons, "[*]", R.drawable.present);
        addPattern(emoticons, "-88-", R.drawable.programmers_work);
        addPattern(emoticons, "O_O", R.drawable.scared);
        addPattern(emoticons, "-_-", R.drawable.surprised);
        addPattern(emoticons, "(`)|~", R.drawable.victimization);


        // ...
    }

    //додавання парсера
    private static void addPattern(Map<Pattern, Integer> map, String smile,
                                   int resource) {
        map.put(Pattern.compile(Pattern.quote(smile)), resource);
    }

    public static String validSmile(String text) {

        int count = 0;
        for (Pattern entry : emoticons.keySet()) {
            Matcher matcher = entry.matcher(text);
            while (matcher.find()) {
                count++;
                if(count >=3){
                    Log.d("matcher", matcher.group(0));
                    text = text.replace(matcher.group(0), "");
                }
            }
        }
        return text;
    }

    //додавання смайлів
    public static boolean addSmiles(Context context, Spannable spannable) {
        boolean hasChanges = false;
        for (Map.Entry<Pattern, Integer> entry : emoticons.entrySet()) {
            Matcher matcher = entry.getKey().matcher(spannable);
            while (matcher.find()) {
                boolean set = true;
                for (ImageSpan span : spannable.getSpans(matcher.start(),
                        matcher.end(), ImageSpan.class))
                    if (spannable.getSpanStart(span) >= matcher.start()
                            && spannable.getSpanEnd(span) <= matcher.end())
                        spannable.removeSpan(span);
                    else {
                        set = false;
                        break;
                    }
                if (set) {
                    hasChanges = true;
                    spannable.setSpan(new ImageSpan(context, entry.getValue()),
                            matcher.start(), matcher.end(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
        return hasChanges;
    }


    //декодування текста в смайли
    public static Spannable getSmiledText(Context context, CharSequence text) {

        Spannable spannable = spannableFactory.newSpannable(text);
        addSmiles(context, spannable);

        return spannable;

    }

    //додавання смайлів, при передачі позиції
    public static void setSmile(EditText editPostText, Activity activity, int position) {
        ImageSpan imageSpan = new ImageSpan(activity, Resource.Smile.emoticons.get(Resource.Smile.emoticons.keySet().toArray()[position]));
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(editPostText.getText());
        int selStart = editPostText.getSelectionStart();
        builder.replace(editPostText.getSelectionStart(), editPostText.getSelectionEnd(), Resource.Smile.emoticons.keySet().toArray()[position].toString());
        builder.setSpan(imageSpan, selStart, selStart + Resource.Smile.emoticons.keySet().toArray()[position].toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        editPostText.setText(builder);
        editPostText.setSelection(editPostText.length());
    }

}
