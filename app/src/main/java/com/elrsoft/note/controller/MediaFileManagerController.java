
package com.elrsoft.note.controller;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.fragment.CreatePostFragment;
import com.elrsoft.note.fragment.ViewPostFragment;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.resource.Resource;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yana on 15.03.2015.
 */
public class MediaFileManagerController {

    /**
     * Повертає останній змінений файл в директорії
     * @param directory - директорія, де відбувається пошук файлу
     * @return останній змінений файл
     */
    public static File getLastModifiedFile(File directory) {

        File file;
        File[] files = directory.listFiles();
        file = files[0];

        for (File temp : files) {
            if (file.lastModified() < temp.lastModified()) {
                file = temp;
            }
        }

        return file;
    }


    /**
     * Інтент перегляду відеофайлу
     * @param videoPath - шлях до файлу
     * @return інтент актівіті перегляду
     */
    public static void getVideoViewIntent(String videoPath, FragmentActivity activity) {
        try{
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
            intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
            activity.startActivity(intent);
        }catch (ActivityNotFoundException exception){
            Toast.makeText(activity, "Sorry, your device doesn`t support this action", Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * Відображає зображення на TitleImage
     * @param path - шлях до зображення
     * @param px - діменс, для оптимізації
     */
    public static void setImage(String path, int px) {

        Resource.PostData.pathToTitleImage = path;
        CreatePostController.editTitleImage.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(path, px, px));
    }

    /**
     * Прикріплює зображення до створеного поста
     * @param path - шлях до зображення
     * @param px   - діменс, для оптимізації
     */
    public static void setImageIcon(String path, int px) {
        Resource.PostData.pathToImage = path;
        CreatePostController.imageViewIcon.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(path, px, px));
        CreatePostController.imageViewIcon.setVisibility(View.VISIBLE);

    }
}