package com.elrsoft.note.controller;

import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ListView;

import com.elrsoft.note.R;
import com.elrsoft.note.activity.MainActivity;
import com.elrsoft.note.adapter.FileManagerAdapter;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.dialog.ShowImageDialogFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.model.FileItem;

import java.io.File;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Yana on 03.03.2015.
 */
public class FileManagerController {


    // повертає список файлів, що містить запитана директорія файлової системи
    public static List fill(File file){

        File[]dirs = file.listFiles();
        List<FileItem> dir = new ArrayList<FileItem>();
        List<FileItem>fls = new ArrayList<FileItem>();

        try{
            for(File files: dirs)
            {
                Date lastModDate = new Date(files.lastModified());
                DateFormat formater = DateFormat.getDateTimeInstance();
                String date_modify = formater.format(lastModDate);
                if(files.isDirectory()){


                    File[] fbuf = files.listFiles();
                    int buf = 0;
                    if(fbuf != null){
                        buf = fbuf.length;
                    }
                    else buf = 0;
                    String num_item = String.valueOf(buf);
                    if(buf == 0) num_item = num_item + " item";
                    else num_item = num_item + " items";

                    dir.add(new FileItem(files.getName(),num_item,date_modify,files.getAbsolutePath(),"directory_icon"));
                }
                else
                {
                    fls.add(new FileItem(files.getName(),files.length() + " Byte", date_modify, files.getAbsolutePath(),"file_icon"));
                }
            }
        }catch(Exception e)
        {

        }
        Collections.sort(dir);
        Collections.sort(fls);
        dir.addAll(fls);
        if(!file.getName().equalsIgnoreCase("sdcard") && file.getParent() != null)
            dir.add(0,new FileItem("..","Parent Directory","",file.getParent(),"directory_up"));

        return dir;
    }




}
