package com.elrsoft.note.controller;


import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 * Created by Yana on 02.04.2015.
 */
public class MailController  {


    // надсилає повідомлення на електронну пошту
    public void sendPassword(final String email, final String password, String newPassword){

        try{

            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            // відкриває сесію доступу до поштової скриньки
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(email, password);
                }
            });


            // Створює повідомлення
            String massage = "Hi. You got a new password:\n" + newPassword +
                    ".\nPlease enter it at the entrance to the application.";
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Q_art note: Password Reset");
            message.setContent(massage, "text/html; charset=utf-8");

            Log.d("massage", massage);
            Transport.send(message);

        }
        catch(MessagingException e){
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //перевіряє чи введена валідна електронна адреса
    public boolean isEmailValid(String email){
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches()){
            isValid = true;
        }
        return isValid;

    }


    // генерує випадковий шестисимвольний пароль
    public String generateRandomPassword() throws Exception{

        StringBuffer stringBuffer = new StringBuffer();
        String characters = "";

        Random random = new Random();
        int mode = random.nextInt(3);

        switch(mode){

            case 1:
                characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                break;

            case 2:
                characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                break;

            case 3:
                characters = "1234567890";
                break;
        }

        int charactersLength = characters.length();

        for (int i = 0; i < 6; i++) {
            double index = Math.random() * charactersLength;
            stringBuffer.append(characters.charAt((int) index));
        }
        return stringBuffer.toString();
    }


    // перевіряє чи доступне інтернет з'єднання в даний момент
    public boolean isInternerConnection(FragmentActivity activity) {
        ConnectivityManager cm =
                (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}