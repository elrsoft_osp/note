package com.elrsoft.note.controller;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.elrsoft.note.model.GPSMarker;
import com.elrsoft.note.model.Post;

/**
 * Created by Nazar on 03.03.2015.
 */
public class GPSController implements LocationListener {

    LocationManager locationManager;
    Context context;
    static GPSMarker marker;

    public GPSController(LocationManager locationManager, Context context) {
        this.context = context;
        this.locationManager = locationManager;
        // може вибити помилку
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                0, 10, this);
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 10,
                    this);
        } catch (Exception e) {
            Log.e("Error Internet", "GPSController, get exeption NETWORK_PROVIDER or GPS_PROVIDER");
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        saveCoordinates(location);
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        saveCoordinates(locationManager.getLastKnownLocation(provider));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void saveCoordinates(Location location) {

        Log.d("MyLog", "CON = " + location.getLatitude() + " , , , , " + location.getLongitude());
        marker = new GPSMarker();
        marker.setLatitude(location.getLatitude());
        marker.setLongitude(location.getLongitude());

    }

    public static GPSMarker getMarker() {
        return marker;
    }

    public LocationManager getLocationManager() {
        return locationManager;
    }
}


