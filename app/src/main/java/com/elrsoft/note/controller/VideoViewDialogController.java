package com.elrsoft.note.controller;

import android.app.Dialog;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.elrsoft.note.R;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.dialog.VideoViewDialogFragment;
import com.elrsoft.note.resource.Resource;

/**
 * Created by Yana on 20.04.2015.
 */
public class VideoViewDialogController extends DialogFragmentListener implements View.OnClickListener {
    ImageView videoViewImage;
    Button cancelVideo, saveVideo;
    FragmentActivity activity;
    VideoViewDialogFragment dialog;
    View view;

    public VideoViewDialogController() {
        this.activity = getActivity();
        this.view = VideoViewDialogFragment.getDialogView();
        this.dialog =  VideoViewDialogFragment.videoViewDialogFragment;



        //ініціалізує всі віджети
        initialize(view);
        // відображає вигляд відео на діалозі
        showVideoView(VideoViewDialogFragment.videoViewDialogFragment);


    }

    /**
     * Відображає зображення відео на діалозі
     *
     * @param dialog - об'єкт VideoViewDialogFragment
     */
    public void showVideoView(VideoViewDialogFragment dialog) {
        Resource.PostData.pathToVideo = (dialog.getArguments()).getString("videoUri");
        videoViewImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(Resource.PostData.pathToVideo,
                MediaStore.Images.Thumbnails.MINI_KIND));
    }

    /**
     * Ініціалізує всі віджети, що належать view
     *
     * @param view - об'єкт вигляду створеного діалогу
     */
    public void initialize(View view) {
        videoViewImage = (ImageView) view.findViewById(R.id.videoViewImage);
        cancelVideo = (Button) view.findViewById(R.id.cancel_video);
        saveVideo = (Button) view.findViewById(R.id.save_video);

        videoViewImage.setOnClickListener(this);
        cancelVideo.setOnClickListener(this);
        saveVideo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            // відкриває актівіті для перегляду відео
            case R.id.videoViewImage:
               MediaFileManagerController.getVideoViewIntent(Resource.PostData.pathToVideo, activity);
                break;

            //закриває діалог із відеофайлом
            case R.id.cancel_video:
                dialog.dismiss();
                break;

            //прикріплює відеофайл до поста
            case R.id.save_video:

                VideoViewDialogFragment.onDialogClick.onClick(Resource.PostData.pathToVideo);
                dialog.dismiss();
                break;
        }
    }

}
