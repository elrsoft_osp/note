package com.elrsoft.note.controller;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.db.service.UserService;
import com.elrsoft.note.dialog.ResetPasswordDialogFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.fragment.RegistrationFragment;
import com.elrsoft.note.model.User;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;


/**
 * Created by Yana on 10.04.2015.
 */
public class LoginController implements View.OnClickListener  {

    static EditText editTextLogin, editTextPassword;
    TextView resetPassword;
    CheckBox showPassword;
    Button login;
    FragmentActivity activity;
    int inputType;
    final int REQUEST_CODE_EMAIL = 4;
    boolean b;

    public LoginController() {

    }


    /**
     * Створює весь вигляд для Login Fragment-у:
     *
     * @param view               - вигляд Login Fragment-а;
     * @param activity           - activity, якому належить Login Fragment;
     * @param savedInstanceState - Bundle, що містить збережені при повороті екрану дані;
     */
    public LoginController(View view, FragmentActivity activity, Bundle savedInstanceState, boolean b) {

        this.b = b;
        this.activity = activity;
        //ініціалізує віджети для view
        initializeWidgets(view);


        //виводить деяку вхідну інформацію на фрагменті
        getDataForView(activity);
        // відновлює дані після повороту екрану
        onRestoreInstanceState(savedInstanceState);





    }

    /**
     * Ініціалізує сві віджети, що належать Login Fragment-у:
     *
     * @param view - - вигляд Login Fragment-а;
     */
    public void initializeWidgets(View view) {
        editTextLogin = (EditText) view.findViewById(R.id.editTextLogin);
        editTextPassword = (EditText) view.findViewById(R.id.editTextPassword);
        inputType = editTextPassword.getInputType();
        resetPassword = (TextView) view.findViewById(R.id.resetPassword);
        login = (Button) view.findViewById(R.id.loginButton);
        showPassword = (CheckBox)view.findViewById(R.id.showLoginPassword);

        login.setOnClickListener(this);
        resetPassword.setOnClickListener(this);
        showPassword.setOnClickListener(this);


        editTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(editTextPassword.getText().toString().length() < 6 ){
                    editTextPassword.setError("Password should contain at least 6 characters");
                }else if(editTextPassword.getText().toString().length() > 10){
                    editTextPassword.setError("Password is too long");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * Виводить деяку початкову інформацію у полях Login Fragment-у:
     *
     * @param activity - - activity, якому належить Login Fragment;
     */
    public void getDataForView(FragmentActivity activity) {
        UserService userService = new UserService(activity.getApplicationContext());
        User user = userService.getUser();

        editTextLogin.setText(user.getEmail());
    }

    /**
     * Відновлює дані після повороту екрану:
     *
     * @param savedInstanceState - - Bundle, що містить збережені при повороті екрану дані;
     */
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        if (savedInstanceState != null) {

            editTextLogin.setText(savedInstanceState.getString("login"));
            editTextPassword.setText(savedInstanceState.getString("password"));
        }
    }

    /**
     * Зберігає дані при повороті екрану:
     *
     * @param outState - Bundle, в який зберігаються дані;
     */
    public static void saveInstanceState(Bundle outState) {
        outState.putString("login", editTextLogin.getText().toString());
        outState.putString("password", editTextPassword.getText().toString());
    }

    /**
     * Перевідяє введені дані користувача:
     *
     * @return - якщо користувач ввів правильні вхідні дані, повертається true,
     * якщо введені логін або пароль не співпадають із реєстраційними даними користувача повертається false;
     */
    public boolean checkUserForLogin() throws Exception{

        UserService userService = new UserService(activity.getApplicationContext());
        User user = userService.getUser();

        String email = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();




        if (email.equals(user.getEmail()) && new PasswordController().encrypt(password).equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Слухач взаємодії з віджетами Login Fragment*а:
     *
     * @param v - віджет, з яким відбулась взаємодія;
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            // провіряє введені дані користувача, та виконує вхід або виводить попередження, в задежності від результату
            case R.id.loginButton:

                try {
                    if (checkUserForLogin()) {
                        if(b) {
                            new UserService(activity.getApplicationContext()).updateOnline(true);

                            FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));
                        } else {
                            // отримання офіційного email-адреса для телефону
                            try {
                                Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                                        new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, null, null, null, null);
                                activity.startActivityForResult(intent, REQUEST_CODE_EMAIL);
                            } catch (ActivityNotFoundException e) {
                                Log.d("AccountPicker", "Can`t find account");
                            }
                            FragmentNavigationManagerController.moveToFragment(activity, new RegistrationFragment(), "Registration Fragment");
                        }
                    } else {
                        Toast toast = Toast.makeText(activity.getApplicationContext(), "Can`t enter. Please, check your username and password."
                                , Toast.LENGTH_LONG);
                        toast.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            // відкриває діалог для відновлення паролю
            case R.id.resetPassword:
                ResetPasswordDialogFragment resetPasswordDialogFragment = new ResetPasswordDialogFragment();
                resetPasswordDialogFragment.show(activity.getSupportFragmentManager(), activity.getString(R.string.tag_reset_password_dialog_fragment));
                break;

            case R.id.showLoginPassword:

                        if(showPassword.isChecked()){
                            editTextPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }else {
                            editTextPassword.setInputType(inputType);
                        }
                break;






        }
    }

}
