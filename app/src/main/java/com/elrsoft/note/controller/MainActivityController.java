package com.elrsoft.note.controller;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.elrsoft.note.R;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.db.service.UserService;
import com.elrsoft.note.dialog.VideoViewDialogFragment;
import com.elrsoft.note.fragment.CloseAppFragment;
import com.elrsoft.note.fragment.CreatePostFragment;
import com.elrsoft.note.fragment.LoginFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.fragment.MapViewFragment;
import com.elrsoft.note.fragment.PaintFragment;
import com.elrsoft.note.fragment.RegistrationFragment;
import com.elrsoft.note.fragment.ViewPostFragment;
import com.elrsoft.note.resource.Resource;
import com.elrsoft.note.view.SingleTouchEventView;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;

import java.io.File;

/**
 * Created by Yana on 16.04.2015.
 */
public class MainActivityController {

    final int REQUEST_CODE_TITLE_PHOTO = 1, REQUEST_CODE_ICON_PHOTO = 2, REQUEST_CODE_VIDEO = 3,
            REQUEST_CODE_EMAIL = 4, REQUEST_PIC_CROP_TITLE = 5, REQUEST_PIC_CROP_ICON = 6, MEDIA_TYPE_VIDEO = 2;
    ImageView videoViewImage;
    File file;

    UserService userService;

    public MainActivityController(Activity activity) {
        userService = new UserService(activity.getApplicationContext());
    }

    /**
     * Обробляє результат виконання певного activity
     *
     * @param activity    - activity, яке обробляє результат
     * @param requestCode - код запиту дії, як виконувалась
     * @param resultCode  - код результату виконання
     * @param data        - intent із результатом виконання
     */
    public void onActivityResult(FragmentActivity activity, int requestCode, int resultCode,
                                 Intent data) {

        videoViewImage = (ImageView) activity.findViewById(R.id.videoViewImageOnPost);
        VideoViewDialogFragment videoViewDialogFragment = new VideoViewDialogFragment();

        if (requestCode == REQUEST_CODE_ICON_PHOTO && resultCode == activity.RESULT_OK) {

            CameraActionController.launchCropActivity(activity, REQUEST_PIC_CROP_ICON);    // викликає актівіті, що дозволяє обрізати зроблений фотознімок

        } else if (requestCode == REQUEST_CODE_VIDEO && resultCode == activity.RESULT_OK) {
            if (Uri.fromFile(VideoCameraController.getOutputVideoFile()) != null) {

                File file = MediaFileManagerController.getLastModifiedFile(VideoCameraController.getVideoStroageDir());
                String videoPath = file.getAbsolutePath();
                Resource.PostData.pathToVideo = videoPath;

                Bundle bundle = new Bundle();
                bundle.putString("videoUri", videoPath);

                videoViewDialogFragment.setArguments(bundle);
                videoViewDialogFragment.setListener(new DialogFragmentListener.OnClickDialogFragment() {
                    @Override
                    public void onClick(String parametr) {


                        videoViewImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(parametr, MediaStore.Images.Thumbnails.MINI_KIND));
                        videoViewImage.setVisibility(View.VISIBLE);

                    }
                });


            }
            videoViewDialogFragment.show(activity.getSupportFragmentManager(), activity.getString(R.string.tag_show_video_dialog_fragment));
        } else if (requestCode == REQUEST_CODE_EMAIL && resultCode == activity.RESULT_OK) {
            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);       // прикріплює офіційний email телефону до ditTextEmail
            ((EditText) activity.findViewById(R.id.editTextEmail)).setText(accountName);
        } else if (requestCode == REQUEST_PIC_CROP_ICON && resultCode == activity.RESULT_OK) {

            File file = MediaFileManagerController.getLastModifiedFile(new File("/mnt/sdcard/ext_sd/DCIM/100MEDIA/"));
            MediaFileManagerController.setImageIcon(file.getAbsolutePath(), activity.getResources().getDimensionPixelSize(R.dimen.image_size));

        } else if ((requestCode == REQUEST_PIC_CROP_TITLE || requestCode == REQUEST_PIC_CROP_ICON) && resultCode == activity.RESULT_CANCELED) {
            CameraActionController.launchCamera(activity, REQUEST_CODE_TITLE_PHOTO);    // повертає користувача на актівіті фотокамери

        } else if (requestCode == REQUEST_CODE_TITLE_PHOTO && resultCode == activity.RESULT_OK) {

            CameraActionController.launchCropActivity(activity, REQUEST_PIC_CROP_TITLE);
        } else if (requestCode == REQUEST_PIC_CROP_TITLE && resultCode == activity.RESULT_OK) {

            File file = MediaFileManagerController.getLastModifiedFile(new File("/mnt/sdcard/ext_sd/DCIM/100MEDIA/"));
            MediaFileManagerController.setImage(file.getAbsolutePath(), activity.getResources().getDimensionPixelSize(R.dimen.image_size));
        }
    }

    /**
     * Cлухач для item-ів із меню
     *
     * @param item     - item, з яким відбулась взаємодія
     * @param activity - - activity, якому належить меню
     * @return
     */
    public boolean onMenuItemSelected(MenuItem item, FragmentActivity activity) {


        switch (item.getItemId()) {

//            Наразі опція Background буде недоступною, але код НЕ ВИДАЛЯТИ
//            case R.id.change_fon:
//                FragmentNavigationManagerController.moveToFragment(activity, new GridFragment(), "GRID");
//                break;

            case R.id.searge_by_tag:
                Animation anim = AnimationUtils.loadAnimation(activity, R.anim.anim_button_create_post_out);
                activity.findViewById(R.id.txtToolBar).startAnimation(anim);
                activity.findViewById(R.id.txtToolBar).setVisibility(View.INVISIBLE);
                activity.findViewById(R.id.searchByTag).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.backSearch).setVisibility(View.VISIBLE);
                anim = AnimationUtils.loadAnimation(activity, R.anim.anim_search);
                activity.findViewById(R.id.backSearch).startAnimation(anim);
                activity.findViewById(R.id.searchByTag).startAnimation(anim);
                return true;

            case R.id.map:
                FragmentNavigationManagerController.moveToFragment(activity, new MapViewFragment(), activity.getString(R.string.tag_map_view_fragment));
                break;

            case R.id.registration:

                if ((userService.getAll()).size() != 0) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("boolean", false);
                    FragmentNavigationManagerController.moveToFragment(activity, new LoginFragment(bundle), "Login Fragment");
                } else {
                    // отримання офіційного email-адреса для телефону
                    try {
                        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                                new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, null, null, null, null);
                        activity.startActivityForResult(intent, REQUEST_CODE_EMAIL);
                    } catch (ActivityNotFoundException e) {
                        Log.d("AccountPicker", "Can`t find account");
                    }
                    FragmentNavigationManagerController.moveToFragment(activity, new RegistrationFragment(), "Registration Fragment");
                }
                break;

            case R.id.create_new_post:
                FragmentNavigationManagerController.moveToFragment(activity, new CreatePostFragment(), activity.getString(R.string.tag_create_post_fragment));
                break;

            case R.id.lock:
                if ((userService.getAll()).size() != 0) {

                    UserService userService1 = new UserService(activity.getApplicationContext());
                    if (item.isChecked()) {
                        item.setChecked(false);
                        userService.updateLogin(false);
                    } else {
                        item.setChecked(true);
                        userService.updateLogin(true);
                    }

                }

                break;

//            case R.id.setting:
//                FragmentNavigationManagerController.moveToFragment(activity, new SettingAppFragment(), activity.getString(R.string.tag_setting_app_fragment));
//                break;

            case R.id.exit:
                new UserService(activity.getApplicationContext()).updateOnline(false);
                FragmentNavigationManagerController.moveToFragment(activity, new CloseAppFragment(), activity.getString(R.string.close_fragment));
                break;

// add menu in CreatePostFragment
            case R.id.menuAddFileManager:

                CreatePostController.addFile(activity.getSupportFragmentManager());
                break;

            case R.id.menuTakeVideo:
                VideoCameraController.launchVideoCamera(activity, REQUEST_CODE_VIDEO);

                break;

            case R.id.menuTakePhoto:
                CameraActionController.launchCamera(activity, REQUEST_CODE_ICON_PHOTO);

                break;

            case R.id.menuRecordAudio:

                CreatePostController.recordAudio();
                break;

            case R.id.menuDraw:
                FragmentNavigationManagerController.removeSmileFragment(activity);
                FragmentNavigationManagerController.addFragment(activity, new PaintFragment(), activity.getString(R.string.tag_paint_fragment));

                break;
        }

        return true;
    }

    /**
     * Повертає на попередній фрагмент
     *
     * @param activity - - - activity, якому належить фрагмент
     */
    public static void backPressed(FragmentActivity activity) {
        MainFragment mainFragment = (MainFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_main_fragment));
        CreatePostFragment createPostFragment = (CreatePostFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_create_post_fragment));
        PaintFragment paintFragment = (PaintFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_paint_fragment));
        LoginFragment loginFragment = (LoginFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_login_fragment));

        if (paintFragment != null) {
            android.support.v4.app.FragmentManager manager = activity.getSupportFragmentManager();
            createPostFragment.setMenu(true);
            manager.beginTransaction().setCustomAnimations(R.anim.show_fragment, R.anim.close_fragment).remove(paintFragment).commit();
        } else if (mainFragment != null && mainFragment.isVisible()) {
            activity.onBackPressed();
        } else if (FragmentNavigationManagerController.isSmileFragmentVisible(activity)) {
            FragmentNavigationManagerController.removeSmileFragment(activity);
        } else if (createPostFragment != null && createPostFragment.isVisible() && CreatePostController.getPost() != null) {
            ViewPostFragment viewPostFragment1 = new ViewPostFragment();
            ViewPostController.setPost(new CreatePostController().getPost());
            CreatePostController.setPost(null);
            SaveDataController.setPostDataToNull();
            FragmentNavigationManagerController.moveToFragment(activity, viewPostFragment1, activity.getString(R.string.tag_view_post_fragment));
        } else if (createPostFragment != null && createPostFragment.isVisible() && CreatePostController.getPost() == null) {
            ViewPostController.setPost(new CreatePostController().getPost());
            CreatePostController.setPost(null);
            SaveDataController.setPostDataToNull();
            FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));
        } else {
            FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));
        }
    }


    /**
     * змінює колір пера
     *
     * @param activity - activity, якому належить фрагмент
     * @param view     - кнопка яка була викликала метод
     */
    public static void paintClicked(View view, FragmentActivity activity) {

        PaintFragment paintFragment = (PaintFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_paint_fragment));
        SingleTouchEventView drawView = (SingleTouchEventView) paintFragment.getView().findViewById(R.id.singleView);
        LinearLayout paintLayout = (LinearLayout) paintFragment.getView().findViewById(R.id.paint_colors);

        ImageButton currPaint = (ImageButton) paintLayout.getChildAt(0);

        drawView.setErase(false);
        drawView.setBrushSize(drawView.getLastBrushSize());

        ImageButton imgView = (ImageButton) view;
        String color = view.getTag().toString();
        drawView.setColor(color);
        currPaint.setImageDrawable(activity.getResources().getDrawable(R.drawable.paint));
    }

    /**
     * Визначає чи вимагає додаток авторизації після запуску та відкриває відповідний фрагмент
     *
     * @param activity           - activity, якому належить фрагмент
     * @param savedInstanceState - Bundle, що містить дані, збережені при повороту екрану
     */
    public static void openFragmentAfterStart(FragmentActivity activity, Bundle savedInstanceState) {
        UserService userService = new UserService(activity.getApplicationContext());


        // перевіряє чи користувач активував функцію логіна при запуску програми,
        // якщо логін вимагається, переходить на LoginFragment(), в іншому випадку - на MainFragment()
        if ((userService.getAll()).size() != 0 && userService.isLogin(userService.getUser())) {
            if (userService.isOnline()) {
                if (savedInstanceState == null) {
                    int flag = activity.getIntent().getIntExtra("Frag", 0);
                    if (flag == 1) {
                        FragmentNavigationManagerController.moveToFragment(activity, new CreatePostFragment(), activity.getString(R.string.tag_main_fragment));
                    } else {
                        FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));
                    }
                }
            } else {
                Bundle bundle = new Bundle();
                bundle.putBoolean("boolean", true);
                FragmentNavigationManagerController.moveToFragment(activity, new LoginFragment(bundle), activity.getString(R.string.tag_login_fragment));

            }

        } else {
            if (savedInstanceState == null) {
                int flag = activity.getIntent().getIntExtra("Frag", 0);
                if (flag == 1) {
                    FragmentNavigationManagerController.moveToFragment(activity, new CreatePostFragment(), activity.getString(R.string.tag_main_fragment));
                } else {
                    FragmentNavigationManagerController.moveToFragment(activity, new MainFragment(), activity.getString(R.string.tag_main_fragment));
                }
            }
        }
    }

    /**
     * Відображає фон для додатку
     *
     * @param activity - - activity, якому належить фрагмент
     */
    public static void showBackgroundAfterStart(FragmentActivity activity) {
        int px = activity.getResources().getDimensionPixelSize(R.dimen.image_size);

//        if (ImageController.getBitmapFromAsset(activity, new BackgroundService(activity).getBackground().getPath()) != null) {
//            ((ImageView) activity.findViewById(R.id.backdround)).setImageBitmap(ImageController.getBitmapFromAsset(activity, new BackgroundService(activity).getBackground().getPath()));
//        } else {
//            ((ImageView) activity.findViewById(R.id.backdround)).setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(new BackgroundService(activity).getBackground().getPath(), px, px));
//        }
    }
}
