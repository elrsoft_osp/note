package com.elrsoft.note.controller;

import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.elrsoft.note.R;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.db.service.GPSMarkerService;
import com.elrsoft.note.db.service.PostFileService;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.fragment.ViewPostFragment;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.GPSMarker;
import com.elrsoft.note.model.Post;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Yana on 25.04.2015.
 */
public class UpdatePostController {

    String title, postText, photoPath, videoPath, imagePath, audioPath;
    FileService fileService;
    PostFileService postFileService;
    GPSMarkerService gpsMarkerService;

    /**
     * Отримує дані з відредагованого поста та зберігає всі зміни
     *
     * @param map      - містить усі дані з поста
     * @param post     - об'єкт редагованого поста
     * @param marker   - маркер цього поста
     * @param activity - актівіті, що належить фрагменту редагування
     */
    public UpdatePostController(Map<String, String> map, Post post, GPSMarker marker, FragmentActivity activity) {

        fileService = new FileService(activity);
        postFileService = new PostFileService(activity);
        gpsMarkerService = new GPSMarkerService(activity);

        //отримує всі дані для поста
        getPostData(map);
        //зберігає відредагований пост
        updatePost(post, marker, activity);
    }

    /**
     * Отримує дані з відредагованого поста
     *
     * @param map - - містить усі дані з поста
     */
    public void getPostData(Map<String, String> map) {
        title = map.get("title");
        postText = map.get("postText");
        photoPath = map.get("photoPath");
        videoPath = map.get("videoPath");
        imagePath = map.get("imagePath");
        audioPath = map.get("audioPath");
    }

    /**
     * Зберігає пост з усіми змінами
     *
     * @param post     - - об'єкт редагованого поста
     * @param marker   - - маркер цього поста
     * @param activity - - актівіті, що належить фрагменту редагування
     */
    public void updatePost(Post post, GPSMarker marker, FragmentActivity activity) {

        post.setPostText(postText);
        post.setRating((int) post.getRating());
        post.setTitle(title);
        post.setTitleImage(photoPath);
        if (marker != null) {
            marker.setLatitude(marker.getLatitude() + 0.3);
            marker.setLongitude(marker.getLongitude() + 0.3);
            Log.d("MyLog", "getLatitude = " + marker.getLatitude() + "; getLongitude = " + marker.getLongitude());
            post.setMarkerId(gpsMarkerService.save(marker));
        }
        long postId = post.getId();
        new PostService(activity).update(post);

        //список з Id усіх файлів, що були прикріплені до поста
        List<Long> filesId = new PostFileService(activity.getApplicationContext()).getBy(post.getId());
        // список файлів(шляхів до файлів), що прикріплені до поста після редагування
        List<String> fileList = getAttachedFiles();


        //Порівнює чи змінилась кількість прикріплених до поста файлів до та після редагування
        if (fileList.size() < filesId.size()) {
            //якщо файл був видалений
            checkForDeleteAction(fileList, filesId, postId);

        } else if (fileList.size() > filesId.size()) {
            // якщо додано новий файл
            checkForSaveAction(fileList, filesId, postId);
        } else if (filesId.size() == fileList.size()) {
            // якщо кількість файлів, прикріплених до поста не змінилась
            checkForUpdateAction(fileList, filesId, postId);
        }

        ViewPostFragment viewPostFragment = new ViewPostFragment();
        ViewPostController.setPost(post);
        CreatePostController.setPost(null);
        FragmentNavigationManagerController.moveToFragment(activity, viewPostFragment, activity.getString(R.string.tag_view_post_fragment));
    }

    /**
     * Отримує список файлів, прикріплених до поста після редагування
     *
     * @return список прикріплених файлів
     */
    public List getAttachedFiles() {
        List<String> fileList = new ArrayList<>();

        if (videoPath != null && !videoPath.equals("")) {
            fileList.add(videoPath);
        }
        if (imagePath != null && !imagePath.equals("")) {
            fileList.add(imagePath);
        }
        if (audioPath != null && !audioPath.equals("")) {
            fileList.add(audioPath);
        }
        return fileList;
    }

    /**
     * Видаляє з бази даних інформацію про файли, що більше не належать посту та оновлює дійсні файли
     *
     * @param fileList - спискок файлів, прикріплених до поста після редагування
     * @param filesId  - список Id файлів, що належали посту до редагування
     * @param postId   - Id поста
     */
    public void checkForDeleteAction(List<String> fileList, List<Long> filesId, long postId) {

        //оновлює/зберігає в базу файли, що прикріплені до поста після редагування
        for (int i = 0; i < fileList.size(); i++) {
            long fileId = filesId.get(i);
            File f = new File(fileList.get(i), fileId);

            if (fileService.getById(f) != null) {
                fileService.update(new File(fileList.get(i), fileId));
                postFileService.update(postId, fileId);
            } else {
                fileService.save(f);
                postFileService.save(postId, fileId);
            }
        }

        //видаляє з бази інформацію про неактувальні фали для поста
        for (int i = fileList.size(); i < filesId.size(); i++) {

            long fileId = filesId.get(i);
            fileService.delete(fileId);
            postFileService.delete(postId, fileId);
        }
    }

    /**
     * Зберігає в базі даних нові прикріплені до поста файли та оновлює інформацію про існуючі
     *
     * @param fileList - спискок файлів, прикріплених до поста після редагування
     * @param filesId  - список Id файлів, що належали посту до редагування
     * @param postId   - Id поста
     */
    public void checkForSaveAction(List<String> fileList, List<Long> filesId, long postId) {

        //оновлює інснуючі файли
        for (int i = 0; i < filesId.size(); i++) {

            long fileId = filesId.get(i);
            File f = new File(fileList.get(i), fileId);
            fileService.update(f);
            postFileService.update(postId, fileId);
            Log.d("update post files", "save/update = " + f.getPath());
            Log.d("update post files", "postId = " + postId + " fileId = " + fileId);




        }

        //заносить дані про нові файли
        for (int i = filesId.size(); i < fileList.size(); i++) {

            long fileId = fileService.save(new File(fileList.get(i)));
            postFileService.save(postId, fileId);


        }
    }

    /**
     * Оновлює в базі даних файли, що прикріплені до поста після редагування
     *
     * @param fileList - спискок файлів, прикріплених до поста після редагування
     * @param filesId  - список Id файлів, що належали посту до редагування
     * @param postId   - Id поста
     */
    public void checkForUpdateAction(List<String> fileList, List<Long> filesId, long postId) {

        for (int i = 0; i < fileList.size(); i++) {

            long fileId = filesId.get(i);
            File f = new File(fileList.get(i), fileId);

            // якщо файл є в базі, оновлює його дані
            if (fileService.getById(f) != null) {

                fileService.update(f);
                postFileService.update(postId, fileId);

            }
            // якщо до поста прикріплений новий файл, зберігає його дані в базі
            else {

                long fId = fileService.save(new File(fileList.get(i)));
                postFileService.save(postId, fId);
            }
        }
    }
}
