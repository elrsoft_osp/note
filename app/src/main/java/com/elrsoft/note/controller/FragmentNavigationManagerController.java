package com.elrsoft.note.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.elrsoft.note.R;
import com.elrsoft.note.fragment.SmileFragment;

/**
 * Created by Yana on 26.02.2015.
 */
public class FragmentNavigationManagerController {

    // універсальний метод для навігації між фрагментами
    public static void moveToFragment(FragmentActivity activity, Fragment nextFragment, String fragmentName) {
        android.support.v4.app.FragmentManager manager = activity.getSupportFragmentManager();
        manager.beginTransaction().setCustomAnimations(R.anim.show_fragment, R.anim.close_fragment).replace(R.id.container,
                nextFragment, fragmentName).commit();

    }

    // універсальний метод для навігації між фрагментами
    public static void addFragment(FragmentActivity activity, Fragment nextFragment, String fragmentName) {
        android.support.v4.app.FragmentManager manager = activity.getSupportFragmentManager();
        manager.beginTransaction().setCustomAnimations(R.anim.show_fragment, R.anim.close_fragment).add(R.id.container,
                nextFragment, fragmentName).commit();

    }

    //додавання SmileFragmen
    public static void addSmileFragment(FragmentActivity activity, Fragment nextFragment, String fragmentName) {
        activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.on_top_anim_smile_fragment, R.anim.on_botom_anim_smile_fragment)
                .replace(R.id.containerSmile, nextFragment, fragmentName).commit();
    }

    //видалення SmileFragmen
    public static void removeSmileFragment(final FragmentActivity activity) {
        final SmileFragment paintFragment = (SmileFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_smile_fragment));
        if (paintFragment != null && isSmileFragmentVisible(activity)) {
            FrameLayout frameLayout = (FrameLayout) activity.findViewById(R.id.containerSmile);
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.on_botom_anim_smile_fragment);
            animation.setDuration(activity.getResources().getInteger(android.R.integer.config_shortAnimTime));
            animation.setDuration(500);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    try {
                        activity.getSupportFragmentManager().beginTransaction().remove(paintFragment).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            frameLayout.startAnimation(animation);
        }
    }

    //чи э на екрані SmileFragmen
    public static boolean isSmileFragmentVisible(FragmentActivity activity) {
        SmileFragment paintFragment = (SmileFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_smile_fragment));
        return paintFragment != null && paintFragment.isVisible();
    }
}
