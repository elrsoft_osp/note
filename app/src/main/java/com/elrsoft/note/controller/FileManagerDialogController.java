package com.elrsoft.note.controller;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.adapter.FileManagerAdapter;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.dialog.FileManagerDialogFragment;
import com.elrsoft.note.dialog.ShowImageDialogFragment;
import com.elrsoft.note.model.FileItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yana on 20.04.2015.
 */
public class FileManagerDialogController implements AdapterView.OnItemClickListener {

    static ListView fileSystemListView;
    Button cancel;
    Dialog dialog;
    FragmentActivity activity;
    FileManagerAdapter fileSystemAdapter;
    List<FileItem> dir;
    DialogFragmentListener.OnClickDialogFragment onClickDialogFragment;
    static File currentDir;

    public FileManagerDialogController() {
    }

    /**
     * Створює та ініціалізує весь вигляд для FileManagerDialogFragment
     *
     * @param view                      - об'єкт view для FileManagerDialogFragment
     * @param activity                  - актівіті, якому належитьFileManagerDialogFragment
     * @param dialog                    - об'єкт відкритого діалогу
     * @param onClickDialogFragment     - слухач для FileManagerDialogFragment
     */
    public FileManagerDialogController(View view, FragmentActivity activity, Dialog dialog, Bundle savedInstanceState,
                                       DialogFragmentListener.OnClickDialogFragment onClickDialogFragment) {
        this.activity = activity;
        this.dialog = dialog;
        this.onClickDialogFragment = onClickDialogFragment;

        GoogleAnalyticsController.setTracker(activity, "File manager");

        //ініціалізує всі вітджети
        initialize(view);
        if (savedInstanceState != null) {
            currentDir = new File(savedInstanceState.getString("dir"));
            fileSystemListView.setSelection(savedInstanceState.getInt("position"));
        }
        //відображає вміст кореневої директорії
        showDirectoryContent();
    }

    /**
     * Ініціалізує всі вітджети для FileManagerDialogFragment
     *
     * @param view - - об'єкт view для FileManagerDialogFragment
     */
    public void initialize(View view) {
        fileSystemListView = (ListView) view.findViewById(R.id.listView_file_system);
        cancel = (Button) view.findViewById(R.id.cancel_files_dialog);

        fileSystemListView.setOnItemClickListener(this);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }

    /**
     * Відображає вміст кореневої директорії на діалозі
     */
    public void showDirectoryContent() {
        // ініціалізуємо кореневу директорію та відображаємо її файли
        if (currentDir == null) {
            currentDir = new File("/mnt/");
        }

        dir = new ArrayList<>();
        dir = FileManagerController.fill(currentDir);

        fileSystemAdapter = new FileManagerAdapter(activity.getApplicationContext(), dir);
        fileSystemListView.setAdapter(fileSystemAdapter);
    }

    /**
     * Відкриває діалог перегляду зображення
     *
     * @param file - файл зображення, що був обраний
     */
    private void onFileClick(FileItem file) {
        Bundle bundle = new Bundle();
        bundle.putString("imagePath", file.getPath());
        ShowImageDialogFragment showImageDialogFragment = new ShowImageDialogFragment();
        showImageDialogFragment.setArguments(bundle);
        showImageDialogFragment.setOnClickDialogFragment(onClickDialogFragment);
        showImageDialogFragment.show(activity.getSupportFragmentManager(), "Show image dialog fragment");

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FileItem file = fileSystemAdapter.getItem(position);
        // якщо item зі списку є директорією, то при кліку, на діалозі відображається вміст цієї директорії
        if (file.getImage().equalsIgnoreCase("directory_icon") || file.getImage().equalsIgnoreCase("directory_up")) {
            currentDir = new File(file.getPath());

            dir = FileManagerController.fill(currentDir);
            fileSystemAdapter = new FileManagerAdapter(activity.getApplicationContext(), dir);
            fileSystemListView.setAdapter(fileSystemAdapter);
        } else {
            // якщо item є зображенням, викликається його метод
            if (file.getPath().contains(".jpg") || file.getPath().contains(".JPG") || file.getPath().contains(".png")) {
                onFileClick(file);
                // якщо item є іншим файлом, виводиться відповідне повідомлення
            } else {
                Toast toast = Toast.makeText(activity.getApplicationContext(), "You choose incorrect file. Please, select the" +
                        "image.", Toast.LENGTH_SHORT);
                toast.show();


            }


        }
    }

    public static void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("dir", currentDir.getPath());
        savedInstanceState.putInt("position", fileSystemListView.getFirstVisiblePosition());
    }


}
