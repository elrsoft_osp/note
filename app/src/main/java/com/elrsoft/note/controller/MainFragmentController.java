package com.elrsoft.note.controller;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import com.elrsoft.note.R;
import com.elrsoft.note.adapter.PostAdapter;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.fragment.CreatePostFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.fragment.ViewPostFragment;
import com.elrsoft.note.model.Post;
import com.software.shell.fab.ActionButton;
import com.elrsoft.note.resource.Resource;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Yana on 15.04.2015.
 */
public class MainFragmentController extends RecyclerView.OnScrollListener implements View.OnClickListener, PostAdapter.OnItemClickListener {

    ActionButton createPost;
    RecyclerView recyclerView;
    FragmentActivity activity;
    AnimButtonCreatePostController animButtonCreatePostController;
    PostAdapter postAdapter;
    List<Post> posts;
    PostService postService;
    ActionBar actionBar;


    public MainFragmentController() {
    }

    public MainFragmentController(View view, FragmentActivity activity) {
        recyclerView = (RecyclerView) view.findViewById(R.id.postAdapter);
        this.activity = activity;
    }

    /**
     * Створює весь вигляд для MainFragment:
     *
     * @param view         - об'єкт view для MainFragment;
     * @param activity     - актівіті,якому належить MainFragment;
     * @param mainFragment - фрагмент, що обслуговується;
     */
    public MainFragmentController(View view, FragmentActivity activity, MainFragment mainFragment, ActionBar actionBar) {

        this.actionBar = actionBar;
        this.activity = activity;


        // витягує пости, що вже є в базі даних
        postService = new PostService(activity);
        posts = postService.getAll();
        Collections.reverse(posts);
        postAdapter = new PostAdapter(posts, mainFragment, activity);

        // ініціалізує всі об'єкти для view
        initializeWidgets(view);


    }

    /**
     * Ініціалізує всі об'єкти для view
     *
     * @param view - view, яке відповідає даному фрагменту
     */
    public void initializeWidgets(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.postAdapter);
        createPost = (ActionButton) view.findViewById(R.id.button_create_post);
        animButtonCreatePostController = new AnimButtonCreatePostController(createPost);

        // створює layoutManager для recyclerView
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(postAdapter);

        postAdapter.setOnItemClickListener(this);
        createPost.setOnClickListener(this);
        recyclerView.setOnScrollListener(this);
    }

    /**
     * Оновлює адаптер:
     *
     * @param list         - містить список постів;
     * @param mainFragment - - фрагмент, що обслуговується;
     */
    public void updateAdapter(List<Post> list, MainFragment mainFragment) {
        postAdapter = new PostAdapter(list, mainFragment, activity);
        posts = list;
        try {
            Collections.reverse(posts);
        } catch (NullPointerException npe){
        }
        recyclerView.setAdapter(postAdapter);
        postAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        Log.d("scroller", "sc = " + recyclerView.getScrollState());


        if (dy > 1 && recyclerView.getScrollState() == 2) {
            //scroll down
            actionBar.hide();
            createPost.hide();
            //animButtonCreatePostController.startAnim(AnimationUtils.loadAnimation(activity, R.anim.anim_button_create_post_out), View.GONE);

        } else if (dy < 1 && recyclerView.getScrollState() == 1) {
            //scroll up
            createPost.show();
            actionBar.show();
            //animButtonCreatePostController.startAnim(AnimationUtils.loadAnimation(activity, R.anim.anim_button_create_post_in), View.VISIBLE);

        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_create_post:
                SaveDataController.setPostDataToNull();
                FragmentNavigationManagerController.moveToFragment(activity, new CreatePostFragment(), activity.getString(R.string.tag_create_post_fragment));
        }
    }


    @Override
    public void onItemClick(View view, int position) {

        ViewPostController.setPost(postAdapter.getItemById(position));
        FragmentNavigationManagerController.moveToFragment(activity, new ViewPostFragment(), activity.getString(R.string.tag_view_post_fragment));
    }


}
