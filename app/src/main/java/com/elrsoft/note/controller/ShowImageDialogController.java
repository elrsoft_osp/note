package com.elrsoft.note.controller;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.elrsoft.note.R;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.dialog.AddFileManagerDialogFragment;
import com.elrsoft.note.dialog.FileManagerDialogFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.model.FileItem;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yana on 20.04.2015.
 */
public class ShowImageDialogController implements View.OnClickListener {
    FragmentActivity activity;
    ImageView imageViewFromFile;
    Button hideImage, saveImage;
    DialogFragmentListener.OnClickDialogFragment onClickDialogFragment;
    Dialog dialog;
    DialogFragment dialogFragment;
    Bitmap bitmap;
    String filePath;
    AddFileManagerDialogFragment addFileManagerDialogFragment;

    public void setAddFile(AddFileManagerDialogFragment addFile){
        addFileManagerDialogFragment = addFile;

    }


    public ShowImageDialogController(){}

    public ShowImageDialogController(View view, FragmentActivity activity, Dialog dialog,  Bitmap bitmap, DialogFragment dialogFragment, DialogFragmentListener.OnClickDialogFragment onClickDialogFragment){
        this.activity = activity;
        this.onClickDialogFragment = onClickDialogFragment;
        this.dialog = dialog;
        this.dialogFragment = dialogFragment;
        this.bitmap = bitmap;


        // ініціалізує вітджети
        initialize(view);
        // відображає зображення в діалозі
        showImageOnDialog(null);
    }

    /**
     * Створює та ініціалізує весь вигляд діалогу
     * @param view - об'єкт вигляду ShowImageDialogFragment
     * @param activity - activity, якому належить ShowImageDialogFragment
     * @param dialog - об'єкт поточного діалогу
     * @param filePath - файл зображення
     * @param image - шлях до зображення
     * @param dialogFragment - об'єкт dialogFragment для діалогу
     * @param onClickDialogFragment - слухач діалогу
     */
    public ShowImageDialogController(View view, FragmentActivity activity, Dialog dialog,  String filePath, String image, DialogFragment dialogFragment, DialogFragmentListener.OnClickDialogFragment onClickDialogFragment) {
        this.activity = activity;
        this.onClickDialogFragment = onClickDialogFragment;
        this.dialog = dialog;
        this.dialogFragment = dialogFragment;
        this.filePath = filePath;

        if (bitmap != null) {
            bitmap.recycle();
        }

        // ініціалізує вітджети
        initialize(view);
        // відображає зображення в діалозі
        showImageOnDialog(image);

    }


    /**
     * Ініціалізує вітджети, що належать view
     * @param view - - об'єкт вигляду ShowImageDialogFragment
     */
    public void initialize(View view){
        imageViewFromFile = (ImageView) view.findViewById(R.id.imageView_from_file);
        hideImage = (Button) view.findViewById(R.id.cancelImageFromFile);
        saveImage = (Button) view.findViewById(R.id.saveImageFromFile);

        hideImage.setOnClickListener(this);
        saveImage.setOnClickListener(this);
    }

    /**
     * Відображає зображення на діалозі
     * @param image - - шлях до зображення
     */
    public void showImageOnDialog( String image){

        if(bitmap == null) {
            if (filePath != null) {

                imageViewFromFile.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(filePath, activity.getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
            } else {
                imageViewFromFile.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(image, activity.getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
            }
        } else {
            imageViewFromFile.setImageBitmap(bitmap);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            // прикріплює зображення до поста та закриває діалог файлової системи
            case R.id.saveImageFromFile:
                    onClickDialogFragment.onClick(filePath);

                dialog.cancel();
                if(FileManagerDialogFragment.fileManagerDialogFragment != null) {
                    FileManagerDialogFragment.fileManagerDialogFragment.getDialog().dismiss();
                }else if(AddFileManagerDialogFragment.addFileManagerDialogFragment != null){
                    AddFileManagerDialogFragment.addFileManagerDialogFragment.getDialog().dismiss();
                }
                break;

            //закриває діалог перегляду зображення
            case R.id.cancelImageFromFile:
                if (bitmap != null) {
                    bitmap.recycle();
                }
                dialog.cancel();
                break;
        }
    }
}
