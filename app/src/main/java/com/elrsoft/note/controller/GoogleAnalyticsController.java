package com.elrsoft.note.controller;

import android.support.v4.app.FragmentActivity;

import com.elrsoft.note.core.AnalyticsApp;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Yana on 07.05.2015.
 */
public class GoogleAnalyticsController {

    public static void setTracker(FragmentActivity activity, String action){
        Tracker t = ((AnalyticsApp)activity.getApplication()).getTracker(
                AnalyticsApp.TrackerName.GLOBAL_TRACKER);
        t.send(new HitBuilders.EventBuilder().setCategory("note").setAction(action).build());
    }

    public static void startAnalytics(FragmentActivity activity){
        GoogleAnalytics.getInstance(activity).reportActivityStart(activity);
    }

    public static void stopAnalytics(FragmentActivity activity){
        GoogleAnalytics.getInstance(activity).reportActivityStop(activity);

    }
}
