package com.elrsoft.note.controller;

import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import com.elrsoft.note.R;

/**
 * Created by <ELRsoft> HANZ on 04.04.2015.
 */
public class ScrollController {

    //зміна для контрола скрола
    int scroll;

    //інцилізація слухача та змін для скрола
    public ScrollController(View view, final ActionBar actionBar) {
//        scroll = 0;
//        final ScrollView scrollView = ((ScrollView) view.findViewById(R.id.scrollView));
//        //слухач скрола
//        view.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//
//            @Override
//            public void onScrollChanged() {
//
//                //виклак метода для анімації, та передача стан скрола
//                int scrollY = scrollView.getScrollY(); //for horizontalScrollView
//                scrollMove(scrollY, actionBar);
//            }
//        });
    }

    //метод анімювання ActionBar
    private void scrollMove(int scrollY, ActionBar actionBar) {

        //скрол в гору
        if (scroll + 10 < scrollY) {
            actionBar.hide();
            scroll = scrollY;

        //скрол в низ
        } else if (scroll - 10 > scrollY) {
            actionBar.show();
            scroll = scrollY;
        }

    }
}
