package com.elrsoft.note.controller;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Galya on 01.04.2015.
 */
public class ImageController {

    //метод по повернення Bitmap з папки assets
    public static Bitmap getBitmapFromAsset(Activity activity, String strName) {
        AssetManager assetManager = activity.getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeStream(istr);
    }

}
