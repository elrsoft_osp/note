package com.elrsoft.note.controller;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import java.sql.Struct;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Yana on 04.04.2015.
 */
public class TextValidationController {

    static  final String TOO_LOND_NAME = "Name is too long";
    static final String TOO_SHORT_PASSWORD = "Password is too short";
    static final String TOO_LONG_PASSWORD = "Password is too long";
    static final String ILLEGAL_CHARACTER = "Can contain english letters only";
    static final String EMPTY_FIELD = "Please, fill this field";
    static final String ILLEGAL_NAME = "Name isn`t valid";

    /**
     * Перевіряє валідність введеної назви
     * @param editText - поле, в яке введена назва;
     * @return введену назву, якщо вона валідна, у іншому випадку - null
     */
    public static String validName(EditText editText) {

        String name = editText.getText().toString();
        if (name.length() <= 0) {
            editText.setError(EMPTY_FIELD);
            name = null;
        } else if (name.length() > 16) {
            editText.setError(TOO_LOND_NAME);
            name = null;
        } else if(name.contains(" ")){
            if(name.endsWith(" ")){
                Log.d("nameValidation", "name is OK");
            }else {
                editText.setError("Can`t contain a space");
                name = null;
            }
        }
        return name;
    }

    /**
     * Перевіряє валідність введеної електронної пошти
     * @param editText - поле, в яке введена електронна адреса;
     * @return введену е-mail, якщо він валідний, у іншому випадку - null
     */
    public static String validEmail(EditText editText) {

        String email = editText.getText().toString();
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        if (email.length() == 0) {
            editText.setError(EMPTY_FIELD);
            email = null;
        } else if (email.length() > 36) {
            editText.setError(TOO_LOND_NAME);
            email = null;
        } else if (!matcher.matches()) {
            editText.setError(ILLEGAL_NAME);
            email = null;
        }
        return email;

    }

    /**
     * Перевіряє валідність введеного паролю
     * @param editText - поле, в яке введений пароль;
     * @return введений пароль, якщо він є валідним, у іншому випадку - null
     */
    public static String validPassword(EditText editText) {
        String password = editText.getText().toString();

        if (password.length() < 6) {
            editText.setError(TOO_SHORT_PASSWORD);
            password = null;

        } else if (password.length() > 10) {
            editText.setError(TOO_LONG_PASSWORD);
            password = null;
        }

        return password;
    }

    /**
     * Перевіряє валідність поста
     * @param data - введений текст;
     * @return введений текст якщо він валідний, у іншому випадку - null
     */
    public static String validPostData(String data) {

        if (data.length() <= 0) {
            data = null;
        }
        return data;
    }

    /**
     * Перевіряє та сповіщає про помилки під час вводу
     * @param map - містить об'єкти полів для вводу
     */
    public  void checkValidation(Map<String, EditText> map){

        final EditText email = map.get("email");
        final EditText firstName = map.get("firstName");
        final EditText lastName = map.get("lastName");
        final EditText country = map.get("country");
        final EditText password = map.get("password");

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(email.getText().toString().length() == 0){
                    email.setError(EMPTY_FIELD);
                }else if(email.getText().toString().length() > 36){
                    email.setError(TOO_LOND_NAME);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        firstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (firstName.getText().toString().length() > 16){
                    firstName.setError(TOO_LOND_NAME);
                }else if (firstName.getText().toString().length() == 0){
                    firstName.setError(EMPTY_FIELD);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(lastName.getText().toString().length() > 16){
                    lastName.setError(TOO_LOND_NAME);
                }else if(lastName.getText().toString().length() == 0){
                    lastName.setError(EMPTY_FIELD);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        country.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (country.getText().toString().length() > 16){
                    country.setError(TOO_LOND_NAME);
                }else if (country.getText().toString().length() == 0){
                    country.setError(EMPTY_FIELD);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(password.getText().toString().length() < 6){
                    password.setError(TOO_SHORT_PASSWORD);
                }else if (password.getText().toString().length() > 10){
                    password.setError(TOO_LONG_PASSWORD);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
