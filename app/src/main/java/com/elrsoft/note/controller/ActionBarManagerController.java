package com.elrsoft.note.controller;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.db.service.UserService;
import com.elrsoft.note.dialog.DeleteDialogFragment;
import com.elrsoft.note.fragment.CreatePostFragment;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.fragment.PaintFragment;
import com.elrsoft.note.fragment.RegistrationFragment;
import com.elrsoft.note.fragment.ViewPostFragment;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.model.User;
import com.elrsoft.note.resource.Resource;

import java.io.File;
import java.util.List;

/**
 * Created by <ELRsoft> HANZ on 01.03.2015.
 */
public class ActionBarManagerController {

    public static ActionBar managerActionBarCustom(final ActionBarActivity activity, int layoutXml, boolean visible) {
        ViewGroup mCustomView = null;
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0097A7")));
        LayoutInflater mInflater = LayoutInflater.from(activity);

        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT);

        if (layoutXml == R.layout.custom_action_bar) {

            mCustomView = (ViewGroup) mInflater.inflate(R.layout.custom_action_bar, null);
            if (visible) {
                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#33000000")));
                mCustomView.findViewById(R.id.txtToolBar).setVisibility(View.INVISIBLE);
                mCustomView.findViewById(R.id.imgToolBar).setVisibility(View.INVISIBLE);
                mCustomView.findViewById(R.id.back).setVisibility(View.VISIBLE);
                mCustomView.findViewById(R.id.delete).setVisibility(View.VISIBLE);

            }
            mCustomView.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onBackPressed();
                }
            });

            mCustomView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewPostFragment viewPostFragment = (ViewPostFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_view_post_fragment));
                    new DeleteDialogFragment().show(viewPostFragment.getChildFragmentManager(), "delete");
                }
            });

            Button back = ((Button) mCustomView.findViewById(R.id.backSearch));
            changeTitleInCreatePost(mCustomView, activity, layoutXml);

            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Animation anim = AnimationUtils.loadAnimation(activity, R.anim.back_anim_search);
                    activity.findViewById(R.id.searchByTag).startAnimation(anim);
                    activity.findViewById(R.id.searchByTag).setVisibility(View.INVISIBLE);
                    activity.findViewById(R.id.backSearch).setVisibility(View.INVISIBLE);
                    anim = AnimationUtils.loadAnimation(activity, R.anim.anim_button_create_post_in);
                    activity.findViewById(R.id.txtToolBar).startAnimation(anim);
                    activity.findViewById(R.id.txtToolBar).setVisibility(View.VISIBLE);
                    List<Post> listPost = new PostService(activity).getAll();
                    MainFragment mainFragment = (MainFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_main_fragment));
                    new MainFragmentController(mainFragment.getView(), mainFragment.getActivity()).updateAdapter(listPost, mainFragment);
                }
            });
            final EditText search = (EditText) mCustomView.findViewById(R.id.searchByTag);
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    List<Post> listPost = new PostService(activity).getByName(s.toString());
                    MainFragment mainFragment = (MainFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_main_fragment));
                    if (mainFragment != null) {
                        new MainFragmentController(mainFragment.getView(), mainFragment.getActivity()).updateAdapter(listPost, mainFragment);
                    }

                    if(search.getText().toString().contains("\n")) {
                        search.setText(search.getText().toString().replaceAll("\n", ""));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        } else {
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#33000000")));
            mCustomView = (ViewGroup) mInflater.inflate(R.layout.custom_action_bar_with_buttons, null);
            if (visible) {
                mCustomView.findViewById(R.id.smileAc).setVisibility(View.INVISIBLE);

            }
         //   changeTitleInCreatePost(mCustomView, activity, layoutXml);

            ((TextView) mCustomView.findViewById(R.id.save_post)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CreatePostFragment createPostFragment = (CreatePostFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_create_post_fragment));
                    PaintFragment paintFragment = (PaintFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_paint_fragment));
                    RegistrationFragment registrationFragment = (RegistrationFragment) activity.getSupportFragmentManager().findFragmentByTag("Registration Fragment");

                    if (registrationFragment != null && registrationFragment.isVisible()) {
                        registrationFragment.getUserDataForRegistration();
                    } else if (paintFragment != null) {
                        paintFragment.saveBitmap();
                        Resource.PostData.pathToImage = MediaFileManagerController.getLastModifiedFile(new File("/sdcard/DCIM/Camera/")).getAbsolutePath();
                        createPostFragment.getController().setImage();
                        createPostFragment.setMenu(true);
                        android.support.v4.app.FragmentManager manager = activity.getSupportFragmentManager();
                        manager.beginTransaction().setCustomAnimations(R.anim.show_fragment, R.anim.close_fragment).remove(paintFragment).commit();
                    } else if (createPostFragment != null) {
                        if (FragmentNavigationManagerController.isSmileFragmentVisible(activity)) {
                            FragmentNavigationManagerController.removeSmileFragment(activity);
                        }

                        if(CreatePostController.getEditTitle().getText().toString().length() > 0) {
                            new SaveDataController().savePost(createPostFragment.getActivity(), CreatePostController.getPost(), CreatePostController.getPostData());
                            CreatePostController.setPost(null);
                            SaveDataController.setPostDataToNull();
                        }else {
                            CreatePostController.getEditTitle().setError(" Please, fill this field");
                        }
                    } else {
                        CreatePostController.setPost(null);
                        SaveDataController.setPostDataToNull();
                        activity.onBackPressed();
                    }

                }
            });

            ((TextView) mCustomView.findViewById(R.id.cancel_post)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("cancelPost", "");
                    CreatePostController.setPost(null);
                    SaveDataController.setPostDataToNull();
                    activity.onBackPressed();
                }
            });

            ((Button) mCustomView.findViewById(R.id.smileAc)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CreatePostFragment createPostFragment = (CreatePostFragment) activity.getSupportFragmentManager().findFragmentByTag(activity.getString(R.string.tag_create_post_fragment));
                    createPostFragment.getController().openSmile();
                }
            });
        }
        actionBar.setCustomView(mCustomView, layout);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.show();
        return actionBar;
    }

    private static void changeTitleInCreatePost(View view, ActionBarActivity activity, int layoutXml) {
        TextView mTitleTextView = (TextView) view.findViewById(R.id.txtToolBar);
        User user = new UserService(activity).getUser();
        if(mTitleTextView != null) {
            if (layoutXml == R.layout.custom_action_bar) {
                if (user != null) {
                    mTitleTextView.setText(user.getFirstName());
                } else {
                    mTitleTextView.setText("Q_artNote");
                }
            } else {

                if (user != null) {
                    mTitleTextView.setText(user.getFirstName());
                } else {
                    mTitleTextView.setText("");
                }

            }
            mTitleTextView.setTextColor(Color.WHITE);
        }
    }

}