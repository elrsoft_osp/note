package com.elrsoft.note.resource;

import android.net.Uri;
import android.widget.ImageView;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;

import java.util.HashMap;

/**
 * Created by <ELRsoft> HANZ on 01.04.2015.
 */
public class Resource {

    public static final class Smile {
        public static final HashMap<String, Integer> emoticons = new HashMap<String, Integer>();

        static {
            emoticons.put(":)", R.drawable.boy_with_flours);
            emoticons.put(":-)", R.drawable.boy_with_flours_run);
            emoticons.put(":-(", R.drawable.cry);
            emoticons.put("_-o", R.drawable.good_morning);
            emoticons.put("(*", R.drawable.i_was_bitten_by_mosquito);
            emoticons.put("^o)", R.drawable.many_students_living);
            emoticons.put("=`)", R.drawable.against_interference);
            emoticons.put(")**(", R.drawable.appointment);
            emoticons.put("OO[", R.drawable.bad_girl);
            emoticons.put("=)", R.drawable.happy);
            emoticons.put(")O_o", R.drawable.happy_girl_with_phone);
            emoticons.put("(..", R.drawable.hopelessness);
            emoticons.put(")|*", R.drawable.boy_with_flours_run);
            emoticons.put(":()", R.drawable.hungry);
            emoticons.put("(((", R.drawable.i_am_sorry);
            emoticons.put(":3", R.drawable.in_love);
            emoticons.put(":8", R.drawable.in_work);
            emoticons.put("^^", R.drawable.love_unlimited);
            emoticons.put(":D", R.drawable.merriment);
            emoticons.put(":-(", R.drawable.miss);
            emoticons.put(":<", R.drawable.offended);
            emoticons.put(":>|", R.drawable.pirate);
            emoticons.put(":/", R.drawable.poor_student_life);
            emoticons.put("[*]", R.drawable.present);
            emoticons.put("O_O", R.drawable.scared);
            emoticons.put("-_-", R.drawable.surprised);
            emoticons.put("(`)|~", R.drawable.victimization);


        }
    }

    public static class PostData {

        public static String pathToTitleImage, pathToImage, pathToVideo, pathToAudio;;

    }
}
