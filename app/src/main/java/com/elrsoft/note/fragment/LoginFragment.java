package com.elrsoft.note.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.controller.LoginController;
import com.elrsoft.note.core.AnalyticsApp;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Nazar on 25.02.2015.
 */
public class LoginFragment extends Fragment {

    boolean b;

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ActionBarActivity) getActivity()).getSupportActionBar().setShowHideAnimationEnabled(false);
        ((ActionBarActivity) getActivity()).getSupportActionBar().show();
    }


    @SuppressLint("ValidFragment")
    public LoginFragment(Bundle bundle){
        b =bundle.getBoolean("boolean");
    }

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "login");

        View view = inflater.inflate(R.layout.login_fragment, container, false);

//        ActionBarManagerController.managerActionBarCustom((ActionBarActivity) this.getActivity(), R.layout.custom_action_bar_with_buttons, true);

        ((ActionBarActivity) getActivity()).getSupportActionBar().setShowHideAnimationEnabled(false);
        ((ActionBarActivity) getActivity()).getSupportActionBar().hide();

        new LoginController(view, getActivity(), savedInstanceState, b);

        return view;
    }

    public boolean getB(){
        return b;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        LoginController.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}
