package com.elrsoft.note.fragment;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import com.elrsoft.note.R;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.view.SingleTouchEventView;

import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.UUID;

import com.elrsoft.note.controller.GoogleAnalyticsController;


/**
 * Created by vika on 25.02.15.
 */
public class PaintFragment extends Fragment{


    private SingleTouchEventView drawView;
    int progress = 0, screenOrientation;
    ActionBar actionBar;
    SeekBar seekBar;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        GoogleAnalyticsController.setTracker(getActivity(), "paint_fragment");
        View view = inflater.inflate(R.layout.paint_fragment, container, false);

        screenOrientation = getActivity().getRequestedOrientation();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setHasOptionsMenu(false);
        actionBar =  ActionBarManagerController.managerActionBarCustom((ActionBarActivity) this.getActivity(), R.layout.custom_action_bar_with_buttons, true);
        CreatePostFragment createPostFragment = (CreatePostFragment) getActivity().getSupportFragmentManager().findFragmentByTag(getActivity().getString(R.string.tag_create_post_fragment));
        createPostFragment.setMenu(false);
        drawView = (SingleTouchEventView) view.findViewById(R.id.singleView);

        seekBar = (SeekBar) view.findViewById(R.id.seekBar);

        view.findViewById(R.id.elastics).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setColor("#FFFFFFFF");
                drawView.setErase(true);
                drawView.setBrushSize(PaintFragment.this.progress);
            }
        });

        view.findViewById(R.id.buttonNew).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.startNew();
            }
        });


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                PaintFragment.this.progress = progress;
                drawView.setBrushSize(progress);
                drawView.setLastBrushSize(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });

        return view;
    }


    public String saveBitmap(){
        drawView.setDrawingCacheEnabled(true);
        String str = UUID.randomUUID().toString()+".jpg";
        String imgSaved = MediaStore.Images.Media.insertImage(
                getActivity().getContentResolver(), drawView.getDrawingCache(),
                UUID.randomUUID().toString()+".jpg", "drawing");

        if(imgSaved!=null){
            Toast savedToast = Toast.makeText(getActivity().getApplicationContext(),
                    "Малюнок збирежено до галереї!", Toast.LENGTH_SHORT);
            savedToast.show();
        }
        else{
            Toast unsavedToast = Toast.makeText(getActivity().getApplicationContext(),
                    "Помилка збереження о_О", Toast.LENGTH_SHORT);
            unsavedToast.show();
        }
        drawView.destroyDrawingCache();

        return str;
    }

    @Override
    public void onDestroy() {
        getActivity().setRequestedOrientation(screenOrientation);
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}



