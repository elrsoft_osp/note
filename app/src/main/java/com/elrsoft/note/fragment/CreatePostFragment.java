package com.elrsoft.note.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.activity.MainActivity;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.CreatePostController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.core.AnalyticsApp;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by vika on 25.02.15.
 */
public class CreatePostFragment extends Fragment {

    ActionBar actionBar;
    CreatePostController createPostController;

    public CreatePostFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "create_post");

        setMenu(true);
        View view = inflater.inflate(R.layout.create_post_fragment, container, false);
        actionBar = ActionBarManagerController.managerActionBarCustom((ActionBarActivity) CreatePostFragment.this.getActivity(), R.layout.custom_action_bar_with_buttons, false);

        createPostController = new CreatePostController(view, getActivity(), savedInstanceState);
//        new DrawerLayoutManagerController().managerDrawerLayout(getActivity(), view);

        return view;
    }

    public void setMenu(boolean b){
        setHasOptionsMenu(b);
    }

    public CreatePostController getController(){
        return createPostController;
    }

    // зберігає дані при повороті екрану
    @Override
    public void onSaveInstanceState(Bundle outState) {
        new CreatePostController().onSaveInstanceState(outState);

        super.onSaveInstanceState(outState);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_test, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }

    @Override
    public void onDestroy() {
        CreatePostController.onDestroy();
        super.onDestroy();
    }


    @Override
    public void onPause() {
        createPostController.disableGPS();
        super.onPause();
    }
}