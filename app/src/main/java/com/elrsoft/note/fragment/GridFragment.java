package com.elrsoft.note.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.elrsoft.note.R;
import com.elrsoft.note.adapter.ImageGridAdapter;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.controller.PhotoSizeController;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.db.service.BackgroundService;
import com.elrsoft.note.dialog.FileManagerDialogFragment;
import com.elrsoft.note.model.Background;

import java.util.ArrayList;
import java.util.List;
import com.elrsoft.note.controller.ImageController;

/**
 * Created by <ELRsoft> HANZ on 02.03.2015.
 */
public class GridFragment extends Fragment {

    FileManagerDialogFragment fileManagerDialogFragment;
    List<Background> list;
    BackgroundService backgroundService;
    GridView gridview;
    private static String[] strings = new String[]{
            "fon/for_girl.jpg",
            "fon/gray_roses.jpg", "fon/normal.jpg", "fon/paris.jpg",
            "fon/wack.jpg", "fon/wall.jpg"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "grid_fragment");

        View view = inflater.inflate(R.layout.grid_fragment, container, false);

        ActionBarManagerController.managerActionBarCustom((ActionBarActivity) GridFragment.this.getActivity(), R.layout.custom_action_bar_with_buttons, true);

        backgroundService = new BackgroundService(getActivity());
        gridview = (GridView) view.findViewById(R.id.gridView);
        addToList();
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != list.size() - 1) {
                    if (position < 6) {
                        ((ImageView) GridFragment.this.getActivity().findViewById(R.id.backdround)).setImageBitmap(ImageController.getBitmapFromAsset(getActivity(), list.get(position).getPath()));
                    } else {
                        int px = getActivity().getResources().getDimensionPixelSize(R.dimen.image_size);
                        ((ImageView) GridFragment.this.getActivity().findViewById(R.id.backdround)).setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(list.get(position).getPath(), px, px));
                    }
                    new BackgroundService(getActivity()).setBackground(list.get(position));
                } else {

                    fileManagerDialogFragment = new FileManagerDialogFragment();
                    fileManagerDialogFragment.setListener(new DialogFragmentListener.OnClickDialogFragment() {
                        @Override
                        public void onClick(String path) {
                            Log.d("MyLog", "WTF?");
                            new BackgroundService(getActivity()).setBackground(new Background(0, path, null, 1));
                            addToList();
                        }
                    });
                    fileManagerDialogFragment.show(getActivity().getSupportFragmentManager(), getString(R.string.tag_file_manager_dialog_fragment));
                }

            }

        });
        registerForContextMenu(gridview);
        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        //додавання елементів до меню
        menu.add(0, 1, 0, "Use");
        menu.add(0, 2, 0, "Delete");

    }

    public void addToList(){
        list = new ArrayList<>();
        list.addAll(backgroundService.getAll());
        list.add(new Background(0, "fon/add.png", null, 0));
        Log.d("Back", "list = " + list.size());
        gridview.setAdapter(new ImageGridAdapter(list, getActivity()));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 1:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                if(info.position != list.size()-1) {
                    if (info.position < 6) {
                        ((ImageView) GridFragment.this.getActivity().findViewById(R.id.backdround)).setImageBitmap(ImageController.getBitmapFromAsset(getActivity(), list.get(info.position).getPath()));
                    } else {
                        int px = getActivity().getResources().getDimensionPixelSize(R.dimen.image_size);
                        ((ImageView) GridFragment.this.getActivity().findViewById(R.id.backdround)).setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(list.get(info.position).getPath(), px, px));
                    }
                }
                break;
            case 2:
                AdapterView.AdapterContextMenuInfo info1 = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                if(new BackgroundService(getActivity()).getBackground().getId() == list.get(info1.position).getId()) {
                    new BackgroundService(getActivity()).setBackground(list.get(0));
                }
                new BackgroundService(getActivity()).delete(list.get(info1.position));
                Log.d("MyLog", "delete");
                addToList();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}
