package com.elrsoft.note.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.db.service.UserService;
import com.elrsoft.note.model.User;

/**
 * Created by vika on 27.03.15.
 */
public class SettingAppFragment extends Fragment {

    CheckBox askCheckBox;
    UserService userService;
    User user;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        GoogleAnalyticsController.setTracker(getActivity(), "settings");

        View view = inflater.inflate(R.layout.setting_app_fragment, container, false);
        ActionBarManagerController.managerActionBarCustom((ActionBarActivity) this.getActivity(), R.layout.custom_action_bar_with_buttons, true);
        askCheckBox = (CheckBox)view.findViewById(R.id.loginCheckBox);
        userService = new UserService(getActivity().getApplicationContext());

        // перевіряємо чи база містить дані зареєстрованого користувача, якщо містить, то
        // askCheckBox стає активним і користувач може активувати функцію
        // логіна при запуску програми
        if((userService.getAll()).size() != 0){
            askCheckBox.setEnabled(true);

            // якщо користувач раніше задавав функцію логіна, при відкритті поточного фрагмента
            // відображаємо стан цієї опції
            if(userService.isLogin(userService.getUser())){
                askCheckBox.setChecked(true);
            }
        }else{
            askCheckBox.setEnabled(false);
        }



// в залежності від того чи askCheckBox.isChecked() оновлюємо відповідні
        // дані користувача в базі
        askCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("click", askCheckBox.isChecked() + " ");
                if (askCheckBox.isChecked()){
                    userService.updateLogin(true);
                }else {
                    userService.updateLogin(false);
                }
            }
        });


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}
