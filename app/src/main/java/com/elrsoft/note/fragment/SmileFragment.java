package com.elrsoft.note.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.elrsoft.note.R;
import com.elrsoft.note.adapter.SmileAdapter;
import com.elrsoft.note.controller.GoogleAnalyticsController;

import it.sephiroth.android.library.widget.HListView;

/**
 * Created by <ELRsoft> HANZ on 01.04.2015.
 */
public class SmileFragment extends Fragment {

    public interface OnClickSmile {
        public void clickSmile(int position);
    }

    private OnClickSmile onClickSmile;

    public void setOnClickSmile(OnClickSmile onClickSmile) {
        this.onClickSmile = onClickSmile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "smile");

        View view = inflater.inflate(R.layout.grid_smile, container, false);

        //додавання грід, з смайлами, та інцилізування слухача
        HListView gridview = (HListView) view.findViewById(R.id.hListView);
        gridview.setAdapter(new SmileAdapter(getActivity()));
        gridview.setOnItemClickListener(new it.sephiroth.android.library.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> adapterView, View view, int position, long l) {
                //визов слухача в createPostFragment
                onClickSmile.clickSmile(position);
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}
