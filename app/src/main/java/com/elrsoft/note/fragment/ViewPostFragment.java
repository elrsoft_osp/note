package com.elrsoft.note.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.controller.ScrollController;
import com.elrsoft.note.controller.ViewPostController;

/**
 * Created by vika on 25.02.15.
 */
public class ViewPostFragment extends Fragment {
    View view;
    ActionBar actionBar;
    ViewPostController viewPostController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "view_post");

        view = inflater.inflate(R.layout.view_post_fragment, container, false);
        actionBar = ActionBarManagerController.managerActionBarCustom( (ActionBarActivity) ViewPostFragment.this.getActivity(), R.layout.custom_action_bar,true);

        //створення контролера на скрол, він і буде робити магію
        new ScrollController(view, actionBar);
        // ініціалізація елементів view для ViewPostFragment
        viewPostController = new ViewPostController(view, getActivity(), getResources(), getChildFragmentManager(), savedInstanceState);

        return view;
    }

    // збереження даних при повороті екрану
    @Override
    public void onSaveInstanceState(Bundle outState) {
        viewPostController.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}