package com.elrsoft.note.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.controller.MainFragmentController;
import com.elrsoft.note.db.service.UserService;

/**
 * Created by vika on 25.02.15.
 */
public class MainFragment extends Fragment {

    ActionBar actionBar;

    public MainFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "main_fragment");

        final View view = inflater.inflate(R.layout.main_fragment, container, false);
        actionBar = ActionBarManagerController.managerActionBarCustom((ActionBarActivity) MainFragment.this.getActivity(), R.layout.custom_action_bar, false);

        new MainFragmentController(view, getActivity(), this, actionBar);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main_fragment, menu);
        UserService userService1 = new UserService(MainFragment.this.getActivity().getApplicationContext());
        if (userService1.getAll().size() != 0) {
            menu.findItem(R.id.lock).setEnabled(true);
            if (userService1.getUser() != null && userService1.isLogin(userService1.getUser())) {
                menu.findItem(R.id.lock).setChecked(true);
            } else {
                menu.findItem(R.id.lock).setChecked(false);
            }
        } else {
            menu.findItem(R.id.lock).setEnabled(false);
            menu.findItem(R.id.lock).setChecked(false);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}