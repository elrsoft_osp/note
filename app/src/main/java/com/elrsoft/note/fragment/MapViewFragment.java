package com.elrsoft.note.fragment;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.DrawPhotoForMapController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.model.GPSMarker;
import com.elrsoft.note.model.Post;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nazar on 11.03.2015.
 */
public class MapViewFragment extends Fragment{

    FragmentManager fragmentManager;
    SupportMapFragment supportMapFragment;
    GoogleMap googleMap;
    DrawPhotoForMapController mapController;

    public MapViewFragment(){}

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "map_view");

        View view = inflater.inflate(R.layout.map_view_fragment, container, false);

        fragmentManager = getChildFragmentManager();
        supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);

        // Створює карту якщо її нема є
        if (googleMap == null) {
            googleMap = supportMapFragment.getMap();
        }

        mapController = new DrawPhotoForMapController(googleMap, getActivity());

        // Слухач для змін на карті
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            private int currentZoom = -1;

            @Override
            public void onCameraChange(CameraPosition position) {
                // Провіряє чи змінювався zoom
                if (position.zoom != currentZoom){
                     currentZoom = (int)position.zoom;
                    Log.d("MyLog", "" + currentZoom);
                    if(currentZoom <= 7) {
                        // Очищає всі мітки і викликає метод для ставлення нових
                        googleMap.clear();
                        mapController.setMarkerForImage(7 - currentZoom);

                    }
                }
            }
        });

        // Викликає метод для ставлення міток
        mapController.setMarkerForImage(8);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }

}
