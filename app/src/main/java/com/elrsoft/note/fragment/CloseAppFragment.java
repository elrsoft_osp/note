package com.elrsoft.note.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ImageController;

import java.util.Random;

/**
 * Created by HANZ on 18.05.2015.
 */
public class CloseAppFragment extends Fragment {
    final String path = "app/goodbye/";

    final String[] resImage = {"bye.png", "come.png", "goodbye.png", "nice.png", "soon.png"};

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.app_fragment, container, false);
        Handler handler = new Handler();
        Random random = new Random();
        ((ImageView) view.findViewById(R.id.imageApp)).setImageBitmap(ImageController.getBitmapFromAsset(getActivity(), path + resImage[random.nextInt(4)]));
        ((ActionBarActivity) CloseAppFragment.this.getActivity()).getSupportActionBar().hide();
        handler.postDelayed(runnable, 1500);
        return view;
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
           CloseAppFragment.this.getActivity().finish();
        }
    };

}
