package com.elrsoft.note.fragment;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.elrsoft.note.R;

import com.elrsoft.note.controller.ActionBarManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.controller.RegistrationController;


/**
 * Created by vika on 25.02.15.
 */
public class RegistrationFragment extends Fragment{

    RegistrationController registrationController;

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ActionBarActivity) getActivity()).getSupportActionBar().setShowHideAnimationEnabled(false);
        ((ActionBarActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        GoogleAnalyticsController.setTracker(getActivity(), "registration");

        View view = inflater.inflate(R.layout.registration_fragment, container, false);
        ActionBarManagerController.managerActionBarCustom((ActionBarActivity) this.getActivity(), R.layout.custom_action_bar_with_buttons, true);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setShowHideAnimationEnabled(false);
//        ((ActionBarActivity) getActivity()).getSupportActionBar().hide();

        registrationController = new RegistrationController(view, getActivity(), savedInstanceState);

        return view;
    }

    public void getUserDataForRegistration(){
        registrationController.getUserDataForRegistration();
    }


    // зберігає дані при повороті екрану
    @Override
    public void onSaveInstanceState(Bundle outState) {
      registrationController.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}
