package com.elrsoft.note.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.CameraActionController;
import com.elrsoft.note.controller.CreatePostController;
import com.elrsoft.note.controller.PhotoSizeController;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.resource.Resource;

/**
 * Created by Yana on 22.04.2015.
 */
public class ChooseFromDialogFragment extends DialogFragment implements View.OnClickListener {

    View view;
    TextView gallery, cammera;
    final int REQUEST_CODE_PHOTO = 1;
    FragmentActivity activity;

    public ChooseFromDialogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.set_image_dialog_fragment, container);
        } else {
            ((ViewGroup) view.getParent()).removeView(view);
        }

        setRetainInstance(true);  // діалог зберігає свій екмпляр та може відновити його після повороту екрану
        getDialog().setTitle("Choose from: ");
        init();

        return view;
    }

    public void init() {
        gallery = (TextView) view.findViewById(R.id.choose_from_gallery);
        cammera = (TextView) view.findViewById(R.id.take_on_camera);

        gallery.setOnClickListener(this);
        cammera.setOnClickListener(this);
    }


    @Override
    public void onDestroyView() {

        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.choose_from_gallery:
                //відкриває діалог файлового менеджера
                FileManagerDialogFragment fileManagerDialogFragment = new FileManagerDialogFragment();
                fileManagerDialogFragment.setListener(new DialogFragmentListener.OnClickDialogFragment() {
                    @Override
                    public void onClick(String path) {

                        CreatePostController.getTitleImageView().setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(path, 100, 100));
                        Resource.PostData.pathToTitleImage = path;
                    }
                });
                fileManagerDialogFragment.show(getActivity().getSupportFragmentManager(), getActivity().getString(R.string.tag_file_manager_dialog_fragment));
                getDialog().cancel();
                break;

            case R.id.take_on_camera:
                Log.d("cameraAction", activity + "");
                CameraActionController.launchCamera(getActivity(), REQUEST_CODE_PHOTO);
                getDialog().cancel();
                break;
        }

    }
}
