package com.elrsoft.note.dialog;

import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.CreatePostController;
import com.elrsoft.note.controller.MediaFileManagerController;
import com.elrsoft.note.controller.PhotoSizeController;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.db.service.PostFileService;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.resource.Resource;

import java.util.List;

/**
 * Created by Yana on 24.04.2015.
 */
public class UpdateFileDialogFragment extends DialogFragment implements View.OnClickListener {

    View view;
    TextView delete;
    Post post;
    ImageView imageView, videoView;
    FragmentActivity activity = getActivity();
    String path;
    TextView textView;

    public UpdateFileDialogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.update_file_dialog_fragment, container);
        } else {
            ((ViewGroup) view.getParent()).removeView(view);
        }

        setRetainInstance(true);  // діалог зберігає свій екмпляр та може відновити його після повороту екрану
        getDialog().setTitle("Action: ");
        init();

        return view;
    }

    /**
     * Ініціалізує об'єкти діалогу
     */
    public void init() {
//        change = (TextView) view.findViewById(R.id.change_onUpdate);
        delete = (TextView) view.findViewById(R.id.delete_onUpdate);

//        change.setOnClickListener(this);
        delete.setOnClickListener(this);


        this.post = getArguments().getParcelable("post");
        this.imageView = CreatePostController.getImageViewIcon();
        this.videoView = CreatePostController.getVideoViewImage();
        this.textView = CreatePostController.getTxtViewMusic();
        this.path = getArguments().getString("path");
    }


    @Override
    public void onDestroyView() {

        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            // дозволяє змінити файл, що вже прикріплений до поста
//            case R.id.change_onUpdate:
//                updateFile(getFragmentManager(), fileView, textView);
//                getDialog().cancel();
//                break;

            // видаляє файл, що прикріплений до поста
            case R.id.delete_onUpdate:

                // виконується при створенні нового поста
                if (post == null) {
                    if (path.equals(Resource.PostData.pathToImage)) {

                        Resource.PostData.pathToImage = null;
                        imageView.setImageBitmap(null);
                        imageView.setVisibility(View.GONE);
                    } else if (path.equals(Resource.PostData.pathToVideo)) {

                        Resource.PostData.pathToVideo = null;
                        videoView.setImageBitmap(null);
                        videoView.setVisibility(View.GONE);
                    } else if (path.equals(Resource.PostData.pathToAudio)) {
                        Resource.PostData.pathToAudio = null;
                        textView.setText(null);
                        textView.setVisibility(View.GONE);
                    }

                }//виконується при редагуванні поста
                else {

                    List<Long> filesId = new PostFileService(getActivity().getApplicationContext()).getBy(post.getId());

                    for (long id : filesId) {

                        if (path.equals((new FileService(getActivity().getApplicationContext()).getById(id)).getPath())) {

                                if (path.equals(Resource.PostData.pathToImage)) {
                                    Resource.PostData.pathToImage = null;
                                    imageView.setImageBitmap(null);
                                    imageView.setVisibility(View.GONE);
                                } else if (path.equals(Resource.PostData.pathToVideo)) {
                                    Resource.PostData.pathToVideo = null;
                                    videoView.setImageBitmap(null);
                                    videoView.setVisibility(View.GONE);
                                }

                             else if(path.equals(Resource.PostData.pathToAudio)) {
                                Log.d("update music", "path = " + Resource.PostData.pathToAudio);
                                Resource.PostData.pathToAudio = null;
                                textView.setText(null);
                                textView.setVisibility(View.GONE);
                            }
                        }
                    }
                }
                getDialog().cancel();

                break;
        }

    }

    /**
     * Викликає діалог, для вибору файлу та прикріплює його до поста
     *
     * @param fragmentManager - fragmentManager, що відповідає фрагменту
     */

    public void updateFile(FragmentManager fragmentManager, final ImageView imageView, final TextView textView) {
        AddFileManagerDialogFragment addFileManagerDialogFragment = new AddFileManagerDialogFragment();
        addFileManagerDialogFragment.setListener(new DialogFragmentListener.OnClickDialogFragment() {
            @Override
            public void onClick(String parameter) {

                if (path == Resource.PostData.pathToImage) {
                    Resource.PostData.pathToImage = null;
                    imageView.setVisibility(View.GONE);
                } else if (path == Resource.PostData.pathToVideo) {
                    Resource.PostData.pathToVideo = null;
                    videoView.setVisibility(View.GONE);
                } else if (path == Resource.PostData.pathToAudio) {
                    Resource.PostData.pathToAudio = null;
                    textView.setVisibility(View.GONE);
                }

                if (parameter.contains(".jpg") || parameter.contains(".JPG")) {
                    Resource.PostData.pathToImage = parameter;

                    imageView.setImageBitmap(PhotoSizeController.decodeSampledBitmapFromResource(parameter, getActivity().getResources().getDimensionPixelSize(R.dimen.image_size), activity.getResources().getDimensionPixelSize(R.dimen.image_size)));
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setOnClickListener(null);


                } else if (parameter.contains(".mp4") || parameter.contains(".3gp")) {
                    Resource.PostData.pathToVideo = parameter;
                    imageView.setImageBitmap(ThumbnailUtils.createVideoThumbnail(parameter, MediaStore.Images.Thumbnails.MINI_KIND));
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MediaFileManagerController.getVideoViewIntent(Resource.PostData.pathToVideo, getActivity());
                        }
                    });


                } else if (parameter.contains(".mp3")) {
                    Resource.PostData.pathToAudio = parameter;
                    textView.setText(parameter.substring(parameter.lastIndexOf("/") + 1));
                    textView.setVisibility(View.VISIBLE);


                }

            }
        });

        addFileManagerDialogFragment.show(fragmentManager, " Add File Manager");

    }
}