package com.elrsoft.note.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.FragmentNavigationManagerController;
import com.elrsoft.note.controller.SaveDataController;
import com.elrsoft.note.controller.ViewPostController;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.db.service.PostFileService;
import com.elrsoft.note.db.service.PostService;
import com.elrsoft.note.fragment.MainFragment;
import com.elrsoft.note.fragment.ViewPostFragment;
import com.elrsoft.note.model.File;
import com.elrsoft.note.model.Post;
import com.elrsoft.note.resource.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vika on 02.03.15.
 */
public class DeleteDialogFragment extends DialogFragment {
    Button buttonYes, buttonNo;
    TextView txtPost;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.delete_post, container,
                false);
        getDialog().setTitle("Delete this post?");
        getDialog().setCancelable(true);


        ((Button) view.findViewById(R.id.buttonYes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new PostService(getActivity()).delete(ViewPostController.getPost());
                List<Long> filesId = new PostFileService(getActivity()).getBy(ViewPostController.getPost().getId());
                List<File> filesList = new FileService(getActivity()).getListFileToListId(filesId);

                for(File temp: filesList){
                    new FileService(getActivity()).delete(temp);
                    }

                new PostFileService(getActivity()).delete(ViewPostController.getPost());
                FragmentNavigationManagerController.moveToFragment(getActivity(), new MainFragment(), getString(R.string.tag_main_fragment));

            }
        });
        ((Button) view.findViewById(R.id.buttonNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                getDialog().dismiss();
            }


        });
        return view;
    }
}