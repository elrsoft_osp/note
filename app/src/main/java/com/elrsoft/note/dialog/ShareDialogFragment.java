package com.elrsoft.note.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.elrsoft.note.R;

/**
 * Created by Yana on 01.03.2015.
 */
public class ShareDialogFragment extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.share_dialog_fragment, container,
                false);
        getDialog().setTitle("Share on");


        ((TextView)view.findViewById(R.id.facebook)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast toast = Toast.makeText(getActivity().getApplicationContext(), "Shared on Facebook", Toast.LENGTH_SHORT);
                toast.show();

            }
        });

        ((TextView)view.findViewById(R.id.twitter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast toast = Toast.makeText(getActivity().getApplicationContext(), "Shared on Twitter", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        ((TextView)view.findViewById(R.id.vkontakte)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast toast = Toast.makeText(getActivity().getApplicationContext(), "Shared on Vkontakte", Toast.LENGTH_SHORT);
                toast.show();

            }
        });


        return view;
    }



}
