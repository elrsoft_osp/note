package com.elrsoft.note.dialog;


import android.content.Intent;
import android.graphics.Bitmap;

import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.adapter.FileManagerAdapter;
import com.elrsoft.note.controller.FileManagerController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.db.DBResource;
import com.elrsoft.note.model.FileItem;
import com.elrsoft.note.model.Post;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.TextTrackStyle;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vika on 09.03.15.
 */
public class AddFileManagerDialogFragment extends DialogFragmentListener {
    Button cancel;
    ListView listView;
    List<FileItem> dir;
    private File currentDir;
    private FileManagerAdapter fileSystemAdapter;
    MediaMetadataRetriever metaRetriver;
    View view;
    public static AddFileManagerDialogFragment addFileManagerDialogFragment;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);

        if (view == null) {
            view = inflater.inflate(R.layout.file_manager_list, container);
        } else {
            ((ViewGroup) view.getParent()).removeView(view);
        }

        GoogleAnalyticsController.setTracker(getActivity(), "Add file manager");

        getDialog().setTitle("Please, choose a file:");
        listView = (ListView) view.findViewById(R.id.listView_file_system);
        cancel = (Button) view.findViewById(R.id.cancel_files_dialog);
        addFileManagerDialogFragment = this;

        if (savedInstanceState != null) {
            currentDir = new File(savedInstanceState.getString("dir"));
            listView.setSelection(savedInstanceState.getInt("position"));
        }

        if(currentDir == null) {
            currentDir = new File("/mnt/");
        }
        dir = new ArrayList<>();
        dir = FileManagerController.fill(currentDir);
        fileSystemAdapter = new FileManagerAdapter(getActivity().getApplication(), dir);
        listView.setAdapter(fileSystemAdapter);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem file = fileSystemAdapter.getItem(position);


                if (file.getImage().equalsIgnoreCase("directory_icon") || file.getImage().equalsIgnoreCase("directory_up")) {
                    currentDir = new File(file.getPath());
                    dir = FileManagerController.fill(currentDir);
                    fileSystemAdapter = new FileManagerAdapter(getActivity().getApplication(), dir);
                    listView.setAdapter(fileSystemAdapter);


                } else {
                    if (file.getPath().contains(".jpg") || file.getPath().contains(".JPG") || file.getPath().contains(".png")) {
                        onImageFileClick(file);


                    } else if (file.getPath().contains(".mp4") || file.getPath().contains(".3gp")) {
                        onVideoFileClick(file);
                    } else if (file.getPath().contains(".mp3")) {
                        onClickDialogFragment.onClick(file.getPath());
                        metaRetriver = new MediaMetadataRetriever();
                        metaRetriver.setDataSource(file.getPath());
                        metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
                        getDialog().cancel();



                    } else {
                        Toast toast = Toast.makeText(getActivity().getApplicationContext(), "You choose incorrect file. Please, select the" +
                                "image.", Toast.LENGTH_SHORT);
                        toast.show();


                    }
                }
            }
        });

        return view;

    }

    // викликає діалог перегляду обраного зображення
    private void onImageFileClick(FileItem file) {
        Bundle bundle = new Bundle();
        bundle.putString("imagePath", file.getPath());

        ShowImageDialogFragment showImageDialogFragment = new ShowImageDialogFragment();
        showImageDialogFragment.setArguments(bundle);
        showImageDialogFragment.setAddFile(this);
        showImageDialogFragment.setOnClickDialogFragment(onClickDialogFragment);
        showImageDialogFragment.show(getActivity().getSupportFragmentManager(), "ShowImageDialogFragment");

    }

    private void onVideoFileClick(FileItem file) {
        Bundle bundle = new Bundle();
        bundle.putString("videoUri", file.getPath());

        VideoViewDialogFragment videoViewDialogFragment = new VideoViewDialogFragment();
        videoViewDialogFragment.setArguments(bundle);
        videoViewDialogFragment.setOnClickDialogFragment(onClickDialogFragment);
        videoViewDialogFragment.show(getActivity().getSupportFragmentManager(), "VideoViewDialogFragment");
        getDialog().cancel();

    }


    public void setListener(OnClickDialogFragment listener) {
        setOnClickDialogFragment(listener);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {


        outState.putString("dir", currentDir.getPath());
        outState.putInt("position", listView.getFirstVisiblePosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }
}

