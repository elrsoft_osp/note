package com.elrsoft.note.dialog;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.MailController;
import com.elrsoft.note.controller.PasswordController;
import com.elrsoft.note.db.service.UserService;
import com.elrsoft.note.model.User;

/**
 * Created by Yana on 03.04.2015.
 */
public class ResetPasswordDialogFragment extends DialogFragment implements View.OnClickListener {

    View view;
    EditText gmailAddress, gmailPassword;
    Button sendPassword;
    Toast toast;
    UserService userService;
    User user;

    public ResetPasswordDialogFragment() {
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.reset_password_dialog_fragment, container);
        } else {
            ((ViewGroup) view.getParent()).removeView(view);
        }

        setRetainInstance(true);

        getDialog().setTitle("Reset password:");
        initialize();


        return view;
    }

    /**
     * Ініціалізує необхідні вітджети
     */
    public void initialize() {
        gmailAddress = (EditText) view.findViewById(R.id.checkGmailAddress);
        gmailPassword = (EditText) view.findViewById(R.id.gmailPassword);
        sendPassword = (Button) view.findViewById(R.id.buttonSendPassword);
        sendPassword.setOnClickListener(this);

        userService = new UserService(getActivity().getApplicationContext());
        user = userService.getUser();

        gmailAddress.setText(user.getEmail());
    }


    @Override
    public void onDestroyView() {

        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            // надсилає повідомлення для відновлення паролю на поштову скриньку
            case R.id.buttonSendPassword:

                final MailController mail = new MailController();
                final String email = gmailAddress.getText().toString();
                final String password = gmailPassword.getText().toString();


                if (email.length() != 0 && password.length() != 0) {
                    if (mail.isInternerConnection(getActivity())) {


                        try {
                            if ((mail.isEmailValid(email) == true) && (email.length() > 0)) {

                                new AsyncTask<Void, Void, Void>() {

                                    @Override
                                    protected Void doInBackground(Void... params) {
                                        String newPassword = "eNpSwrS";
                                        try {
                                            newPassword = mail.generateRandomPassword();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        mail.sendPassword(email, password, newPassword);
                                        try {
                                            userService.updatePassword(new PasswordController().encrypt(newPassword));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        return null;
                                    }
                                }.execute();

                                toast = Toast.makeText(getActivity().getApplicationContext(), "A new password has been sent to your inbox.", Toast.LENGTH_SHORT);
                                toast.show();

                                getDialog().cancel();

                            } else {
                                toast = Toast.makeText(getActivity().getApplicationContext(), "Enter valid username and email address.", Toast.LENGTH_SHORT);
                                toast.show();

                            }
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "Oops... Something goes wrong", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        toast = Toast.makeText(getActivity().getApplicationContext(), "Can`t send. Please, check your internet connection.", Toast.LENGTH_SHORT);
                        toast.show();

                    }
                } else {
                    String massage = "Please, fill all fields";
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), massage, Toast.LENGTH_SHORT);
                    toast.show();
                }

                break;
        }
    }


}
