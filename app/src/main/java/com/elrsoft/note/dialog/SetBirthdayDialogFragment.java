package com.elrsoft.note.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import com.elrsoft.note.core.DialogFragmentListener;

import java.util.Calendar;

/**
 * Created by Yana on 28.03.2015.
 */
public class SetBirthdayDialogFragment extends DialogFragmentListener implements DatePickerDialog.OnDateSetListener {

    // створює діалог DatePicker, що надає можливість вибрати потрібну дату
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    // слухає створений DatePicker діалог, зберігає обрані дані
    public void onDateSet(DatePicker view, int year, int month, int day) {

        // DatePicker веде відлік місяців починаючи з 0, тому для звичного відображення
        // потрібно зробити невелике форматування отриманої дати
        String monthFormatted, dayFormatted;
        if(month < 9){
            monthFormatted = "0" + (month + 1);
        }else{
            monthFormatted = "" + (month + 1);
        }

        if(day < 10){
            dayFormatted = "0" + day;
        }
        else {
            dayFormatted = "" + day;
        }

        String birthday = dayFormatted + "." + monthFormatted + "." + year;

        onClickDialogFragment.onClick(birthday);
    }

    public void setListener(OnClickDialogFragment listener) {
        setOnClickDialogFragment(listener);
    }
}