package com.elrsoft.note.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.FileManagerDialogController;
import com.elrsoft.note.controller.GoogleAnalyticsController;
import com.elrsoft.note.core.DialogFragmentListener;

/**
 * Created by Yana on 02.03.2015.
 */
public class FileManagerDialogFragment extends DialogFragmentListener {

    // діалог файлового менеджера, що викликається при додаванні зображення на заголовк поста
    View view;
    public static FileManagerDialogFragment fileManagerDialogFragment;

    public FileManagerDialogFragment() {

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        setRetainInstance(true);

        if (view == null) {
            view = inflater.inflate(R.layout.file_manager_list, null, false);
        } else {
            ((ViewGroup) view.getParent()).removeView(view);
        }

        getDialog().setTitle("Select image");
        fileManagerDialogFragment = this;
        new FileManagerDialogController(view, getActivity(), getDialog(), savedInstanceState, onClickDialogFragment);
        return view;
    }


    // метод потрібний для збереження даних при повороті екрану
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        FileManagerDialogController.onSaveInstanceState(savedInstanceState);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setListener(OnClickDialogFragment listener) {
        setOnClickDialogFragment(listener);
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsController.startAnalytics(getActivity());
    }

    @Override
    public void onStop() {
        GoogleAnalyticsController.stopAnalytics(getActivity());
        super.onStop();
    }

}
