package com.elrsoft.note.dialog;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.ShowImageDialogController;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.model.FileItem;

/**
 * Created by Yana on 04.03.2015.
 */
public class ShowImageDialogFragment extends DialogFragmentListener {

    View view;
    static FileItem file;
    DialogFragment dialog;
    String image;
    Bitmap bitmap;
    AddFileManagerDialogFragment addFileManagerDialogFragment;

    public void setAddFile(AddFileManagerDialogFragment addFile) {
        addFileManagerDialogFragment = addFile;
    }

    @Override
    public void setOnClickDialogFragment(OnClickDialogFragment onClickListenerImageNavigator) {
        this.onClickDialogFragment = onClickListenerImageNavigator;
    }

    @SuppressLint("ValidFragment")
    public ShowImageDialogFragment() {

        this.dialog = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.show_image_dialog, container);
        } else {
            ((ViewGroup) view.getParent()).removeView(view);
        }

        setRetainInstance(true);  // діалог зберігає свій екмпляр та може відновити його після повороту екрану

        if (bitmap == null) {

            Bundle bundle = getArguments();
            String filePath = bundle.getString("imagePath");
            new ShowImageDialogController(view, getActivity(), getDialog(), filePath, image, dialog, onClickDialogFragment);
        } else {
            new ShowImageDialogController(view, getActivity(), getDialog(), bitmap, dialog, onClickDialogFragment);
        }

        return view;
    }


    @Override
    public void onDestroyView() {

        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }


    public static void setFileItem(FileItem fileItem){
        file = fileItem;
    }
}


