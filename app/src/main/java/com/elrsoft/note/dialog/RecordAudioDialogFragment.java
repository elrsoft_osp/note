package com.elrsoft.note.dialog;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.elrsoft.note.R;
import com.elrsoft.note.core.DialogFragmentListener;
import com.elrsoft.note.db.service.FileService;
import com.elrsoft.note.model.File;
import com.elrsoft.note.resource.Resource;

import java.io.IOException;

/**
 * Created by vika on 16.03.15.
 */
public class RecordAudioDialogFragment extends DialogFragmentListener implements View.OnClickListener {
    Button btnStartRecord, btnStopRecord, btnPlayRecord, btnStopPlayRecord;
    MediaRecorder mediaRecorder;
    String outputFile;
    MediaPlayer mediaPlayer;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.record_audio_dialog_fragment, container);
        getDialog().setTitle("Record audio");
        btnPlayRecord = (Button) view.findViewById(R.id.btnPlayRecord);
        btnPlayRecord.setOnClickListener(this);
        btnStopRecord = (Button) view.findViewById(R.id.btnStopRecord);
        btnStopRecord.setOnClickListener(this);
        btnStartRecord = (Button) view.findViewById(R.id.btnStartRecord);
        btnStartRecord.setOnClickListener(this);
        btnStopPlayRecord = (Button) view.findViewById(R.id.btnStopPlayRecord);
        btnStopPlayRecord.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStartRecord:
                startRecord(v);
                btnStopRecord.setEnabled(true);
                break;

            case R.id.btnStopRecord:
                stopRecord(v);
                Log.d("recordAudioFile", " stopRecord " + outputFile);
                onClickDialogFragment.onClick(outputFile);
                btnPlayRecord.setEnabled(true);
                break;

            case R.id.btnPlayRecord:
                playRecord(v);
                Log.d("recordAudioFile", "  playRecord ");
                btnStartRecord.setEnabled(true);
                break;

            case R.id.btnStopPlayRecord:
                stopPlay(v);
                Log.d("recordAudioFile", "  btnStopPlayRecord ");
                break;
        }


    }

    public void setListener(OnClickDialogFragment listener) {
        setOnClickDialogFragment(listener);
    }


    private void init() {
        if (mediaRecorder == null) {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording" + System.currentTimeMillis() +
                    ".mp3";

            mediaRecorder.setOutputFile(outputFile);

        }
    }

    private void startRecord(View v) {
        Log.d("recordAudioFile", " startRecord ");
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.mp3";

        try {
            init();

            mediaRecorder.prepare();
            mediaRecorder.start();
            new FileService(getActivity()).save(new File(outputFile));
            Resource.PostData.pathToAudio = outputFile;
        } catch (IllegalStateException e) {

            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        btnStartRecord.setEnabled(false);
        btnStopRecord.setEnabled(true);
        btnPlayRecord.setEnabled(false);
        btnStopPlayRecord.setEnabled(false);


    }

    private void stopRecord(View v) {
        if (mediaRecorder != null) {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
            btnStopRecord.setEnabled(false);
            btnStartRecord.setEnabled(true);
            btnPlayRecord.setEnabled(true);


        }
    }

    private void playRecord(final View v) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(outputFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            btnStopPlayRecord.setEnabled(true);
            btnPlayRecord.setEnabled(false);
            btnStartRecord.setEnabled(true);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    btnStopPlayRecord.setEnabled(false);
                    btnPlayRecord.setEnabled(true);
                    btnStartRecord.setEnabled(true);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        stopRecord(null);
        Log.d("recordAudioFile", " stopRecord " + outputFile);
        onClickDialogFragment.onClick(outputFile);
        btnPlayRecord.setEnabled(true);
        super.onDetach();
    }

    private void stopPlay(View v) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            btnStopPlayRecord.setEnabled(false);
            btnPlayRecord.setEnabled(true);
            btnStartRecord.setEnabled(true);


        }
    }
}

