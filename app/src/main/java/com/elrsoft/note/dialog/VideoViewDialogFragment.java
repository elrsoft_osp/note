package com.elrsoft.note.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elrsoft.note.R;
import com.elrsoft.note.controller.VideoViewDialogController;
import com.elrsoft.note.core.DialogFragmentListener;

/**
 * Created by Yana on 13.03.2015.
 */
public class VideoViewDialogFragment extends DialogFragmentListener {

    static View dialogView;
    public static VideoViewDialogFragment videoViewDialogFragment;
    public static OnClickDialogFragment onDialogClick;

    public VideoViewDialogFragment() {
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (dialogView == null) {
            dialogView = inflater.inflate(R.layout.show_video_dialog_fragment, container);
        } else {
            ((ViewGroup) dialogView.getParent()).removeView(dialogView);
        }

        setRetainInstance(true);
        videoViewDialogFragment = this;
        onDialogClick = onClickDialogFragment;
        new VideoViewDialogController();

        return dialogView;
    }


    @Override
    public void onDestroyView() {

        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    public void setListener(OnClickDialogFragment listener) {
        setOnClickDialogFragment(listener);
    }

    public static View getDialogView() {
        return dialogView;
    }


}
