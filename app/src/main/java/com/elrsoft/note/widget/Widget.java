package com.elrsoft.note.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.elrsoft.note.R;
import com.elrsoft.note.activity.MainActivity;

/**
 * Created by Galya on 04.03.2015.
 */
public class Widget extends AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverWidget";
    public static String ACTION_WIDGET_RECEIVER2 = "ActionReceiverWidget2";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        //Создаем новый RemoteViews
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_main);

        //Подготавливаем Intent для Broadcast
        Intent active = new Intent(context, Widget.class);
        active.setAction(ACTION_WIDGET_RECEIVER);
        active.putExtra("msg", "Hello Habrahabr");

        Intent active2 = new Intent(context, Widget.class);
        active2.setAction(ACTION_WIDGET_RECEIVER2);
        active2.putExtra("msg", "Page!!!!5151515+");

        //создаем наше событие
        PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, active, 0);
        PendingIntent actionPendingIntent2 = PendingIntent.getBroadcast(context, 0, active2, 0);

        //регистрируем наше событие
        remoteViews.setOnClickPendingIntent(R.id.pen, actionPendingIntent);
        remoteViews.setOnClickPendingIntent(R.id.page, actionPendingIntent2);

        //обновляем виджет
        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        //Ловим наш Broadcast, проверяем и выводим сообщение
        final String action = intent.getAction();
        String msg = "null";
        if (ACTION_WIDGET_RECEIVER.equals(action)) {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("Frag", 1);
            context.startActivity(i);

        } else if (ACTION_WIDGET_RECEIVER2.equals(action)) {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
        super.onReceive(context, intent);
    }

}